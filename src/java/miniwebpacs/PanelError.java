/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class PanelError extends JPanel
{
    private BorderLayout borderLayout1 = new BorderLayout();
    private JButton jButtonLimpar = new JButton();
    private JScrollPane jScrollPane1 = new JScrollPane();
    private JTextArea jTextArea1 = new JTextArea();

    public PanelError()
    {
        try
        {
            jbInit();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    void jbInit() throws Exception
    {
        jButtonLimpar.setText("Limpar");
        jButtonLimpar.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                jButtonLimpar_actionPerformed(e);
            }
        });
        this.setLayout(borderLayout1);
        jTextArea1.setText("");
        this.add(jButtonLimpar, BorderLayout.SOUTH);
        this.add(jScrollPane1, BorderLayout.CENTER);
        jScrollPane1.getViewport().add(jTextArea1, null);
    }

    void jButtonLimpar_actionPerformed(ActionEvent e)
    {
        jTextArea1.setText("");
    }

    public void add(Exception e)
    {
        java.io.ByteArrayOutputStream out = new java.io.ByteArrayOutputStream();
        java.io.PrintStream ps = new java.io.PrintStream(out);
        e.printStackTrace(ps);
        String str = out.toString();
        jTextArea1.append(str);
    }

}
