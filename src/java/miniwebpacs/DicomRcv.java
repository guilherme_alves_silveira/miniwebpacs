/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 * ********************************************************************
 */
package miniwebpacs;

import java.io.BufferedOutputStream;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.dcm4che.data.Command;
import org.dcm4che.data.Dataset;
import org.dcm4che.data.DcmDecodeParam;
import org.dcm4che.data.DcmElement;
import org.dcm4che.data.DcmEncodeParam;
import org.dcm4che.data.DcmObjectFactory;
import org.dcm4che.data.DcmParser;
import org.dcm4che.data.DcmParserFactory;
import org.dcm4che.data.FileMetaInfo;
import org.dcm4che.dict.Status;
import org.dcm4che.dict.Tags;
import org.dcm4che.dict.UIDs;
import org.dcm4che.net.AcceptorPolicy;
import org.dcm4che.net.ActiveAssociation;
import org.dcm4che.net.AssociationFactory;
import org.dcm4che.net.DcmServiceBase;
import org.dcm4che.net.DcmServiceRegistry;
import org.dcm4che.net.Dimse;
import org.dcm4che.server.DcmHandler;
import org.dcm4che.server.Server;
import org.dcm4che.server.ServerFactory;
import org.dcm4che.util.DcmProtocol;

/*
 * Modificacao da classe DcmRcv, exemplo do dcm4che.
 * @author     ramon moreno
 * @author     <a href="mailto:gunter@tiani.com">gunter zeilinger</a>
 */
public class DicomRcv extends DcmServiceBase
{

    private static final int RSP_DELAY = 0;
    private static final int BUFFER_SIZE = 512;
    private static final Logger log = Logger.getLogger(DicomRcv.class);
    private static final ServerFactory srvFact = ServerFactory.getInstance();
    private static final AssociationFactory fact = AssociationFactory.getInstance();
    private static final DcmParserFactory pFact = DcmParserFactory.getInstance();
    private static final DcmObjectFactory oFact = DcmObjectFactory.getInstance();

    private static final String[] syntaxIMG = new String[]
    {
        "ExplicitVRLittleEndian",
        "ExplicitVRBigEndian",
        "ImplicitVRLittleEndian",
        "JPEGLossless14",
        "JPEGLossless",
        "JPEGLSLossless",
        "JPEG2000Lossless",
        "RLELossless",
        "JPEGBaseline",
        "JPEGExtended",
        "JPEGLSLossy",
        "JPEG2000Lossy"
    };

    private static final String[] storage = new String[]
    {
        "HardcopyGrayscaleImageStorage",
        "HardcopyColorImageStorage",
        "ComputedRadiographyImageStorage",
        "DigitalXRayImageStorageForPresentation",
        "DigitalXRayImageStorageForProcessing",
        "DigitalMammographyXRayImageStorageForPresentation",
        "DigitalMammographyXRayImageStorageForProcessing",
        "DigitalIntraoralXRayImageStorageForPresentation",
        "DigitalIntraoralXRayImageStorageForProcessing",
        "CTImageStorage",
        "UltrasoundMultiframeImageStorageRetired",
        "UltrasoundMultiframeImageStorage",
        "MRImageStorage",
        "EnhancedMRImageStorage",
        "MRSpectroscopyStorage",
        "NuclearMedicineImageStorageRetired",
        "UltrasoundImageStorageRetired",
        "UltrasoundImageStorage",
        "SecondaryCaptureImageStorage",
        "MultiframeSingleBitSecondaryCaptureImageStorage",
        "MultiframeGrayscaleByteSecondaryCaptureImageStorage",
        "MultiframeGrayscaleWordSecondaryCaptureImageStorage",
        "MultiframeColorSecondaryCaptureImageStorage",
        "XRayAngiographicImageStorage",
        "XRayRadiofluoroscopicImageStorage",
        "XRayAngiographicBiPlaneImageStorageRetired",
        "NuclearMedicineImageStorage",
        "VLImageStorageRetired",
        "VLMultiframeImageStorageRetired",
        "VLEndoscopicImageStorage",
        "VLMicroscopicImageStorage",
        "VLSlideCoordinatesMicroscopicImageStorage",
        "VLPhotographicImageStorage",
        "PositronEmissionTomographyImageStorage",
        "RTImageStorage"
    };

    private final Dataset overwrite = oFact.newDataset();
    private final AcceptorPolicy policy = fact.newAcceptorPolicy();
    private final DcmServiceRegistry services = fact.newDcmServiceRegistry();
    private final DcmHandler handler = srvFact.newDcmHandler(policy, services);
    private final Server server = srvFact.newServer(handler);
    private final DcmProtocol protocol = DcmProtocol.DICOM;
    private final List<StoreListener> listeners = new ArrayList<StoreListener>();

    private int port = 104;
    private File dir = null;
    private DicomRcvFSU fsu = null;
    private String basePath = null;

    public DicomRcv(int port, String basePath)
    {
        this.port = port;
        this.basePath = basePath;
        initServer();
        initDest();
        initPolicy();
    }

    public void addStoreListener(StoreListener listener)
    {
        listeners.add(listener);
    }

    public void removeStoreListener(StoreListener listener)
    {
        listeners.remove(listener);
    }

    private void initServer()
    {
        server.setPort(port);
        server.setMaxClients(10);
    }

    private final void initDest()
    {
        String dest = basePath;
        this.dir = new File(dest);
        this.fsu = new DicomRcvFSU(new File(dir, "DICOMDIR"));
        handler.addAssociationListener(fsu);
    }

    private void initPolicy()
    {
        //aceita todos
        policy.setCalledAETs(null);
        //aceita todos
        policy.setCallingAETs(null);
        initPresContext(storage, syntaxIMG);
    }

    private void initPresContext(String[] asNames, String[] tsNames)
    {
        for (String asName : asNames)
        {
            initPresContext(asName, tsNames);
        }
    }

    private void initPresContext(String asName, String[] tsNames)
    {
        String as = UIDs.forName(asName);
        String[] tsUIDs = new String[tsNames.length];
        for (int i = 0; i < tsUIDs.length; ++i)
        {
            tsUIDs[i] = UIDs.forName(tsNames[i]);
        }
        policy.putPresContext(as, tsUIDs);
        services.bind(as, this);
    }

    /**
     * Description of the Method
     *
     * @exception IOException Description of the Exception
     */
    public void start() throws IOException
    {
        if (fsu != null)
        {
            new Thread(fsu).start();
        }
        server.start();
    }

    public void startedMove()
    {
        for (StoreListener listener : listeners)
        {
            listener.started();
        }
    }

    public void finishedMove()
    {
        for (StoreListener listener : listeners)
        {
            listener.finished();
        }
    }

    public void stop()
    {
        System.out.println("STOPPING");
        server.stop();
    }

    /**
     * DcmServiceBase overrides --------------------------------------
     * Description of the Method
     *
     * @param assoc Description of the Parameter
     * @param rq Description of the Parameter
     * @param rspCmd Description of the Parameter
     *
     * @exception IOException Description of the Exception
     */
    @Override
    protected void doCStore(ActiveAssociation assoc, Dimse rq, Command rspCmd) throws IOException
    {
        System.out.println("chamando CStore!!!!!!");
        try (InputStream in = rq.getDataAsStream())
        {
            if (dir != null)
            {
                Command rqCmd = rq.getCommand();
                FileMetaInfo fmi = objFact.newFileMetaInfo(
                        rqCmd.getAffectedSOPClassUID(),
                        rqCmd.getAffectedSOPInstanceUID(),
                        rq.getTransferSyntaxUID());
                store(in, fmi);
            }
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
        catch (java.sql.SQLException ioe)
        {
            ioe.printStackTrace();
        }

        if (RSP_DELAY > 0L)
        {
            try
            {
                Thread.sleep(RSP_DELAY);
            }
            catch (InterruptedException ie)
            {
                ie.printStackTrace();
            }
        }

        rspCmd.putUS(Tags.Status, Status.Success);
    }

    private OutputStream openOutputStream(File file) throws IOException
    {
        File parent = file.getParentFile();
        if (!parent.exists())
        {
            if (!parent.mkdirs())
            {
                throw new IOException("Could not create " + parent);
            }
            log.info("M-WRITE " + parent);
        }
        log.info("M-WRITE " + file);
        return new BufferedOutputStream(new FileOutputStream(file));
    }

    private void store(InputStream in, FileMetaInfo fmi) throws IOException, java.sql.SQLException
    {
        System.out.println("chamou store!!!!");
        Dataset ds = oFact.newDataset();
        DcmParser parser = pFact.newDcmParser(in);
        parser.setDcmHandler(ds.getDcmHandler());
        DcmDecodeParam decParam = DcmDecodeParam.valueOf(fmi.getTransferSyntaxUID());
        parser.parseDataset(decParam, Tags.PixelData);
        doOverwrite(ds);
        File file = fsu.toFile(ds);
        System.out.println("file=" + file);
        //Aqui eu guardo em bd
        DB.getInstance().store(ds, file);
        try (OutputStream out = openOutputStream(file))
        {
            ds.setFileMetaInfo(fmi);
            ds.writeFile(out, (DcmEncodeParam) decParam);
            if (parser.getReadTag() != Tags.PixelData)
            {
                return;
            }
            ds.writeHeader(out, (DcmEncodeParam) decParam,
                           parser.getReadTag(),
                           parser.getReadVR(),
                           parser.getReadLength());
            copy(in, out);
        }

        fsu.schedule(file, ds);
        for (StoreListener listener : listeners)
        {
            listener.received(file);
        }
    }

    private void doOverwrite(Dataset ds)
    {
        for (Iterator it = overwrite.iterator(); it.hasNext();)
        {
            DcmElement el = (DcmElement) it.next();
            ds.putXX(el.tag(), el.vr(), el.getByteBuffer());
        }
    }

    // Package protected ---------------------------------------------
    // Protected -----------------------------------------------------
    // Private -------------------------------------------------------
    private void copy(InputStream in, OutputStream out) throws IOException
    {
        if (BUFFER_SIZE > 0)
        {
            byte[] buffer = new byte[BUFFER_SIZE];
            int c;
            while ((c = in.read(buffer)) != -1)
            {
                out.write(buffer, 0, c);
            }
        }
        else
        {
            int ch;
            while ((ch = in.read()) != -1)
            {
                out.write(ch);
            }
        }
    }

    public static void main(String[] args)
    {
        System.setProperty("database.impl", "br.usp.incor.minipacs.DBAccess");
        DicomRcv rcv = new DicomRcv(104, "E:/lixo/imagensdcm4che");
        try
        {
            rcv.start();
            System.out.println("O prompt eh liberado?");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}