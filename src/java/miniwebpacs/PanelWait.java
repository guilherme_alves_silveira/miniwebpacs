/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs;

import java.awt.*;
import javax.swing.*;

/**
 * <p>
 * Title: </p>
 * <p>
 * Description: </p>
 * <p>
 * Copyright: Copyright (c) 2004</p>
 * <p>
 * Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PanelWait extends JDialog implements Runnable
{
    private FlowLayout flowLayout1 = new FlowLayout();
    private JProgressBar jProgressBar1 = new JProgressBar();
    private int size = 2000;
    private int pos = 0;
    private boolean keep_running = true;
    private Thread thread = null;
    private JLabel jLabel1 = new JLabel();

    public PanelWait()
    {
        super(MainFrame.getInstance(), "Aguarde", false);
        try
        {
            jbInit();
            this.setLocationRelativeTo(MainFrame.getInstance());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    void jbInit() throws Exception
    {
        this.getContentPane().setLayout(flowLayout1);
        jProgressBar1.setPreferredSize(new Dimension(200, 21));
        jProgressBar1.setMinimum(0);
        jProgressBar1.setMaximum(size);
        jLabel1.setToolTipText("");
        jLabel1.setText("Aguarde. Carregando imagens...");
        this.getContentPane().add(jLabel1, null);
        this.getContentPane().add(jProgressBar1, null);
        this.setSize(250, 100);
    }

    public void run()
    {
        try
        {
            while (keep_running)
            {
                pos = (pos + 1) % size;
                jProgressBar1.setValue(pos);
                Thread.currentThread().sleep(5);
            }
            jProgressBar1.setValue(size);
            Thread.currentThread().sleep(50);
            System.out.println("Done.");
            setVisible(false);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void start()
    {
        keep_running = true;
        thread = new Thread(this);
        thread.start();
    }

    public void stop()
    {
        keep_running = false;
        thread = null;
    }

}
