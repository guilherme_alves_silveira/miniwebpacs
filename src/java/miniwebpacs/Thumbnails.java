/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs;

import java.awt.*;
import java.awt.image.*;
import javax.swing.*;

public class Thumbnails extends JPanel implements FileDataSourceListener, Viewer
{
    private GridLayout gridLayout1 = new GridLayout(0, 3, 5, 5);
    private int sz = 256;
    private int count = 0;
    private LabelGroup group = new LabelGroup();
    private PanelWait pw = new PanelWait();

    public Thumbnails()
    {
        try
        {
            jbInit();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public JComponent getGUI()
    {
        return this;
    }

    public JToolBar getControls()
    {
        return new JToolBar();
    }

    void jbInit() throws Exception
    {
        MainFrame.getInstance().getFileDataSource().addFileDataSourceListener(this);
        this.setLayout(gridLayout1);
        this.setBackground(Color.black);
    }

    public void setNumColumns(int cols)
    {
        gridLayout1.setColumns(cols);
        updateUI();
    }

    public void started()
    {
        pw.setVisible(true);
        pw.start();
    }

    public void dataChanged()
    {
        FileDataSource src = MainFrame.getInstance().getFileDataSource();
        try
        {
            int fileID = src.getSize() - 1;
            BufferedImage image = src.getFrameAt(fileID, 0);
            BufferedImage thumbImage = new BufferedImage(sz,
                                                         sz, BufferedImage.TYPE_INT_RGB);
            Graphics2D graphics2D = thumbImage.createGraphics();
            graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                                        RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            graphics2D.drawImage(image, 0, 0, sz, sz, null);
            StateLabel jlabel = new StateLabel(group, new ImageIcon(thumbImage), null, fileID);
            jlabel.setForeground(Color.YELLOW);
            jlabel.setVerticalTextPosition(JLabel.BOTTOM);
            jlabel.setHorizontalTextPosition(JLabel.CENTER);
            jlabel.setHorizontalAlignment(JLabel.CENTER);
            jlabel.setVerticalAlignment(JLabel.CENTER);
            add(jlabel);
            count++;
            if (count % gridLayout1.getColumns() == 0)
            {
                validate();
            }
            pw.stop();
            adjust();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public synchronized void adjust()
    {
        int cols = getWidth() / sz;
        if (cols > count)
        {
            cols = count;
        }
        if (cols > 0)
        {
            setNumColumns(cols);
        }
        else
        {
            setNumColumns(1);
        }
    }
}
