/**********************************************************************
 *                                                                    *
 *  Mini PACS with WEB interface for small-sized hospitals.           *
 *  Copyright (C) 2004 Funda��o Zerbini                               *
 *                                                                    *
 *  This program is free software; you can redistribute it and/or     *
 *  modify it under the terms of the GNU General Public License as    *
 *  published by the Free Software Foundation; either version 2 of    *
 *  the License, or (at your option) any later version.               *
 *                                                                    *
 *  This program is distributed in the hope that it will be useful,   *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of    *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     *
 *  GNU General Public License for more details.                      *
 *                                                                    *
 *  You should have received a copy of the GNU General Public License *
 *  along with this program; if not, write to the Free Software       *
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA         *
 *  02111-1307, USA.                                                  *
 *                                                                    *
 *  MiniWEBPACS     - http://miniwebpacs.sourceforge.net              *
 *  Ramon A. Moreno - ramon.moreno@incor.usp.br                       *
 *                                                                    *
 **********************************************************************/

package miniwebpacs;
import java.sql.*;
import org.dcm4che.data.*;
import org.dcm4che.dict.*;
import java.io.*;
import java.util.Vector;


public class DBAccess extends DB{

  public DBAccess() {
    try {
      Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  public Connection getConnection(String conName) {
    try {
      return DriverManager.getConnection("jdbc:odbc:minipacs","","");
    }
    catch (Exception ex) {
      ex.printStackTrace();
      return null;
    }
  }
  public void freeConnection(String conName, Connection con) {
    try {
      con.close();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  public void store(Dataset dataset, File file) throws java.sql.SQLException {
    String patid = dataset.getString(Tags.PatientID);
    String patnam = dataset.getString(Tags.PatientName);
    String patsex = dataset.getString(Tags.PatientSex);
    String stuid = dataset.getString(Tags.StudyID);
    String accnum = dataset.getString(Tags.AccessionNumber);
    String studat = dataset.getString(Tags.StudyDate);
    String stuinsuid = dataset.getString(Tags.StudyInstanceUID);
    String mod = dataset.getString(Tags.Modality);
    String serinsuid = dataset.getString(Tags.SeriesInstanceUID);
    String sopinsuid = dataset.getString(Tags.SOPInstanceUID);
    String sql = "SELECT * FROM IMAGENS WHERE IMG_SOPINSUID = ? ";
    Connection con = getConnection(null);
    PreparedStatement statement = con.prepareStatement(sql);
    statement.setString(1,sopinsuid);
    boolean b = statement.executeQuery().next();
    statement.close();
    if(b) return;//isto �, j� existe no banco de dados
    sql = "INSERT INTO IMAGENS(IMG_PATID,IMG_PATNAM,IMG_PATSEX,IMG_STUID,IMG_ACCNUM,IMG_STUDAT,IMG_STUINSUID,IMG_MOD,IMG_SERINSUID,IMG_SOPINSUID,IMG_PATH) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
    statement = con.prepareStatement(sql);
    statement.setString(1,patid);
    statement.setString(2,patnam);
    statement.setString(3,patsex);
    statement.setString(4,stuid);
    statement.setString(5,accnum);
    statement.setString(6,studat);
    statement.setString(7,stuinsuid);
    statement.setString(8,mod);
    statement.setString(9,serinsuid);
    statement.setString(10,sopinsuid);
    statement.setString(11,file.getAbsolutePath());
    statement.executeUpdate();
    statement.close();
    freeConnection(null,con);
  }

  public boolean hasExam(String stuinsuid) throws java.sql.SQLException {
    String sql = "SELECT * FROM IMAGENS WHERE IMG_STUINSUID = ? ";
    Connection con = getConnection(null);
    PreparedStatement statement = con.prepareStatement(sql);
    statement.setString(1,stuinsuid);
    boolean b = statement.executeQuery().next();
    statement.close();
    return b;
  }

  public String[] listFiles(String stuinsuid) throws java.sql.SQLException {
    String sql = "SELECT IMG_PATH FROM IMAGENS WHERE IMG_STUINSUID = ? ";
    Connection con = getConnection(null);
    PreparedStatement statement = con.prepareStatement(sql);
    statement.setString(1,stuinsuid);
    ResultSet rs = statement.executeQuery();
    Vector list = new Vector();
    while (rs.next()) {
      String path = rs.getString(1);
      list.add(path);
    }
    statement.close();
    String[] answer = new String[list.size()];
    list.copyInto(answer);
    list.clear();
    return answer;
  }

}
