/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs;

import java.sql.*;
import org.dcm4che.data.Dataset;
import java.io.File;
import org.dcm4che.dict.*;
import java.util.Vector;

public class DBHsqldb extends DB
{

    public DBHsqldb()
    {
        try
        {
            Class.forName("org.hsqldb.jdbcDriver").newInstance();
            File file = new File(getDir(), "database");
            Connection con = DriverManager.getConnection("jdbc:hsqldb:" + file.getAbsolutePath(), "sa", "");
            Statement statement = con.createStatement();
            try
            {
                /*
                 CREATE USER MINIPACS PASSWORD "" ADMIN
                 */
                String sql = "CREATE USER MINIPACS PASSWORD \"\" ADMIN";
                statement.executeUpdate(sql);
                statement.close();
                statement = con.createStatement();
                /*
                 CREATE TABLE IMAGENS(
                    IMG_PATID VARCHAR(64),
                    IMG_PATNAM  VARCHAR(64),
                    IMG_PATSEX  VARCHAR(64),
                    IMG_STUID  VARCHAR(64),
                    IMG_ACCNUM  VARCHAR(64),
                    IMG_STUDAT  VARCHAR(64),
                    IMG_STUINSUID  VARCHAR(64),
                    IMG_MOD  VARCHAR(64),
                    IMG_SERINSUID  VARCHAR(64),
                    IMG_SOPINSUID  VARCHAR(64),
                    IMG_PATH VARCHAR(255),
                    PRIMARY KEY (IMG_SOPINSUID)
                 )
                 */
                sql = "CREATE TABLE IMAGENS( "
                        + "IMG_PATID VARCHAR(64), "
                        + "IMG_PATNAM  VARCHAR(64), "
                        + "IMG_PATSEX  VARCHAR(64), "
                        + "IMG_STUID  VARCHAR(64), "
                        + "IMG_ACCNUM  VARCHAR(64), "
                        + "IMG_STUDAT  VARCHAR(64), "
                        + "IMG_STUINSUID  VARCHAR(64), "
                        + "IMG_MOD  VARCHAR(64), "
                        + "IMG_SERINSUID  VARCHAR(64), "
                        + "IMG_SOPINSUID  VARCHAR(64), "
                        + "IMG_PATH VARCHAR(255),"
                        + "PRIMARY KEY (IMG_SOPINSUID) )";
                statement.executeUpdate(sql);
                statement.close();
                con.close();
                System.out.println("Tabela criada.");
            }
            catch (Exception ex)
            {
            }
        }
        catch (Exception e)
        {
            System.out.println("ERROR: failed to load HSQLDB JDBC driver.");
            e.printStackTrace();
        }
    }

    public void freeConnection(String conName, Connection con)
    {
        try
        {
            con.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public Connection getConnection(String conName)
    {
        try
        {
            File dir = getDir();
            File file = new File(dir, "database");
            Connection c = DriverManager.getConnection("jdbc:hsqldb:" + file.toString(), "MINIPACS", "");
            return c;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    public boolean hasExam(String stuinsuid) throws SQLException
    {
        String sql = "SELECT * FROM IMAGENS WHERE IMG_STUINSUID = ? ";
        Connection con = getConnection(null);
        PreparedStatement statement = con.prepareStatement(sql);
        statement.setString(1, stuinsuid);
        boolean b = statement.executeQuery().next();
        statement.close();
        return b;
    }

    public String[] listFiles(String stuinsuid) throws SQLException
    {
        String sql = "SELECT IMG_PATH FROM IMAGENS WHERE IMG_STUINSUID = ? ";
        Connection con = getConnection(null);
        PreparedStatement statement = con.prepareStatement(sql);
        statement.setString(1, stuinsuid);
        ResultSet rs = statement.executeQuery();
        Vector list = new Vector();
        while (rs.next())
        {
            String path = rs.getString(1);
            list.add(path);
        }
        statement.close();
        String[] answer = new String[list.size()];
        list.copyInto(answer);
        list.clear();
        return answer;
    }

    public void store(Dataset dataset, File file) throws SQLException
    {
        String patid = dataset.getString(Tags.PatientID);
        String patnam = dataset.getString(Tags.PatientName);
        String patsex = dataset.getString(Tags.PatientSex);
        String stuid = dataset.getString(Tags.StudyID);
        String accnum = dataset.getString(Tags.AccessionNumber);
        String studat = dataset.getString(Tags.StudyDate);
        String stuinsuid = dataset.getString(Tags.StudyInstanceUID);
        String mod = dataset.getString(Tags.Modality);
        String serinsuid = dataset.getString(Tags.SeriesInstanceUID);
        String sopinsuid = dataset.getString(Tags.SOPInstanceUID);
        String sql = "SELECT * FROM IMAGENS WHERE IMG_SOPINSUID = ? ";
        Connection con = getConnection(null);
        PreparedStatement statement = con.prepareStatement(sql);
        statement.setString(1, sopinsuid);
        boolean registroExistenteNoBanco = statement.executeQuery().next();
        statement.close();
        if (registroExistenteNoBanco)
        {
            return;
        }
        sql = "INSERT INTO IMAGENS(IMG_PATID,IMG_PATNAM,IMG_PATSEX,IMG_STUID,IMG_ACCNUM,IMG_STUDAT,IMG_STUINSUID,IMG_MOD,IMG_SERINSUID,IMG_SOPINSUID,IMG_PATH) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
        statement = con.prepareStatement(sql);
        statement.setString(1, patid);
        statement.setString(2, patnam);
        statement.setString(3, patsex);
        statement.setString(4, stuid);
        statement.setString(5, accnum);
        statement.setString(6, studat);
        statement.setString(7, stuinsuid);
        statement.setString(8, mod);
        statement.setString(9, serinsuid);
        statement.setString(10, sopinsuid);
        statement.setString(11, file.getAbsolutePath());
        statement.executeUpdate();
        statement.close();
        freeConnection(null, con);
    }

    private File getDir() throws java.io.IOException
    {
        String dirStr = System.getProperty("user.home") + File.separator + "miniwebpacs";
        File dir = new File(dirStr);
        if (!dir.exists())
        {
            dir.mkdirs();
        }
        /*
         File propsFile = new File(dir,"database.properties");
         if(!propsFile.exists()){
         java.io.InputStream in = getClass().getResourceAsStream("test.properties");
         java.io.OutputStream out = new java.io.FileOutputStream(propsFile);
         int i = 0;
         while ((i=in.read())!=-1) {
         out.write(i);
         }
         in.close();
         out.close();
         }
         File scriptFile = new File(dir,"database.script");
         if(!scriptFile.exists()){
         java.io.InputStream in = getClass().getResourceAsStream("test.script");
         System.out.println("in="+in);
         java.io.OutputStream out = new java.io.FileOutputStream(propsFile);
         int i = 0;
         while ((i=in.read())!=-1) {
         out.write(i);
         }
         in.close();
         out.close();
         }
         */
        return dir;
    }

}
