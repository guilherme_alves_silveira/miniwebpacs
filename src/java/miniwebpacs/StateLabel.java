/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.border.Border;
import javax.swing.BorderFactory;
import javax.imageio.ImageReader;
import java.awt.event.*;

/**
 * <p>
 * Title: </p>
 * <p>
 * Description: </p>
 * <p>
 * Copyright: Copyright (c) 2004</p>
 * <p>
 * Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class StateLabel extends JLabel
{

    private LabelGroup group = null;
    private Border border = BorderFactory.createLineBorder(java.awt.Color.RED);
    private int reader = 0;

    public StateLabel(LabelGroup group, ImageIcon icon)
    {
        this(group, icon, "");
    }

    public StateLabel(LabelGroup group, ImageIcon icon, String text)
    {
        this(group, icon, text, 0);
    }

    public StateLabel(LabelGroup group, ImageIcon icon, String text, int reader)
    {
        super(text, icon, JLabel.CENTER);
        this.group = group;
        this.reader = reader;
        this.setPreferredSize(new java.awt.Dimension(icon.getIconWidth(), icon.getIconHeight()));
        try
        {
            jbInit();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void jbInit() throws Exception
    {
        this.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mousePressed(MouseEvent e)
            {
                this_mousePressed(e);
            }
        });
    }

    public void select()
    {
        setBorder(border);
    }

    public void unselect()
    {
        setBorder(null);
    }

    public int getReader()
    {
        return reader;
    }

    void this_mousePressed(MouseEvent e)
    {
        if (e.getClickCount() == 1)
        {
            group.setSelected(this);
        }
        else if (e.getClickCount() > 1)
        {
            try
            {
                //dispara outro viewer...
                ImageReader rd = MainFrame.getInstance().getFileDataSource().getReader(reader);
                int sz = rd.getNumImages(true);
                if (sz > 1)
                {
                    PanelDinamico pd = new PanelDinamico(reader);
                    MainFrame.getInstance().setViewer(pd, false);
                    pd.start();
                }
                else
                {
                    PanelEstatico pe = new PanelEstatico(reader);
                    MainFrame.getInstance().setViewer(pe, true);
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }

}
