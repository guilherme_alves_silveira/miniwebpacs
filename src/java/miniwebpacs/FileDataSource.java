/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs;

import java.util.Vector;
import java.util.Iterator;
import java.awt.image.*;
import org.dcm4che.data.*;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import org.dcm4che.dict.Tags;

public class FileDataSource implements StoreListener
{

    Vector readers = new Vector();
    Vector files = new Vector();
    Vector listeners = new Vector();
    int selectedFile = 0;
    int selectedFrame = 0;
    private final static DcmObjectFactory oFact
            = DcmObjectFactory.getInstance();
    private final static DcmParserFactory pFact
            = DcmParserFactory.getInstance();

    public FileDataSource()
    {
    }

    //{implementacao da interface StoreListener

    public void received(java.io.File file)
    {
        try
        {
            Iterator iter = ImageIO.getImageReadersByFormatName("DICOM");
            ImageReader reader = (ImageReader) iter.next();
            ImageInputStream iis = ImageIO.createImageInputStream(file);
            reader.setInput(iis, false);
            readers.add(reader);
            files.add(file);
            fireDataChanged();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void started()
    {

    }

    public void finished()
    {
    }

    //implementacao da interface StoreListener}

    public int getSize()
    {
        return readers.size();
    }

    public int frameNumber(int i) throws java.io.IOException
    {
        return getReader(i).getNumImages(true);
    }

    public BufferedImage getFrameAt(int i, int pos) throws java.io.IOException
    {
        return getReader(i).read(pos);
    }

    public void addFileDataSourceListener(FileDataSourceListener sl)
    {
        listeners.add(sl);
    }

    public void removeFileDataSourceListener(FileDataSourceListener sl)
    {
        listeners.remove(sl);
    }

    public void clear()
    {//keep the listeners
        readers.clear();
        files.clear();
        selectedFile = 0;
        selectedFrame = 0;
        fireDataChanged();
    }

    protected ImageReader getReader(int i)
    {
        return (ImageReader) readers.get(i);
    }

    protected Dataset getDataset(int i)
    {
        try
        {
            java.io.File fl = (java.io.File) files.get(i);
            javax.imageio.stream.FileImageInputStream in = new javax.imageio.stream.FileImageInputStream(fl);
            Dataset ds = oFact.newDataset();
            DcmParser parser = pFact.newDcmParser(in);
            parser.setDcmHandler(ds.getDcmHandler());
            parser.parseDcmFile(null, Tags.PixelData);
            return ds;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    protected void fireDataChanged()
    {
        for (int i = 0; i < listeners.size(); i++)
        {
            FileDataSourceListener fdsl = (FileDataSourceListener) listeners.get(i);
            fdsl.dataChanged();
        }
    }
}
