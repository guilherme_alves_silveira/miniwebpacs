/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs;

import java.sql.*;
import org.dcm4che.data.Dataset;

public abstract class DB
{

    private static DB singleton = null;

    protected DB()
    {
    }

    public static DB getInstance()
    {
        if (singleton != null)
        {
            return singleton;
        }
        String implClass = System.getProperty("database.impl", "miniwebpacs.DBAccess");
        try
        {
            singleton = (DB) Class.forName(implClass).newInstance();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return singleton;
    }

    public abstract Connection getConnection(String conName);

    public abstract void freeConnection(String conName, Connection con);

    public abstract void store(Dataset dataset, java.io.File file) throws SQLException;

    //deprecated
    public abstract boolean hasExam(String stuinsuid) throws java.sql.SQLException;

    public abstract String[] listFiles(String stuinsuid) throws java.sql.SQLException;

}
