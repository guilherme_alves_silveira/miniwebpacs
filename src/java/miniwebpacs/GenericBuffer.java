/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs;

import java.util.*;

public class GenericBuffer
{

    private final Stack stack = new Stack();
    private int maxSize;

    public GenericBuffer()
    {
        this(10);
    }

    public GenericBuffer(int max)
    {
        maxSize = max;
    }

    private boolean isFull()
    {
        return stack.size() >= maxSize;
    }

    private boolean isEmpty()
    {
        return stack.isEmpty();
    }

    public synchronized void addObject(Object obj)
    {
        while (isFull())
        {
            try
            {
                // wait for Consumer to get value
                wait();
            }
            catch (InterruptedException e)
            {
            }
        }
        stack.push(obj);
        // notify Consumer that value has been set
        notifyAll();
    }

    public synchronized Object getObject()
    {
        while (isEmpty())
        {
            try
            {
                // wait for Producer to put value
                wait();
            }
            catch (InterruptedException e)
            {
            }
        }
        // notify Producer that value has been retrieved
        notifyAll();
        return stack.remove(0);
    }
}
