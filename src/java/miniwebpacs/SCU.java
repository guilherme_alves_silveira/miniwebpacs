/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 * ********************************************************************
 */
package miniwebpacs;

import java.io.*;
import java.security.GeneralSecurityException;
import org.dcm4che.net.Association;
import org.dcm4che.net.ActiveAssociation;
import org.dcm4che.dict.UIDs;
import org.apache.log4j.Logger;
import org.dcm4che.data.DcmObjectFactory;
import org.dcm4che.net.AssociationFactory;
import java.text.MessageFormat;
import java.net.Socket;
import org.dcm4che.net.PDU;
import org.dcm4che.net.AAssociateRQ;
import org.dcm4che.net.AAssociateAC;
import org.dcm4che.data.Dataset;
import org.dcm4che.dict.Tags;
import org.dcm4che.net.Dimse;
import org.dcm4che.net.FutureRSP;

public class SCU
{

    private final int repeatSingle = 1;
    private final int repeatWhole = 1;
    private static final String[] DEF_TS =
    {
        UIDs.ImplicitVRLittleEndian
    };
    private final static String[] TS =
    {
        UIDs.ExplicitVRLittleEndian,
        UIDs.ImplicitVRLittleEndian
    };
    private final static String STUDY_LABEL = "STUDY";
    private static final int PCID_ECHO = 1;
    private final static int PCID_FIND = 1;
    private final static int PCID_MOVE = 3;
    private static final Logger log = Logger.getLogger("DcmSCU");
    private static final DcmObjectFactory OFACT = DcmObjectFactory.getInstance();
    private static final AssociationFactory AFACT = AssociationFactory.getInstance();
    private final int acTimeout = 5000;
    private final int dimseTimeout = 0;
    private final int soCloseDelay = 500;
    private final boolean packPDVs = false;
    private Association assoc = null;//verificar se pode ser global...
    private final int priority = org.dcm4che.data.Command.MEDIUM;
    private final int port;
    private final String remoteAE;
    private final String localAE;
    private String host = null;

    public SCU(String host, int port, String remoteAE, String localAE)
    {
        this.host = host;
        this.port = port;
        this.remoteAE = remoteAE;
        this.localAE = localAE;
    }

    public void move(String stuinsuid) throws InterruptedException, IOException, GeneralSecurityException
    {
        AAssociateRQ assocRQ = AFACT.newAAssociateRQ();
        assocRQ.setCalledAET(remoteAE);
        assocRQ.setCallingAET(localAE);
        assocRQ.addPresContext(AFACT.newPresContext(PCID_MOVE,
                                                    UIDs.StudyRootQueryRetrieveInformationModelMOVE, TS));

        ActiveAssociation aassoc = openAssoc(host, port, assocRQ);

        org.dcm4che.data.Command rqCmd = OFACT.newCommand();
        rqCmd.initCMoveRQ(assoc.nextMsgID(),
                          UIDs.StudyRootQueryRetrieveInformationModelMOVE,
                          priority,
                          localAE);//estou fixando o destino como sendo este AE.
        Dataset rqDs = OFACT.newDataset();
        rqDs.putCS(Tags.QueryRetrieveLevel, STUDY_LABEL);
        rqDs.putUI(Tags.StudyInstanceUID, stuinsuid);
        Dimse moveRq = AFACT.newDimse(PCID_MOVE, rqCmd, rqDs);
        FutureRSP future = aassoc.invoke(moveRq);
        Dimse moveRsp = future.get();
        org.dcm4che.data.Command rspCmd = moveRsp.getCommand();
        int status = rspCmd.getStatus();
        switch (status)
        {
            case 0x0000:
                log.info("Moved " + stuinsuid);
                break;
            case 0xB000:
                log.error("One or more failures during move of " + stuinsuid);
                break;
            default:
                log.error("Failed to move " + stuinsuid
                        + "\n\terror tstatus: " + Integer.toHexString(status));
                break;
        }
    }

    public Dataset[] findStudy(int[] tags, String[] values) throws InterruptedException, IOException, GeneralSecurityException
    {
        AAssociateRQ assocRQ = AFACT.newAAssociateRQ();
        assocRQ.setCalledAET(remoteAE);
        assocRQ.setCallingAET(localAE);
        assocRQ.addPresContext(AFACT.newPresContext(PCID_FIND,
                                                    UIDs.StudyRootQueryRetrieveInformationModelFIND, TS));
        Dataset keys = OFACT.newDataset();
        keys.putCS(Tags.QueryRetrieveLevel, STUDY_LABEL);
        keys.putUS(Tags.NumberOfStudyRelatedSeries);
        keys.putUS(Tags.NumberOfStudyRelatedInstances);
        keys.putUI(Tags.StudyInstanceUID);
        keys.putLO(Tags.PatientID);
        keys.putPN(Tags.PatientName);
        keys.putDA(Tags.StudyDate);
        keys.putSH(Tags.StudyID);
        keys.putSH(Tags.AccessionNumber);
        if (tags != null && values != null)
        {
            for (int i = 0; i < tags.length; i++)
            {
                keys.putXX(tags[i], values[i]);
            }
        }
        ActiveAssociation aassoc = openAssoc(host, port, assocRQ);

        if (aassoc == null)
        {
            throw new IllegalStateException("No Association established");
        }
        int count = assoc.countAcceptedPresContext();
        if (count == 0)
        {
            System.out.println("Nenhuma associacao foi aceita!!!");
            return new Dataset[0];
        }
        org.dcm4che.data.Command rqCmd = OFACT.newCommand();
        rqCmd.initCFindRQ(assoc.nextMsgID(),
                          UIDs.StudyRootQueryRetrieveInformationModelFIND, priority);
        Dimse findRq = AFACT.newDimse(PCID_FIND, rqCmd, keys);
        FutureRSP future = aassoc.invoke(findRq);
        Dimse findRsp = future.get();
        java.util.List findRspList = future.listPending();
        Dataset[] response = new Dataset[findRspList.size()];
        for (int i = 0; i < findRspList.size(); i++)
        {
            Dimse findRsp1 = (Dimse) findRspList.get(i);
            response[i] = findRsp1.getDataset();
        }
        try
        {
            aassoc.release(false);
        }
        finally
        {
            assoc = null;
            aassoc = null;
        }
        return response;
    }

    public void echo() throws InterruptedException, IOException, GeneralSecurityException
    {
        AAssociateRQ assocRQ = AFACT.newAAssociateRQ();
        assocRQ.setCalledAET(remoteAE);
        assocRQ.setCallingAET(localAE);
        assocRQ.addPresContext(AFACT.newPresContext(PCID_ECHO, UIDs.Verification, DEF_TS));
        long t1 = System.currentTimeMillis();
        int count = 0;
        for (int i = 0; i < repeatWhole; ++i)
        {
            ActiveAssociation active = openAssoc(host, port, assocRQ);
            if (active != null)
            {
                if (active.getAssociation().getAcceptedTransferSyntaxUID(PCID_ECHO)
                        == null)
                {
                    log.error("noPCEcho");
                }
                else
                {
                    for (int j = 0; j < repeatSingle; ++j, ++count)
                    {
                        active.invoke(AFACT.newDimse(PCID_ECHO,
                                                     OFACT.newCommand().initCEchoRQ(j)), null);
                    }
                }
                active.release(true);
            }
        }
        long dt = System.currentTimeMillis() - t1;
        log.info(MessageFormat.format("echoDone", new Object[] { count, dt}));
    }

    private ActiveAssociation openAssoc(String host, int port, AAssociateRQ assocRQ) throws IOException, GeneralSecurityException
    {
        assoc = AFACT.newRequestor(
                new Socket(host, port));
        assoc.setAcTimeout(acTimeout);
        assoc.setDimseTimeout(dimseTimeout);
        assoc.setSoCloseDelay(soCloseDelay);
        assoc.setPackPDVs(packPDVs);

        PDU assocAC = assoc.connect(assocRQ);
        if (!(assocAC instanceof AAssociateAC))
        {
            return null;
        }
        ActiveAssociation retval = AFACT.newActiveAssociation(assoc, null);
        retval.start();
        return retval;
    }

}
