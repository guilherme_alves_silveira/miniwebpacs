/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import org.dcm4che.dict.Tags;
import org.dcm4che.data.Dataset;
import java.awt.event.*;

public class MainFrame extends JFrame
{
    private static final MainFrame singleton = new MainFrame();
    private BorderLayout borderLayout1 = new BorderLayout();
    private DicomRcv receiver = null;
    private String images_path = "";
    private JLabel jLabelStatus = new JLabel();
    private String host = "127.0.0.1";
    private int port = 3004;
    private String aeServer = "DCMSERVER";
    private String aeClient = "CLIENTE";
    private String stuinsuid = "";
    private JTabbedPane jTabbedPaneCentral = new JTabbedPane();
    private PanelError panelError = new PanelError();
    private PanelViewer panelViewer = new PanelViewer();
    private JToolBar currentToolbar = null;
    private FileDataSource dados = new FileDataSource();
    private Thumbnails thumbnails = null;
    private JMenuBar jMenuBar1 = new JMenuBar();
    private JMenu jMenu1 = new JMenu();
    private JMenuItem jMenuItemExit = new JMenuItem();
    private JMenu jMenu2 = new JMenu();
    private JMenuItem jMenuItemThumbs = new JMenuItem();

    public static void main(String[] args)
    {
        //host,porta,aeservidor,aecliente,stuinsuid
        MainFrame mainFrame = MainFrame.getInstance();
        mainFrame.init();
        if (args.length >= 5)
        {
            mainFrame.setParameters(args);
            try
            {
                mainFrame.contactServer();
            }
            catch (Exception ex)
            {
                JOptionPane.showConfirmDialog(mainFrame, "Falha ao contatar o servidor Dicom!!! Este programa ser� fechado...");
                System.exit(-1);
            }
        }
        else
        {
            mainFrame.setParameters(new String[]
            {
                "127.0.0.1", "5678", "CONQUESTSRV1", "CLIENTE", "1.3.46.670589.5.2.10.2156913941.892665384.993397"
            });
            try
            {
                mainFrame.contactServer();
            }
            catch (Exception ex)
            {
                JOptionPane.showConfirmDialog(mainFrame, "Falha ao contatar o servidor Dicom!!! Este programa ser� fechado...");
                System.exit(-1);
            }
        }
    }

    public static MainFrame getInstance()
    {
        return singleton;
    }

    public FileDataSource getFileDataSource()
    {
        return dados;
    }

    public void showException(Exception e)
    {
        showException(e, true);
    }

    public void showException(Exception e, boolean display)
    {
        panelError.add(e);
        if (display)
        {
            jTabbedPaneCentral.setSelectedComponent(panelError);
        }
    }

    public void addTab(String name, Component comp)
    {
        jTabbedPaneCentral.addTab(name, comp);
    }

    public void setViewer(Viewer v, boolean scroll)
    {
        if (currentToolbar != null)
        {
            getContentPane().remove(currentToolbar);
        }
        currentToolbar = v.getControls();
        if (currentToolbar != null)
        {
            getContentPane().add(currentToolbar, BorderLayout.NORTH);
        }
        panelViewer.setComponent(v.getGUI(), scroll);
        validate();
    }

    public void setStatus(String st)
    {
        jLabelStatus.setText("Status: " + st);
    }

    private void setParameters(String[] args)
    {
        host = args[0];
        port = Integer.parseInt(args[1]);
        aeServer = args[2];
        aeClient = args[3];
        stuinsuid = args[4];
    }

    private MainFrame()
    {
        try
        {
            jbInit();
        }
        catch (Exception e)
        {
            setStatus("Falha.");
            showException(e);
        }
    }

    private void jbInit() throws Exception
    {
        jLabelStatus.setText("Status:");
        this.setTitle("Visualizador Dicom MiniWEBPACS");
        this.addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(WindowEvent e)
            {
                this_windowClosing(e);
            }
        });
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setJMenuBar(jMenuBar1);
        this.getContentPane().setLayout(borderLayout1);
        jMenu1.setText("Arquivos");
        jMenuItemExit.setText("Sair");
        jMenuItemExit.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                jMenuItemExit_actionPerformed(e);
            }
        });
        jMenu2.setText("Visualizadores");
        jMenuItemThumbs.setText("Thumbnails");
        jMenuItemThumbs.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                jMenuItemThumbs_actionPerformed(e);
            }
        });
        this.getContentPane().add(jLabelStatus, BorderLayout.SOUTH);
        addTab("Visualizador", panelViewer);
        addTab("Erros", panelError);
        this.getContentPane().add(jTabbedPaneCentral, BorderLayout.CENTER);
        jMenuBar1.add(jMenu1);
        jMenuBar1.add(jMenu2);
        jMenu1.add(jMenuItemExit);
        jMenu2.add(jMenuItemThumbs);
        int iw = 800;
        int ih = 600;
        setSize(iw, ih);
        Dimension d = getToolkit().getScreenSize();
        setLocation((d.width - iw) / 2, (d.height - ih) / 2);
        setVisible(true);
        setStatus("Pronto.");
    }

    public void init()
    {
        thumbnails = new Thumbnails();
        setViewer(thumbnails, true);
        // Configura o banco a ser utilizado
        System.setProperty("database.impl", "miniwebpacs.DBHsqldb");
        // Configura o path dos arquivos.
        images_path = System.getProperty("user.home") + File.separator + "images";
        // Deveria tentar at� achar uma porta livre.
        receiver = new DicomRcv(105, images_path);
        receiver.addStoreListener(dados);
        try
        {
            receiver.start();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this, "Nao foi possivel iniciar o servidor local na porta 104. Este programa sera fechado...");
            System.exit(-1);
        }
    }

    public void contactServer() throws InterruptedException, java.io.IOException, java.security.GeneralSecurityException
    {
        try
        {
            String[] path = DB.getInstance().listFiles(stuinsuid);
            if (path.length > 0)
            {
                receiver.startedMove();
                for (int i = 0; i < path.length; i++)
                {
                    dados.received(new File(path[i]));
                }
                receiver.finishedMove();//avisa que terminou move...
                return;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return;
        }
        SCU scu = new SCU(host, port, aeServer, aeClient);
        Dataset[] list = scu.findStudy(new int[]
        {
            Tags.StudyInstanceUID
        }, new String[]
                               {
                                   stuinsuid
        });
        // a lista deve voltar somente 1 elemento...
        if (list == null || list.length == 0)
        {
            JOptionPane.showMessageDialog(this, "A imagem requisitada (" + stuinsuid + ") nao foi encontrada no servidor!", "Aten��o", JOptionPane.ERROR_MESSAGE);
            return;
        }
        receiver.startedMove();
        scu.move(stuinsuid);
        receiver.finishedMove();//avisa que terminou o move...
    }

    void this_windowClosing(WindowEvent e)
    {
        receiver.stop();
    }

    void jMenuItemExit_actionPerformed(ActionEvent e)
    {
        receiver.stop();
        System.exit(0);
    }

    void jMenuItemThumbs_actionPerformed(ActionEvent e)
    {
        setViewer(thumbnails, true);
    }

}
