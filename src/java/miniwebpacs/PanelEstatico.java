/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs;

import java.awt.*;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.JToolBar;
import java.awt.image.*;
import org.dcm4che.data.Dataset;

public class PanelEstatico extends JPanel implements Viewer
{
    private BorderLayout borderLayout1 = new BorderLayout();
    private BufferedImage image = null;
    private BufferedImage backBuffer = null;
    private int width = 0;
    private int height = 0;
    private WindowLevel wl = null;
    private JToolBar jtb = new JToolBar();
    private Zoom zoom = new Zoom();

    public PanelEstatico(int reader)
    {
        try
        {
            image = MainFrame.getInstance().getFileDataSource().getReader(reader).read(0);
            Dataset ds = MainFrame.getInstance().getFileDataSource().getDataset(reader);
            wl = new WindowLevel(ds);
            jbInit();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    void jbInit() throws Exception
    {
        this.setLayout(borderLayout1);
        jtb.add(wl.getGUI());
        jtb.add(zoom.getGUI());
    }

    /**
     * getGUI
     *
     * @return JComponent
     *
     * @todo Implement this miniwebpacs.Viewer method
     */
    @Override
    public JComponent getGUI()
    {
        return this;
    }

    /**
     * getControls
     *
     * @return JToolBar
     *
     * @todo Implement this miniwebpacs.Viewer method
     */
    @Override
    public JToolBar getControls()
    {
        return jtb;
    }

    @Override
    public void paint(Graphics g)
    {
        if (backBuffer == null || width != getWidth() || height != getHeight())
        {
            width = getWidth();
            height = getHeight();
            createBackBuffer();
        }
        Graphics gBB = backBuffer.getGraphics();
        gBB.setColor(Color.WHITE);
        gBB.fillRect(1, 1, getWidth() - 1, getHeight() - 1);
        gBB.setColor(Color.BLACK);
        //for( processor;;)
        BufferedImage processed = wl.processImage(image);
        BufferedImage processed2 = zoom.processImage(processed);
        // end for
        gBB.drawImage(processed2, 0, 0, this);
        //cria overlays....
        g.drawImage(backBuffer, 0, 0, this);
    }

    public void createBackBuffer()
    {
        backBuffer = (BufferedImage) createImage(width, height);
    }
}