/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs.server;

import java.sql.*;
import org.dcm4che.data.*;
import java.io.*;
import java.util.Vector;

public class DBAccess extends DB
{
    public DBAccess()
    {
        try
        {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public Connection getConnection(String conName)
    {
        try
        {
            return DriverManager.getConnection("jdbc:odbc:minipacsServer", "", "");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public void freeConnection(String conName, Connection con)
    {
        try
        {
            con.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public String[] listConnections()
    {
        return new String[]
        {
            "minipacsserver"
        };
    }

    @Override
    public DBWriter getDBWriter()
    {
        return new DBWriterAccess();
    }
    
    @Override
    public File getBaseFile(final String aeclient, final String aeserver)
    {
        String path = (String) doStatement(new Query()
        {
            @Override
            public String getSQL()
            {
                /*
                 SELECT AES_SAVE_PATH,
                 AXA_SAVE_PATH
                 FROM IMG_AEC_X_AES,
                 IMG_AE_SERVIDOR
                 WHERE AXA_AES_AETITLE = ?
                 AND   AXA_AEC_AETITLE = ?
                 AND   AXA_AES_AETITLE = AES_AETITLE
                 */
                return "SELECT AES_SAVE_PATH, "
                        + "       AXA_SAVE_PATH "
                        + "FROM IMG_AEC_X_AES, "
                        + "     IMG_AE_SERVIDOR "
                        + "WHERE AXA_AES_AETITLE = '" + aeserver + "' "
                        + "AND   AXA_AEC_AETITLE = '" + aeclient + "' "
                        + "AND   AXA_AES_AETITLE = AES_AETITLE ";
            }

            @Override
            public Object process(ResultSet rs) throws SQLException
            {
                String path = null;
                if (rs.next())
                {
                    String str = rs.getString(1);
                    String str2 = rs.getString(2);
                    if (str2 != null && !str.equals(""))
                    {
                        path = str2;
                    }
                    else
                    {
                        path = str;
                    }
                }
                return path;
            }
        });
        return new File(path);
    }

    @Override
    public String[] getCallingAEs(final String serverAE)
    {
        final Vector vec = new Vector();
        doStatement(new Query()
        {
            @Override
            public String getSQL()
            {
                /*
                 SELECT AXA_AEC_AETITLE
                 FROM IMG_AEC_X_AES
                 WHERE AXA_AES_AETITLE = ?
                 */
                return "SELECT AXA_AEC_AETITLE "
                        + "FROM IMG_AEC_X_AES  "
                        + "WHERE AXA_AES_AETITLE = '" + serverAE + "'";
            }

            public Object process(ResultSet rs) throws SQLException
            {
                while (rs.next())
                {
                    String str = rs.getString(1);
                    vec.add(str);
                }
                return vec;
            }
        });
        String[] resp = new String[vec.size()];
        vec.copyInto(resp);
        return resp;
    }

    public AEInfo[] getClientAEInfoByIP(final String ip)
    {
        final java.util.Vector vec = new java.util.Vector();
        doStatement(new Query()
        {
            public String getSQL()
            {
                /*
                 SELECT AEC_AETITLE,
                 AEC_IP,
                 AEC_PORT,
                 AEC_DESCRICAO
                 FROM IMG_AE_CLIENTE
                 WHERE AEC_IP = ?
                 */
                return "SELECT AEC_AETITLE, "
                        + "       AEC_IP, "
                        + "       AEC_PORT, "
                        + "       AEC_DESCRICAO "
                        + "FROM IMG_AE_CLIENTE "
                        + "WHERE AEC_IP = '" + ip + "'";
            }

            public Object process(ResultSet rs) throws SQLException
            {
                while (rs.next())
                {
                    AEInfo info = new AEInfo();
                    info.setAeTitle(rs.getString(1));
                    info.setIp(rs.getString(2));
                    info.setPort(rs.getInt(3));
                    info.setDescription(rs.getString(4));
                    vec.add(info);
                }
                return vec;
            }
        });
        AEInfo[] resp = new AEInfo[vec.size()];
        vec.copyInto(resp);
        return resp;
    }

    public AEInfo getClientAEInfoByIP(String server, String ip)
    {
        return null;
    }

    public AEInfo[] getClientAEInfo(final String aetitle)
    {
        final java.util.Vector vec = new java.util.Vector();
        doStatement(new Query()
        {
            public String getSQL()
            {
                /*
                 SELECT AEC_AETITLE,
                 AEC_IP,
                 AEC_PORT,
                 AEC_DESCRICAO
                 FROM IMG_AE_CLIENTE,
                 IMG_AEC_X_AES
                 WHERE AXA_AES_AETITLE = ?
                 AND AEC_AETITLE = AXA_AEC_AETITLE
                 */
                return "SELECT AEC_AETITLE, "
                        + "       AEC_IP, "
                        + "       AEC_PORT, "
                        + "       AEC_DESCRICAO "
                        + "FROM IMG_AE_CLIENTE, "
                        + "     IMG_AEC_X_AES  "
                        + "WHERE AXA_AES_AETITLE = '" + aetitle + "'"
                        + "AND AEC_AETITLE = AXA_AEC_AETITLE ";
            }

            public Object process(ResultSet rs) throws SQLException
            {
                while (rs.next())
                {
                    AEInfo info = new AEInfo();
                    info.setAeTitle(rs.getString(1));
                    info.setIp(rs.getString(2));
                    info.setPort(rs.getInt(3));
                    info.setDescription(rs.getString(4));
                    vec.add(info);
                }
                return vec;
            }
        });
        AEInfo[] resp = new AEInfo[vec.size()];
        vec.copyInto(resp);
        return resp;
    }

    public AEInfo getClientAEInfo(final String serverAE, final String clientAE)
    {
        AEInfo resp = (AEInfo) doStatement(new Query()
        {
            public String getSQL()
            {
                /*
                 SELECT AEC_AETITLE,
                 AEC_IP,
                 AEC_PORT,
                 AEC_DESCRICAO
                 FROM IMG_AE_CLIENTE,
                 IMG_AEC_X_AES
                 WHERE AXA_AES_AETITLE = ?
                 AND AEC_AETITLE = AXA_AEC_AETITLE
                 */
                return "SELECT AEC_AETITLE, "
                        + "       AEC_IP, "
                        + "       AEC_PORT, "
                        + "       AEC_DESCRICAO "
                        + "FROM IMG_AE_CLIENTE, "
                        + "     IMG_AEC_X_AES  "
                        + "WHERE AXA_AES_AETITLE = '" + serverAE + "'"
                        + "AND AXA_AEC_AETITLE = '" + clientAE + "'"
                        + "AND AEC_AETITLE = AXA_AEC_AETITLE ";
            }

            public Object process(ResultSet rs) throws SQLException
            {
                AEInfo info = null;
                if (rs.next())
                {
                    info = new AEInfo();
                    info.setAeTitle(rs.getString(1));
                    info.setIp(rs.getString(2));
                    info.setPort(rs.getInt(3));
                    info.setDescription(rs.getString(4));
                }
                return info;
            }
        });
        return resp;
    }

    public AEInfo[] getServerAEInfo(final int type)
    {
        final java.util.Vector vec = new java.util.Vector();
        doStatement(new Query()
        {
            @Override
            public String getSQL()
            {
                /*
                 SELECT AES_AETITLE,
                 AES_IP,
                 AES_PORT,
                 AES_DESCRICAO
                 FROM IMG_AE_SERVIDOR
                 WHERE AES_TIPO_SERVIDOR = ?
                 */
                return "SELECT AES_AETITLE, "
                        + "       AES_IP, "
                        + "       AES_PORT, "
                        + "       AES_DESCRICAO "
                        + "FROM IMG_AE_SERVIDOR "
                        + "WHERE AES_TIPO_SERVIDOR = " + type;
            }

            @Override
            public Object process(ResultSet rs) throws SQLException
            {
                while (rs.next())
                {
                    AEInfo info = new AEInfo();
                    info.setAeTitle(rs.getString(1));
                    info.setIp(rs.getString(2));
                    info.setPort(rs.getInt(3));
                    info.setDescription(rs.getString(4));
                    vec.add(info);
                }
                return vec;
            }
        });
        AEInfo[] resp = new AEInfo[vec.size()];
        vec.copyInto(resp);
        return resp;
    }

    @Override
    public Response getFindResponse(Dataset command)
    {
        return new FindResponseAccess(command);
    }

    @Override
    public Response getMoveResponse(Command command, Dataset dataset, String aetitle, String ip)
    {
        return new MoveResponseAccess(command, dataset, aetitle, ip);
    }

    private Object doStatement(Query q)
    {
        Connection con = getConnection(null);
        try
        {
            Object obj;
            try (Statement statement = con.createStatement())
            {
                ResultSet rs = statement.executeQuery(q.getSQL());
                obj = q.process(rs);
            }
            return obj;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
        finally
        {
            freeConnection(null, con);
        }
    }
}