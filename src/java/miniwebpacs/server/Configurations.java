/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs.server;

public abstract class Configurations
{
    private static Configurations singleton = null;

    protected Configurations()
    {
    }

    public static Configurations getInstance()
    {
        if (singleton == null)
        {
            try
            {
                String className = System.getProperty("configurations", "miniwebpacs.server.ConfigurationsDB");
                singleton = (Configurations) Class.forName(className).newInstance();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
        return singleton;
    }

    public String getString(String nome)
    {
        return getString(nome, null);
    }

    public String[] getStringArray(String nome)
    {
        return getStringArray(nome, null);
    }

    public int getInt(String nome)
    {
        return getInt(nome, -1);
    }

    public int[] getIntArray(String nome)
    {
        return getIntArray(nome, null);
    }

    public abstract String getString(String nome, String defVal);

    public abstract String[] getStringArray(String nome, String[] defVal);

    public abstract int getInt(String nome, int defVal);

    public abstract int[] getIntArray(String nome, int[] defVal);

}