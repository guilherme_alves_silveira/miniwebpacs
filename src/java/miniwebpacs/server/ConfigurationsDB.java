/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs.server;

import java.util.Vector;
import java.sql.*;

public class ConfigurationsDB extends Configurations
{

    public ConfigurationsDB()
    {
        //Vazio
    }

    /**
     * getInt
     *
     * @param nome String
     * @param defVal int
     *
     * @return int
     *
     * @todo Implement this br.usp.incor.minipacs.server.Configurations method
     */
    public int getInt(String nome, int defVal)
    {
        try
        {
            String[] resp = getParams(nome);
            if (resp.length == 0)
            {
                return defVal;
            }
            return Integer.parseInt(resp[0]);
        }
        catch (Exception ex)
        {
            return defVal;
        }
    }

    /**
     * getIntArray
     *
     * @param nome String
     * @param defVal int[]
     *
     * @return int[]
     *
     * @todo Implement this br.usp.incor.minipacs.server.Configurations method
     */
    public int[] getIntArray(String nome, int[] defVal)
    {
        try
        {
            String[] resp = getParams(nome);
            if (resp.length == 0)
            {
                return defVal;
            }
            int[] converted = new int[resp.length];
            for (int i = 0; i < resp.length; i++)
            {
                converted[i] = Integer.parseInt(resp[i]);
            }
            return converted;
        }
        catch (Exception ex)
        {
            return defVal;
        }
    }

    /**
     * getString
     *
     * @param nome String
     * @param defVal String
     *
     * @return String
     *
     * @todo Implement this br.usp.incor.minipacs.server.Configurations method
     */
    @Override
    public String getString(String nome, String defVal)
    {
        try
        {
            String[] resp = getParams(nome);
            if (resp.length == 0)
            {
                return defVal;
            }
            return resp[0];
        }
        catch (Exception ex)
        {
            return defVal;
        }
    }

    /**
     * getStringArray
     *
     * @param nome String
     * @param defVal String[]
     *
     * @return String[]
     *
     * @todo Implement this br.usp.incor.minipacs.server.Configurations method
     */
    @Override
    public String[] getStringArray(String nome, String[] defVal)
    {
        try
        {
            String[] resp = getParams(nome);
            if (resp.length == 0)
            {
                return defVal;
            }
            return resp;
        }
        catch (Exception ex)
        {
            return defVal;
        }
    }

    //-------------------------------------------------------------
    private String[] getParams(String name) throws SQLException
    {
        String sql = "SELECT CFG_VALOR,CFG_SEQUENCIA FROM IMG_CONFIGURACOES WHERE CFG_NOME = ? ORDER BY 2 ";
        Connection con = DB.getInstance().getConnection(null);
        try
        {
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setString(1, name);
            ResultSet rs = statement.executeQuery();
            Vector list = new Vector();
            while (rs.next())
            {
                String valor = rs.getString(1);
                list.add(valor);
            }
            statement.close();
            String[] resp = new String[list.size()];
            list.copyInto(resp);
            list.clear();
            return resp;
        }
        finally
        {
            DB.getInstance().freeConnection(null, con);
        }
    }

    public static void main(String[] args)
    {
        try
        {
            System.setProperty("database.impl", "miniwebpacs.server.DBAccess");
            ConfigurationsDB cdb = new ConfigurationsDB();
            String[] bah = cdb.getStringArray("syntax.img");
            for (int i = 0; i < bah.length; i++)
            {
                System.out.println("bah[" + i + "]=" + bah[i]);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

}
