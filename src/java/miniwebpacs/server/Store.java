/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs.server;

import org.dcm4che.data.Command;
import org.dcm4che.data.Dataset;
import org.dcm4che.data.DcmDecodeParam;
import org.dcm4che.data.DcmEncodeParam;
import org.dcm4che.data.DcmObjectFactory;
import org.dcm4che.data.DcmParser;
import org.dcm4che.data.DcmParseException;
import org.dcm4che.data.DcmParserFactory;
import org.dcm4che.data.FileFormat;
import org.dcm4che.dict.DictionaryFactory;
import org.dcm4che.dict.Tags;
import org.dcm4che.dict.UIDDictionary;
import org.dcm4che.dict.UIDs;
import org.dcm4che.dict.VRs;
import org.dcm4che.net.ActiveAssociation;
import org.dcm4che.net.Association;
import org.dcm4che.net.AAssociateAC;
import org.dcm4che.net.AAssociateRQ;
import org.dcm4che.net.AssociationFactory;
import org.dcm4che.net.DataSource;
import org.dcm4che.net.PDU;
import org.dcm4che.net.PresContext;

import java.io.File;
import java.io.EOFException;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.net.Socket;
import java.text.MessageFormat;
import java.security.GeneralSecurityException;

import org.apache.log4j.Logger;

public class Store
{
    private static final int BUFFER_SIZE = 2048;
    private static final int REPEAT_WHOLE = 1;
    private static final int REPEAT_SINGLE = 1;
    private static final int priority = Command.MEDIUM;
    private static final Logger log = Logger.getLogger("Move");
    private static final DcmParserFactory PFACT = DcmParserFactory.getInstance();
    private static final DcmObjectFactory OFACT = DcmObjectFactory.getInstance();
    private static final UIDDictionary UID_DICT = DictionaryFactory.getInstance().getDefaultUIDDictionary();
    private static final AssociationFactory AFACT = AssociationFactory.getInstance();
    
    private final AAssociateRQ assocRQ = AFACT.newAAssociateRQ();
    private final String remoteHost;
    private final int remotePort;
    
    private byte[] buffer = null;
    private int sentCount = 0;
    private long sentBytes = 0L;
    
    private final String[] tsNative = new String[]
    {
        UIDs.ExplicitVRLittleEndian, UIDs.ImplicitVRLittleEndian
    };
    
    private final String[] tsJpeg14 = new String[]
    {
        UIDs.JPEGLossless14
    };
    
    private final String[] tsJpegB = new String[]
    {
        UIDs.JPEGBaseline
    };

    public Store(String localAE, String remoteHost, String remoteAE, int remotePort)
    {
        this.remoteHost = remoteHost;
        this.remotePort = remotePort;
        assocRQ.setCalledAET(remoteAE);
        assocRQ.setCallingAET(localAE);
        String[] storage = Configurations.getInstance().getStringArray("sop.storage");
        initPresContext(storage);
    }

    private void initPresContext(String[] list)
    {
        int pcid = 1;
        for (String info : list)
        {
            String realID = UIDs.forName(info);
            assocRQ.addPresContext(AFACT.newPresContext(pcid, realID, tsNative));
            pcid = pcid + 2;
            assocRQ.addPresContext(AFACT.newPresContext(pcid, realID, tsJpeg14));
            pcid = pcid + 2;
            assocRQ.addPresContext(AFACT.newPresContext(pcid, realID, tsJpegB));
            pcid = pcid + 2;
        }
    }

    private ActiveAssociation openAssoc() throws IOException, GeneralSecurityException
    {
        Association assoc = AFACT.newRequestor(
                new Socket(remoteHost, remotePort));

        PDU assocAC = assoc.connect(assocRQ);
        if (!(assocAC instanceof AAssociateAC))
        {
            return null;
        }
        ActiveAssociation retval = AFACT.newActiveAssociation(assoc, null);
        retval.start();
        return retval;
    }

    public void send(String[] files) throws InterruptedException, IOException, GeneralSecurityException
    {
        if (BUFFER_SIZE > 0)
        {
            buffer = new byte[BUFFER_SIZE];
        }
        long t1 = System.currentTimeMillis();
        for (int i = 0; i < REPEAT_WHOLE; ++i)
        {
            ActiveAssociation active = openAssoc();
            if (active != null)
            {
                for (int k = 0; k < files.length; ++k)
                {
                    send(active, new File(files[k]));
                }
                active.release(true);
            }
        }
        long dt = System.currentTimeMillis() - t1;
        log.info(MessageFormat.format("sendDone", new Object[] { sentCount, sentBytes, dt, sentBytes / (1.024f * dt), }));
    }

    private void send(ActiveAssociation active, File file) throws InterruptedException, IOException
    {
        for (int i = 0; i < REPEAT_SINGLE; ++i)
        {
            sendFile(active, file);
        }
    }

    private boolean sendFile(ActiveAssociation active, File file) throws InterruptedException, IOException
    {
        DcmParser parser;
        Dataset ds;
        try (InputStream in = new BufferedInputStream(new FileInputStream(file)))
        {
            try
            {
                parser = PFACT.newDcmParser(in);
                FileFormat format = parser.detectFileFormat();
                if (format != null)
                {
                    ds = OFACT.newDataset();
                    parser.setDcmHandler(ds.getDcmHandler());
                    parser.parseDcmFile(format, Tags.PixelData);
                    if (parser.getReadTag() == Tags.PixelData)
                    {
                        if (parser.getStreamPosition() + parser.getReadLength()
                                > file.length())
                        {
                            throw new EOFException("Pixel Data Length: "
                                    + parser.getReadLength()
                                    + " exceeds file length: "
                                    + file.length());
                        }
                    }
                    log.info(
                            MessageFormat.format("readDone",
                                                 new Object[]
                                                 {
                                                     file
                                                 }));
                }
                else
                {
                    log.error(
                            MessageFormat.format("failformat",
                                                 new Object[]
                                                 {
                                                     file
                                                 }));
                    return false;
                }
            }
            catch (IOException e)
            {
                log.error(
                        MessageFormat.format("failread",
                                             new Object[]
                                             {
                                                 file, e
                                             }));
                return false;
            }
            sendDataset(active, file, parser, ds);
            return true;
        }
    }

    private boolean sendDataset(ActiveAssociation active, File file, DcmParser parser, Dataset ds) throws InterruptedException, IOException
    {
        String sopInstUID = ds.getString(Tags.SOPInstanceUID);
        if (sopInstUID == null)
        {
            log.error(
                    MessageFormat.format("noSOPinst",
                                         new Object[]
                                         {
                                             file
                                         }));
            return false;
        }
        String sopClassUID = ds.getString(Tags.SOPClassUID);
        if (sopClassUID == null)
        {
            log.error(
                    MessageFormat.format("noSOPclass",
                                         new Object[]
                                         {
                                             file
                                         }));
            return false;
        }
        
        PresContext pc;
        Association assoc = active.getAssociation();
        if (parser.getDcmDecodeParam().encapsulated)
        {
            String tsuid = ds.getFileMetaInfo().getTransferSyntaxUID();
            if ((pc = assoc.getAcceptedPresContext(sopClassUID, tsuid))
                    == null)
            {
                log.error(
                        MessageFormat.format("noPCStore3",
                                             new Object[]
                                             {
                                                 UID_DICT.lookup(sopClassUID),
                                                 UID_DICT.lookup(tsuid), file
                                             }));
                return false;
            }
        }
        else if ((pc = assoc.getAcceptedPresContext(sopClassUID,
                                                    UIDs.ImplicitVRLittleEndian)) == null
                && (pc = assoc.getAcceptedPresContext(sopClassUID,
                                                      UIDs.ExplicitVRLittleEndian)) == null
                && (pc = assoc.getAcceptedPresContext(sopClassUID,
                                                      UIDs.ExplicitVRBigEndian)) == null)
        {
            log.error(
                    MessageFormat.format("noPCStore2",
                                         new Object[]
                                         {
                                             UID_DICT.lookup(sopClassUID), file
                                         }));
            return false;

        }
        active.invoke(AFACT.newDimse(pc.pcid(),
                                     OFACT.newCommand().initCStoreRQ(assoc.nextMsgID(),
                                                                     sopClassUID, sopInstUID, priority),
                                     new ParserDataSource(parser, ds, buffer)), null);
        sentBytes += parser.getStreamPosition();
        ++sentCount;
        return true;
    }

    private static final class ParserDataSource implements DataSource
    {
        private final DcmParser parser;
        private final Dataset ds;
        private final byte[] buffer;

        private ParserDataSource(DcmParser parser, Dataset ds, byte[] buffer)
        {
            this.parser = parser;
            this.ds = ds;
            this.buffer = buffer;
        }

        @Override
        public void writeTo(OutputStream out, String tsUID) throws IOException
        {
            DcmEncodeParam netParam
                    = (DcmEncodeParam) DcmDecodeParam.valueOf(tsUID);
            ds.writeDataset(out, netParam);
            if (parser.getReadTag() == Tags.PixelData)
            {
                DcmDecodeParam fileParam = parser.getDcmDecodeParam();
                ds.writeHeader(out, netParam,
                               parser.getReadTag(),
                               parser.getReadVR(),
                               parser.getReadLength());
                if (netParam.encapsulated)
                {
                    parser.parseHeader();
                    while (parser.getReadTag() == Tags.Item)
                    {
                        ds.writeHeader(out, netParam,
                                       parser.getReadTag(),
                                       parser.getReadVR(),
                                       parser.getReadLength());
                        writeValueTo(out, false);
                    }
                    if (parser.getReadTag() != Tags.SeqDelimitationItem)
                    {
                        throw new DcmParseException("Unexpected Tag:"
                                + Tags.toString(parser.getReadTag()));
                    }
                    if (parser.getReadLength() != 0)
                    {
                        throw new DcmParseException("(fffe,e0dd), Length:"
                                + parser.getReadLength());
                    }
                    ds.writeHeader(out, netParam,
                                   Tags.SeqDelimitationItem, VRs.NONE, 0);
                }
                else
                {
                    boolean swap = fileParam.byteOrder != netParam.byteOrder
                            && parser.getReadVR() == VRs.OW;
                    writeValueTo(out, swap);
                }
                ds.clear();
                try
                {
                    parser.parseDataset(fileParam, -1);
                }
                catch (IOException e)
                {
                    log.warn("Error reading post-pixeldata attributes:", e);
                }
                ds.writeDataset(out, netParam);
            }
        }

        private void writeValueTo(OutputStream out, boolean swap) throws IOException
        {
            InputStream in = parser.getInputStream();
            int len = parser.getReadLength();
            if (swap && (len & 1) != 0)
            {
                throw new DcmParseException("Illegal length of OW Pixel Data: " + len);
            }
            
            if (null == buffer)
            {
                if (swap)
                {
                    int tmp;
                    for (int i = 0; i < len; ++i, ++i)
                    {
                        tmp = in.read();
                        out.write(in.read());
                        out.write(tmp);
                    }
                }
                else
                {
                    for (int i = 0; i < len; ++i)
                    {
                        out.write(in.read());
                    }
                }
            }
            else
            {
                byte tmp;
                int c, remain = len;
                while (remain > 0)
                {
                    c = in.read(buffer, 0, Math.min(buffer.length, remain));
                    if (c == -1)
                    {
                        throw new EOFException("EOF during read of pixel data");
                    }
                    if (swap)
                    {
                        if ((c & 1) != 0)
                        {
                            buffer[c++] = (byte) in.read();
                        }
                        for (int i = 0; i < c; ++i, ++i)
                        {
                            tmp = buffer[i];
                            buffer[i] = buffer[i + 1];
                            buffer[i + 1] = tmp;
                        }
                    }
                    out.write(buffer, 0, c);
                    remain -= c;
                }
            }
            
            parser.setStreamPosition(parser.getStreamPosition() + len);
        }
    }
}