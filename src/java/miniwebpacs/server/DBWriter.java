/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs.server;

import org.dcm4che.data.Dataset;
import java.util.LinkedList;
import java.io.File;
import java.sql.*;

public abstract class DBWriter implements Runnable
{
    private final LinkedList<Runnable> queue = new LinkedList<>();
    private Thread thread = null;

    public DBWriter()
    {
        //Vazio
    }

    public void start()
    {
        thread = new Thread(this);
        thread.start();
    }

    public void stop()
    {
        if (thread != null)
        {
            thread.interrupt();
        }
    }

    public void schedule(final File file, final Dataset ds, final String callingAE, final String calledAE)
    {
        synchronized (queue)
        {
            queue.addLast(() ->
            {
                try
                {
                    update(file, ds, callingAE, calledAE);
                }
                catch (SQLException ex)
                {
                    ex.printStackTrace();
                }
            });
            
            queue.notify();
        }
    }

    @Override
    public void run()
    {
        try
        {
            for (;;)
            {
                getJob().run();
            }
        }
        catch (InterruptedException ie)
        {
            ie.printStackTrace();
        }
    }

    private Runnable getJob() throws InterruptedException
    {
        synchronized (queue)
        {
            while (queue.isEmpty())
            {
                queue.wait();
            }
            return (Runnable) queue.removeFirst();
        }
    }

    protected abstract void update(File file, Dataset ds, String callingAE, String calledAE) throws SQLException;
}