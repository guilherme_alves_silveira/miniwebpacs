/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs.server;

import org.dcm4che.data.DcmObjectFactory;
import org.dcm4che.data.DcmElement;
import org.dcm4che.data.Dataset;
import org.dcm4che.data.Command;
import org.dcm4che.dict.Tags;
import java.util.Iterator;
import java.util.Vector;
import java.sql.*;
import java.text.SimpleDateFormat;

public class FindResponseAccess implements Response
{

    private static final DcmObjectFactory OFACT = DcmObjectFactory.getInstance();
    private Dataset[] resp = null;
    private int pointer = -1;

    public FindResponseAccess(Dataset command)
    {
        Connection con = DB.getInstance().getConnection(null);
        try
        {
            /*
             SELECT MAP_TABLE
             ,      MAP_COLUMN
             ,      MAP_TYPE
             FROM IMG_MAP_TABLES
             ,    IMG_DICOM_DICT
             WHERE DIC_ID = MAP_DIC_ID
             AND DIC_GRUPO = ?
             AND DIC_ELEMENTO = ?
             */
            String sql = "SELECT MAP_TABLE "
                    + ",      MAP_COLUMN "
                    + ",      MAP_TYPE "
                    + "FROM IMG_MAP_TABLES "
                    + ",    IMG_DICOM_DICT "
                    + "WHERE DIC_ID = MAP_DIC_ID "
                    + "AND DIC_GRUPO = ? "
                    + "AND DIC_ELEMENTO = ? ";
            Vector<Integer> foundTags;
            Vector<Integer> foundTypes;
            Vector<Integer> missingTags;
            Vector<String> tables;
            String select;
            String from;
            String where;
            try (PreparedStatement statement = con.prepareStatement(sql))
            {
                foundTags = new Vector<>();
                foundTypes = new Vector<>();
                missingTags = new Vector<>();
                tables = new Vector<>();
                select = "SELECT ";
                from = "FROM ";
                where = "WHERE ";
                for (Iterator it = command.iterator(); it.hasNext();)
                {
                    DcmElement el = (DcmElement) it.next();
                    int tag = el.tag();
                    String group = Tags.toHexString(tag >>> 16, 4);
                    String element = Tags.toHexString(tag & 0xffff, 4);
                    statement.setString(1, group);
                    statement.setString(2, element);
                    ResultSet rs = statement.executeQuery();
                    if (rs.next())
                    {
                        foundTags.add(tag);
                        String tabela = rs.getString(1);
                        String coluna = rs.getString(2);
                        select += tabela + "." + coluna + ",";
                        if (!tables.contains(tabela))
                        {
                            tables.add(tabela);
                            from += tabela + ",";
                        }
                        int tipo = rs.getInt(3);
                        foundTypes.add(tipo);
                        String value = el.getString(null);
                        if (value != null && !value.equals(""))
                        {
                            //tratar wild cards (?,*) e range de datas
                            if (tipo != 1)
                            {
                                where += tabela + "." + coluna + "='" + value + "' AND ";
                            }
                            else
                            {
                                where += tabela + "." + coluna + "=" + value + " AND ";
                            }
                        }
                    }
                    else
                    {
                        missingTags.add(tag);
                    }
                }
            }
            if (foundTags.isEmpty())
            {
                return;
            }
            
            String and = "";
            if (tables.contains("IMG_IMAGELEVEL"))
            {
                and = " IMG_IMAGELEVEL.SERINSUID = IMG_SERIESLEVEL.SERINSUID "
                        + " AND IMG_SERIESLEVEL.STUINSUID = IMG_STUDYLEVEL.STUINSUID ";
                if (tables.contains("IMG_PATIENTLEVEL"))
                {
                    and += " AND IMG_STUDYLEVEL.PACI_ID = IMG_PATIENTLEVEL.PACI_ID ";
                }
            }
            else if (tables.contains("IMG_SERIESLEVEL"))
            {
                and = " IMG_SERIESLEVEL.STUINSUID = IMG_STUDYLEVEL.STUINSUID ";
                if (tables.contains("IMG_PATIENTLEVEL"))
                {
                    and += " AND IMG_STUDYLEVEL.PACI_ID = IMG_PATIENTLEVEL.PACI_ID ";
                }
            }
            else if (tables.contains("IMG_STUDYLEVEL"))
            {
                if (tables.contains("IMG_PATIENTLEVEL"))
                {
                    and = " IMG_STUDYLEVEL.PACI_ID = IMG_PATIENTLEVEL.PACI_ID ";
                }
            }
            
            String createdSQL;
            if ("WHERE ".equals(where))
            {
                createdSQL = select.substring(0, select.length() - 1)
                        + " " + from.substring(0, from.length() - 1);
                if (!"".equals(and))
                {
                    createdSQL += " WHERE " + and;
                }
            }
            else
            {
                createdSQL = select.substring(0, select.length() - 1)
                        + " " + from.substring(0, from.length() - 1)
                        + " " + where.substring(0, where.length() - 4);
                if (!"".equals(and))
                {
                    createdSQL += " AND " + and;
                }
            }
            
            System.out.println("sql = " + createdSQL);
            Vector<Dataset> response;
            try (Statement statement = con.createStatement())
            {
                ResultSet rs = statement.executeQuery(createdSQL);
                SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
                response = new Vector<>();
                while (rs.next())
                {
                    Dataset dataset = OFACT.newDataset();
                    for (int count = 0; count < foundTags.size(); count++)
                    {
                        int tag = foundTags.get(count);
                        int tipo = foundTypes.get(count);
                        String val = null;
                        switch (tipo)
                        {
                            case 0:
                                val = rs.getString(count + 1);
                                break;
                            case 1:
                                val = String.valueOf(rs.getInt(count + 1));
                                break;
                            case 2:
                                java.sql.Date dt = rs.getDate(count + 1);
                                System.out.println("date=" + dt);
                                if (dt == null)
                                {
                                    val = "00000000";
                                }
                                else
                                {
                                    val = formatter.format(dt);
                                }
                                break;
                            default:
                                System.out.println("Valor nao esperado!!! " + tipo);
                        }
                        //colocar online aqui!!!
                        dataset.putXX(tag, val);
                    }
                    for (Integer missingTag : missingTags)
                    {
                        int tag = missingTag;
                        if (tag != Tags.QueryRetrieveLevel)
                        {
                            dataset.putXX(tag);
                        }
                    }
                    
                    response.add(dataset);
                }
            }
            System.out.println("resp size: " + response.size());
            resp = new Dataset[response.size()];
            response.copyInto(resp);
            response.clear();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            DB.getInstance().freeConnection(null, con);
        }
    }

    @Override
    public void close()
    {
        //Vazio
    }

    @Override
    public Command getCommand()
    {
        return null;
    }

    @Override
    public Dataset getDataset()
    {
        if (resp == null)
        {
            return null;
        }
        if (pointer >= resp.length)
        {
            return null;
        }
        return resp[pointer];
    }

    @Override
    public void next()
    {
        pointer++;
    }
}