/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs.server;

import java.io.BufferedOutputStream;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.log4j.Logger;
import org.dcm4che.data.Command;
import org.dcm4che.data.Dataset;
import org.dcm4che.data.DcmDecodeParam;
import org.dcm4che.data.DcmEncodeParam;
import org.dcm4che.data.DcmObjectFactory;
import org.dcm4che.data.DcmParser;
import org.dcm4che.data.DcmParserFactory;
import org.dcm4che.data.FileMetaInfo;
import org.dcm4che.dict.Status;
import org.dcm4che.dict.Tags;
import org.dcm4che.dict.UIDs;
import org.dcm4che.net.AcceptorPolicy;
import org.dcm4che.net.ActiveAssociation;
import org.dcm4che.net.AssociationFactory;
import org.dcm4che.net.DcmServiceBase;
import org.dcm4che.net.DcmServiceRegistry;
import org.dcm4che.net.DcmServiceException;
import org.dcm4che.net.Dimse;
import org.dcm4che.server.DcmHandler;
import org.dcm4che.server.Server;
import org.dcm4che.server.ServerFactory;

/*

 Modificacao da classe DcmRcv, exemplo do dcm4che.
 * @author     <a href="mailto:ramon.moreno@incor.usp.br">ramon moreno</a>
 * @author     <a href="mailto:gunter@tiani.com">gunter zeilinger</a>
 */
public class DicomServer extends DcmServiceBase
{
    private static final int RPS_DELAY = 0;
    private static final int BUFFER_SIZE = 512;
    private static final  Logger log = Logger.getLogger(DicomServer.class);
    private static final ServerFactory SRV_FACT = ServerFactory.getInstance();
    private static final AssociationFactory FACT = AssociationFactory.getInstance();
    private static final DcmParserFactory PFACT = DcmParserFactory.getInstance();
    private static final DcmObjectFactory OFACT = DcmObjectFactory.getInstance();

    private final AcceptorPolicy policy = FACT.newAcceptorPolicy();
    private final DcmServiceRegistry services = FACT.newDcmServiceRegistry();
    private final DcmHandler handler = SRV_FACT.newDcmHandler(policy, services);
    private final Server server = SRV_FACT.newServer(handler);
    private int port = 104;
    private String serverAETitle = null;
    private DBWriter dbWriter = null;

    public DicomServer(String aetitle, int port)
    {
        this.serverAETitle = aetitle;
        this.port = port;
        initServer();
    }

    private void initServer()
    {
        dbWriter = DB.getInstance().getDBWriter();
        String[] syntaxIMG = Configurations.getInstance().getStringArray("syntax.img");
        String[] storage = Configurations.getInstance().getStringArray("sop.storage");
        String[] syntaxFnd = Configurations.getInstance().getStringArray("syntax.find");
        String[] find = Configurations.getInstance().getStringArray("sop.find");
        server.setPort(port);
        server.setMaxClients(10);
        policy.setCalledAETs(new String[]
        {
            serverAETitle
        });
        policy.setCallingAETs(DB.getInstance().getCallingAEs(serverAETitle));
        initPresContext(storage, syntaxIMG);
        initPresContext(find, syntaxFnd);
    }

    private void initPresContext(String[] asNames, String[] tsNames)
    {
        for (String asName : asNames)
        {
            initPresContext(asName, tsNames);
        }
    }

    private void initPresContext(String asName, String[] tsNames)
    {
        String as = UIDs.forName(asName);
        String[] tsUIDs = new String[tsNames.length];
        for (int i = 0; i < tsUIDs.length; ++i)
        {
            tsUIDs[i] = UIDs.forName(tsNames[i]);
        }
        policy.putPresContext(as, tsUIDs);
        services.bind(as, this);
    }

    /**
     * Description of the Method
     *
     * @exception IOException Description of the Exception
     */
    public void start() throws IOException
    {
        if (dbWriter != null)
        {
            dbWriter.start();
        }
        server.start();
    }

    public void stop()
    {
        System.out.println("STOPPING");
        server.stop();
    }

    /**
     * DcmServiceBase overrides --------------------------------------
     * Description of the Method
     * @param assoc Description of the Parameter
     * @param rq Description of the Parameter
     * @param rspCmd Description of the Parameter
     *
     * @exception IOException Description of the Exception
     */
    @Override
    protected void doCStore(ActiveAssociation assoc, Dimse rq, Command rspCmd) throws IOException
    {
        // who is calling
        String callingAE = assoc.getAssociation().getCallingAET();
        try (InputStream in = rq.getDataAsStream())
        {
            Command rqCmd = rq.getCommand();
            FileMetaInfo fmi = objFact.newFileMetaInfo(
                    rqCmd.getAffectedSOPClassUID(),
                    rqCmd.getAffectedSOPInstanceUID(),
                    rq.getTransferSyntaxUID());
            //save it in file and in DB
            storeToDir(in, fmi, callingAE);
        }
        catch (IOException | java.sql.SQLException ex)
        {
            ex.printStackTrace();
        }
        
        if (RPS_DELAY > 0L)
        {
            try
            {
                Thread.sleep(RPS_DELAY);
            }
            catch (InterruptedException ex)
            {
                ex.printStackTrace();
            }
        }
        
        rspCmd.putUS(Tags.Status, Status.Success);
    }

    /**
     * DcmServiceBase overwrites -------------------------------------
     * Description of the Method
     *
     * @param assoc Description of the Parameter
     * @param rq Description of the Parameter
     * @param rspCmd Description of the Parameter
     *
     * @return Description of the Return Value
     *
     * @exception IOException Description of the Exception
     * @exception DcmServiceException Description of the Exception
     */
    @Override
    protected MultiDimseRsp doCFind(ActiveAssociation assoc, Dimse rq, Command rspCmd) throws IOException, DcmServiceException
    {
        System.out.println("Requisitou C-FIND!");
        Dataset ds = rq.getDataset();// read out dataset
        try
        {
            return new FindMultiDimseRsp(ds);
        }
        catch (Exception e)
        {
            throw new DcmServiceException(Status.UnableToProcess, e);
        }
    }

    @Override
    protected MultiDimseRsp doCMove(ActiveAssociation assoc, Dimse rq, Command rspCmd) throws IOException, DcmServiceException
    {
        System.out.println("Requisitou C-MOVE!");
        try
        {
            Command c = rq.getCommand();
            Dataset ds = rq.getDataset();// read out dataset
            String ip = assoc.getAssociation().getSocket().getInetAddress().getHostAddress();
            return new MoveMultiDimseRsp(c, ds, serverAETitle, ip);
        }
        catch (Exception e)
        {
            throw new DcmServiceException(Status.UnableToProcess, e);
        }
    }

    private OutputStream openOutputStream(File file) throws IOException
    {
        File parent = file.getParentFile();
        if (!parent.exists())
        {
            if (!parent.mkdirs())
            {
                throw new IOException("Could not create " + parent);
            }
            log.info("M-WRITE " + parent);
        }
        log.info("M-WRITE " + file);
        return new BufferedOutputStream(new FileOutputStream(file));
    }

    private void storeToDir(InputStream in, FileMetaInfo fmi, String aetitle) throws IOException, java.sql.SQLException
    {
        // Obtain the root path where to store the image
        File dir = DB.getInstance().getBaseFile(aetitle, serverAETitle);
        // Convert the incoming request into a dataset
        Dataset ds = OFACT.newDataset();
        DcmParser parser = PFACT.newDcmParser(in);
        parser.setDcmHandler(ds.getDcmHandler());
        DcmDecodeParam decParam = DcmDecodeParam.valueOf(fmi.getTransferSyntaxUID());
        parser.parseDataset(decParam, Tags.PixelData);
        // Obtain the path where to save the file
        String[] names = Configurations.getInstance().getStringArray("storage.path");
        // Concat the dir with the path
        File file = new Util().toFile(ds, names, dir);
        // Save the file
        try (OutputStream out = openOutputStream(file))
        {
            ds.setFileMetaInfo(fmi);
            ds.writeFile(out, (DcmEncodeParam) decParam);
            if (parser.getReadTag() != Tags.PixelData)
            {
                return;
            }
            ds.writeHeader(out, (DcmEncodeParam) decParam,
                           parser.getReadTag(),
                           parser.getReadVR(),
                           parser.getReadLength());
            copy(in, out);
        }
        
        // Schedule to write in database
        // and generate thumbanail or anything similar
        dbWriter.schedule(file, ds, aetitle, serverAETitle);
    }

    private void copy(InputStream in, OutputStream out) throws IOException
    {
        if (BUFFER_SIZE > 0)
        {
            byte[] buffer = new byte[BUFFER_SIZE];
            int c;
            while ((c = in.read(buffer)) != -1)
            {
                out.write(buffer, 0, c);
            }
        }
        else
        {
            int ch;
            while ((ch = in.read()) != -1)
            {
                out.write(ch);
            }
        }
    }

    public static void main(String[] args)
    {
        System.setProperty("database.impl", "miniwebpacs.server.DBPostgre");
        System.setProperty("configurations", "miniwebpacs.server.ConfigurationsDB");
        AEInfo[] list = DB.getInstance().getServerAEInfo(DB.SRV_ARMAZENAMENTO);
        DicomServer rcv = new DicomServer(list[0].getAeTitle(), list[0].getPort());
        try
        {
            rcv.start();
            System.out.println("Iniciado.");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}