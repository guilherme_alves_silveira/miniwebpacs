/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Fundação Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs.server;

import org.dcm4che.data.DcmObjectFactory;
import org.dcm4che.data.Dataset;
import org.dcm4che.data.Command;
import org.dcm4che.dict.Tags;
import org.dcm4che.dict.Status;
import java.util.Vector;
import java.sql.*;

public class MoveResponseAccess implements Response
{
    /*
     SELECT IMG_PATH
     FROM IMG_IMAGELEVEL
     ,    IMG_SERIESLEVEL
     ,    IMG_STUDYLEVEL
     ,    IMG_PATIENTLEVEL
     WHERE IMG_IMAGELEVEL.SERINSUID = IMG_SERIESLEVEL.SERINSUID
     AND IMG_SERIESLEVEL.STUINSUID = IMG_STUDYLEVEL.STUINSUID
     AND IMG_STUDYLEVEL.PACI_ID = IMG_PATIENTLEVEL.PACI_ID
     AND PACI_ID = ?

     SELECT IMG_PATH
     FROM IMG_IMAGELEVEL
     ,    IMG_SERIESLEVEL
     ,    IMG_STUDYLEVEL
     WHERE IMG_IMAGELEVEL.SERINSUID = IMG_SERIESLEVEL.SERINSUID
     AND IMG_SERIESLEVEL.STUINSUID = IMG_STUDYLEVEL.STUINSUID
     AND STUINSUID = ?

     SELECT IMG_PATH
     FROM IMG_IMAGELEVEL
     ,    IMG_SERIESLEVEL
     WHERE IMG_IMAGELEVEL.SERINSUID = IMG_SERIESLEVEL.SERINSUID
     AND SERINSUID = ?

     SELECT IMG_PATH
     FROM IMG_IMAGELEVEL
     WHERE SOPINSUID = ?
     */
    private static final  String SQL_IMAGE_PATIENT = "SELECT IMG_PATH "
                                                   + "FROM IMG_IMAGELEVEL "
                                                   + ",    IMG_SERIESLEVEL "
                                                   + ",    IMG_STUDYLEVEL "
                                                   + ",    IMG_PATIENTLEVEL "
                                                   + "WHERE IMG_IMAGELEVEL.SERINSUID = IMG_SERIESLEVEL.SERINSUID "
                                                   + "AND IMG_SERIESLEVEL.STUINSUID = IMG_STUDYLEVEL.STUINSUID "
                                                   + "AND IMG_STUDYLEVEL.PACI_ID = IMG_PATIENTLEVEL.PACI_ID "
                                                   + "AND IMG_PATIENTLEVEL.PACI_ID = ? ";

    private static final  String SQL_IMAGE_STUDY = "SELECT IMG_PATH "
                                                 + "FROM IMG_IMAGELEVEL "
                                                 + ",    IMG_SERIESLEVEL "
                                                 + ",    IMG_STUDYLEVEL "
                                                 + "WHERE IMG_IMAGELEVEL.SERINSUID = IMG_SERIESLEVEL.SERINSUID  "
                                                 + "AND IMG_SERIESLEVEL.STUINSUID = IMG_STUDYLEVEL.STUINSUID  "
                                                 + "AND IMG_STUDYLEVEL.STUINSUID = ? ";

    private static final  String SQL_IMAGE_SERIES = "SELECT IMG_PATH "
                                                  + "FROM IMG_IMAGELEVEL "
                                                  + ",    IMG_SERIESLEVEL "
                                                  + "WHERE IMG_IMAGELEVEL.SERINSUID = IMG_SERIESLEVEL.SERINSUID  "
                                                  + "AND IMG_IMAGELEVEL.SERINSUID = ? ";

    private static final  String SQL_IMAGE = "SELECT IMG_PATH "
                                           + "FROM IMG_IMAGELEVEL "
                                           + "WHERE SOPINSUID = ? ";

    private static final DcmObjectFactory OFACT = DcmObjectFactory.getInstance();
    private Vector<String> paths = new Vector<>();
    private int pointer = -1;
    private String aeToCall = null;
    private String localAE = null;
    private int success = 0;
    private int failed = 0;
    private String ipToCall = null;

    public MoveResponseAccess(Command command, Dataset dataset, String aetitle, String ip)
    {
        localAE = aetitle;
        aeToCall = command.getString(Tags.MoveDestination, "");
        ipToCall = ip;
        String sql = null;
        String list[] = null;
        String patientId = dataset.getString(Tags.PatientID);
        String[] studyIds = dataset.getStrings(Tags.StudyInstanceUID);
        String[] seriesIds = dataset.getStrings(Tags.SeriesInstanceUID);
        String[] imageIds = dataset.getStrings(Tags.SOPInstanceUID);
        /*
         We'll do the following:
         1. If it contains the lower levels, i answer then. If not,
         I try a higher level.
         */
        if (patientId != null && !patientId.equals(""))
        {
            sql = SQL_IMAGE_PATIENT;
            list = new String[] { patientId };
        }
        
        if (studyIds != null && studyIds.length > 0)
        {
            sql = SQL_IMAGE_STUDY;
            list = studyIds;
        }
        
        if (seriesIds != null && seriesIds.length > 0)
        {
            sql = SQL_IMAGE_SERIES;
            list = seriesIds;
        }
        
        if (imageIds != null && imageIds.length > 0)
        {
            sql = SQL_IMAGE;
            list = imageIds;
        }
        
        System.out.println("sql = " + sql);
        if (list == null)
        {
            return;
        }
        Connection con = DB.getInstance().getConnection(null);
        try
        {
            try (PreparedStatement statement = con.prepareStatement(sql))
            {
                process(statement, list);
                System.out.println("size: " + paths.size());
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            DB.getInstance().freeConnection(null, con);
        }
    }

    @Override
    public void close()
    {
        //Vazio
    }

    @Override
    public Command getCommand()
    {
        if (paths.isEmpty())
        {
            return null;
        }
        
        if (pointer >= paths.size())
        {
            return null;
        }
        
        String path = paths.get(pointer);
        System.out.println("path=" + path);
        //send the image
        System.out.println("aetocall=" + aeToCall);
        
        AEInfo info = DB.getInstance().getClientAEInfoByIP(localAE, ipToCall);
        if (null == info)
        {
            System.out.println("aeinfo " + aeToCall + " nao encontrado [" + ipToCall + "]");
            return null;
        }
        
        System.out.println("info = " + info);
        Store store = new Store(localAE, info.getIp(), info.getAeTitle(), info.getPort());
        try
        {
            store.send(new String[] { path });
            success++;
        }
        catch (Exception ex)
        {
            System.out.println("Falha ao transmitir imagem.. " + ex.getMessage());
            failed++;
        }
        
        int completed = success + failed;
        int remaining = paths.size() - completed;
        Command cmd = OFACT.newCommand();
        cmd.putUS(Tags.Status, Status.Pending);
        cmd.putUS(Tags.NumberOfWarningSubOperations, 0);
        cmd.putUS(Tags.NumberOfFailedSubOperations, failed);
        cmd.putUS(Tags.NumberOfRemainingSubOperations, remaining);
        cmd.putUS(Tags.NumberOfCompletedSubOperations, completed);
        return cmd;
    }

    @Override
    public Dataset getDataset()
    {
        return null;
    }

    @Override
    public void next()
    {
        pointer++;
    }

    public int getSize()
    {
        return paths.size();
    }

    private void process(PreparedStatement statement, String[] ids) throws SQLException
    {
        for (String id : ids)
        {
            statement.setString(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next())
            {
                String path = rs.getString(1);
                paths.add(path);
            }
        }
    }
}