/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs.server;

import org.dcm4che.net.DimseListener;
import org.dcm4che.net.DcmServiceBase;
import org.dcm4che.data.Dataset;
import org.dcm4che.data.Command;
import org.dcm4che.dict.Status;
import org.dcm4che.dict.Tags;
import org.dcm4che.net.ActiveAssociation;
import org.dcm4che.net.Association;
import org.dcm4che.net.Dimse;

/**
 * Adapted from example of Gunter Zeilinger
 *
 * @author     <a href="mailto:gunter@tiani.com">gunter zeilinger</a>
 * @since March 20, 2003
 * @version $Revision: 1.2 $ $Date: 2003/03/23 08:58:37 $
 */
public class FindMultiDimseRsp implements DcmServiceBase.MultiDimseRsp, DimseListener
{
    private boolean cancel = false;
    private Response response = null;

    /**
     * Constructor for the FindMultiDimseRsp object
     *
     * @param ds
     */
    public FindMultiDimseRsp(Dataset ds)
    {
        try
        {
            response = DB.getInstance().getFindResponse(ds);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * Description of the Method
     *
     * @param assoc Description of the Parameter
     * @param dimse Description of the Parameter
     */
    @Override
    public void dimseReceived(Association assoc, Dimse dimse)
    {
        cancel = true;
    }

    /**
     * Gets the cancelListener attribute of the FindMultiDimseRsp object
     *
     * @return The cancelListener value
     */
    @Override
    public DimseListener getCancelListener()
    {
        return this;
    }

    /**
     * Description of the Method
     *
     * @param assoc Description of the Parameter
     * @param rq Description of the Parameter
     * @param rspCmd Description of the Parameter
     *
     * @return Description of the Return Value
     */
    @Override
    public Dataset next(ActiveAssociation assoc, Dimse rq, Command rspCmd)
    {
        if (cancel)
        {
            rspCmd.putUS(Tags.Status, Status.Cancel);
            return null;
        }
        response.next();
        Dataset ds = response.getDataset();
        if (ds == null)
        {
            rspCmd.putUS(Tags.Status, Status.Success);
            return null;
        }
        rspCmd.putUS(Tags.Status, Status.Pending);
        ds.putAE(Tags.RetrieveAET, assoc.getAssociation().getCalledAET());
      // Options for Instance Availability are:
        // - ONLINE
        // - NEARLINE
        // - OFFLINE
        ds.putCS(Tags.InstanceAvailability, "ONLINE");
        return ds;
    }

    /**
     * Description of the Method
     */
    @Override
    public void release()
    {
        try
        {
            response.close();
        }
        catch (Exception e)
        {
            System.out.println("Erro?");
        }
        response = null;
    }

}
