/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs.server;

import java.sql.*;
import org.dcm4che.data.*;
import java.io.*;
import java.util.Properties;
import java.util.Vector;

public class DBPostgre extends DB
{
    public DBPostgre()
    {
        try
        {
            Class.forName("org.postgresql.Driver");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public Connection getConnection(String conName)
    {
        try
        {
            Properties props = new Properties();
            InputStream in;
            // Primeiro tenta ler arquivo local
            File file = new File("postgre.properties");
            if (file.exists())
            {
                in = new FileInputStream(file);
            }
            else
            {
                // Se nao existe, usa o que está no CLASSPATH
                in = getClass().getResourceAsStream("postgre.properties");
            }
            props.load(in);
            in.close();
            String host = props.getProperty("postgre.host", "");
            String db = props.getProperty("postgre.database", "");
            String user = props.getProperty("postgre.user", "");
            String pass = props.getProperty("postgre.password", "");
            return DriverManager.getConnection("jdbc:postgresql://" + host + "/" + db, user, pass);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public void freeConnection(String conName, Connection con)
    {
        try
        {
            con.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public String[] listConnections()
    {
        return new String[]
        {
            "minipacsserver"
        };
    }

    @Override
    public DBWriter getDBWriter()
    {
        return new DBWriterAccess();
    }

    @Override
    public File getBaseFile(final String aeclient, final String aeserver)
    {
        String path = (String) doStatement(new Query()
        {
            @Override
            public String getSQL()
            {
                /*
                 SELECT AES_SAVE_PATH,
                 AXA_SAVE_PATH
                 FROM IMG_AEC_X_AES,
                 IMG_AE_CLIENTE,
                 IMG_AE_SERVIDOR
                 WHERE AES_AETITLE = ?
                 AND   AEC_AETITLE = ?
                 AND   AXA_AES_ID = AES_ID
                 AND   AXA_AEC_ID = AEC_ID
                 */
                return "SELECT AES_SAVE_PATH, "
                    + "       AXA_SAVE_PATH "
                    + "FROM IMG_AEC_X_AES, "
                    + "     IMG_AE_CLIENTE, "
                    + "     IMG_AE_SERVIDOR "
                    + "WHERE AES_AETITLE = '" + aeserver + "' "
                    + "AND   AEC_AETITLE = '" + aeclient + "' "
                    + "AND   AXA_AES_ID = AES_ID "
                    + "AND   AXA_AEC_ID = AEC_ID ";
            }

            @Override
            public Object process(ResultSet rs) throws SQLException
            {
                String path = null;
                if (rs.next())
                {
                    String first = rs.getString(1);
                    String second = rs.getString(2);
                    if (second != null && !second.equals(""))
                    {
                        path = second;
                    }
                    else
                    {
                        path = first;
                    }
                }
                return path;
            }
        });
        
        return new File(path);
    }

    @Override
    public String[] getCallingAEs(final String serverAE)
    {
        final Vector vec = new Vector();
        doStatement(new Query()
        {
            @Override
            public String getSQL()
            {
                /*
                 SELECT AEC_AETITLE
                 FROM IMG_AEC_X_AES
                 ,    IMG_AE_CLIENTE
                 ,    IMG_AE_SERVIDOR
                 WHERE AES_AETITLE = ?
                 AND   AXA_AES_ID = AES_ID
                 AND   AXA_AEC_ID = AEC_ID
                 */
                return "SELECT AEC_AETITLE "
                        + "FROM IMG_AEC_X_AES  "
                        + ",    IMG_AE_CLIENTE "
                        + ",    IMG_AE_SERVIDOR "
                        + "WHERE AES_AETITLE = '" + serverAE + "' "
                        + "AND   AXA_AES_ID = AES_ID "
                        + "AND   AXA_AEC_ID = AEC_ID ";
            }

            @Override
            public Object process(ResultSet rs) throws SQLException
            {
                while (rs.next())
                {
                    String str = rs.getString(1);
                    vec.add(str);
                }
                return vec;
            }
        });
        String[] resp = new String[vec.size()];
        vec.copyInto(resp);
        return resp;
    }

    @Override
    public AEInfo[] getClientAEInfoByIP(final String ip)
    {
        final Vector<AEInfo> vec = new Vector<>();
        doStatement(new Query()
        {
            @Override
            public String getSQL()
            {
                /*
                 SELECT AEC_AETITLE,
                 AEC_IP,
                 AEC_PORT,
                 AEC_DESCRICAO
                 FROM IMG_AE_CLIENTE
                 WHERE AEC_IP = ?
                 */
                return "SELECT AEC_AETITLE, "
                        + "       AEC_IP, "
                        + "       AEC_PORT, "
                        + "       AEC_DESCRICAO "
                        + "FROM IMG_AE_CLIENTE "
                        + "WHERE AEC_IP = '" + ip + "'";
            }

            @Override
            public Object process(ResultSet rs) throws SQLException
            {
                while (rs.next())
                {
                    AEInfo info = new AEInfo();
                    info.setAeTitle(rs.getString(1));
                    info.setIp(rs.getString(2));
                    info.setPort(rs.getInt(3));
                    info.setDescription(rs.getString(4));
                    vec.add(info);
                }
                return vec;
            }
        });
        AEInfo[] resp = new AEInfo[vec.size()];
        vec.copyInto(resp);
        return resp;
    }

    @Override
    public AEInfo getClientAEInfoByIP(final String server, final String ip)
    {
        return (AEInfo) doStatement(new Query()
        {
            @Override
            public String getSQL()
            {
                /*
                 SELECT AEC_AETITLE,
                 AEC_IP,
                 AEC_PORT,
                 AEC_DESCRICAO
                 FROM IMG_AEC_X_AES,
                 IMG_AE_CLIENTE,
                 IMG_AE_SERVIDOR
                 WHERE AEC_IP = ?
                 AND   AES_AETITLE = ?
                 AND   AXA_AES_ID = AES_ID
                 AND   AXA_AEC_ID = AEC_ID
                 */
                return "SELECT AEC_AETITLE, "
                     + "       AEC_IP, "
                     + "       AEC_PORT, "
                     + "       AEC_DESCRICAO "
                     + "FROM IMG_AEC_X_AES, "
                     + "     IMG_AE_CLIENTE, "
                     + "     IMG_AE_SERVIDOR "
                     + "WHERE AEC_IP = '" + ip + "' "
                     + "AND   AES_AETITLE = '" + server + "' "
                     + "AND   AXA_AES_ID = AES_ID "
                     + "AND   AXA_AEC_ID = AEC_ID ";
            }

            @Override
            public Object process(ResultSet rs) throws SQLException
            {
                if (rs.next())
                {
                    AEInfo info = new AEInfo();
                    info.setAeTitle(rs.getString(1));
                    info.setIp(rs.getString(2));
                    info.setPort(rs.getInt(3));
                    info.setDescription(rs.getString(4));
                    return info;
                }
                return null;
            }
        });
    }
    
    @Override
    public AEInfo[] getClientAEInfo(final String aetitle)
    {
        final Vector<AEInfo> vec = new Vector<AEInfo>();
        doStatement(new Query()
        {
            @Override
            public String getSQL()
            {
                /*
                 SELECT AEC_AETITLE,
                 AEC_IP,
                 AEC_PORT,
                 AEC_DESCRICAO
                 FROM IMG_AEC_X_AES
                 ,    IMG_AE_CLIENTE
                 ,    IMG_AE_SERVIDOR
                 WHERE AES_AETITLE = ?
                 AND   AXA_AES_ID = AES_ID
                 AND   AXA_AEC_ID = AEC_ID
                 */
                return "SELECT AEC_AETITLE, "
                     + "       AEC_IP, "
                     + "       AEC_PORT, "
                     + "       AEC_DESCRICAO "
                     + "FROM IMG_AE_CLIENTE, "
                     + "     IMG_AEC_X_AES  "
                     + "     IMG_AE_SERVIDOR  "
                     + "WHERE AES_AETITLE = '" + aetitle + "' "
                     + "AND   AXA_AES_ID = AES_ID "
                     + "AND   AXA_AEC_ID = AEC_ID ";
            }

            @Override
            public Object process(ResultSet rs) throws SQLException
            {
                while (rs.next())
                {
                    AEInfo info = new AEInfo();
                    info.setAeTitle(rs.getString(1));
                    info.setIp(rs.getString(2));
                    info.setPort(rs.getInt(3));
                    info.setDescription(rs.getString(4));
                    vec.add(info);
                }
                
                return vec;
            }
        });
        AEInfo[] resp = new AEInfo[vec.size()];
        vec.copyInto(resp);
        return resp;
    }

    @Override
    public AEInfo getClientAEInfo(final String serverAE, final String clientAE)
    {
        AEInfo resp = (AEInfo) doStatement(new Query()
        {
            @Override
            public String getSQL()
            {
                /*
                 SELECT AEC_AETITLE,
                 AEC_IP,
                 AEC_PORT,
                 AEC_DESCRICAO
                 FROM IMG_AEC_X_AES
                 ,    IMG_AE_CLIENTE
                 ,    IMG_AE_SERVIDOR
                 WHERE AES_AETITLE = ?
                 AND   AEC_AETITLE = ?
                 AND   AXA_AES_ID = AES_ID
                 AND   AXA_AEC_ID = AEC_ID
                 */
                
                return "SELECT AEC_AETITLE, "
                     + "       AEC_IP, "
                     + "       AEC_PORT, "
                     + "       AEC_DESCRICAO "
                     + "FROM IMG_AEC_X_AES "
                     + ",    IMG_AE_CLIENTE "
                     + ",    IMG_AE_SERVIDOR "
                     + "WHERE AES_AETITLE = '" + serverAE + "' "
                     + "AND   AEC_AETITLE = '" + clientAE + "' "
                     + "AND   AXA_AES_ID = AES_ID "
                     + "AND   AXA_AEC_ID = AEC_ID ";
            }

            @Override
            public Object process(ResultSet rs) throws SQLException
            {
                AEInfo info = null;
                if (rs.next())
                {
                    info = new AEInfo();
                    info.setAeTitle(rs.getString(1));
                    info.setIp(rs.getString(2));
                    info.setPort(rs.getInt(3));
                    info.setDescription(rs.getString(4));
                }
                
                return info;
            }
        });
        return resp;
    }

    @Override
    public AEInfo[] getServerAEInfo(final int type)
    {
        final Vector vec = new Vector();
        doStatement(new Query()
        {
            @Override
            public String getSQL()
            {
                /*
                 SELECT AES_AETITLE,
                 AES_IP,
                 AES_PORT,
                 AES_DESCRICAO
                 FROM IMG_AE_SERVIDOR
                 WHERE AES_TIPO_SERVIDOR = ?
                 */
                return "SELECT AES_AETITLE, "
                        + "       AES_IP, "
                        + "       AES_PORT, "
                        + "       AES_DESCRICAO "
                        + "FROM IMG_AE_SERVIDOR "
                        + "WHERE AES_TIPO_SERVIDOR = " + type;
            }

            @Override
            public Object process(ResultSet rs) throws SQLException
            {
                while (rs.next())
                {
                    AEInfo info = new AEInfo();
                    info.setAeTitle(rs.getString(1));
                    info.setIp(rs.getString(2));
                    info.setPort(rs.getInt(3));
                    info.setDescription(rs.getString(4));
                    vec.add(info);
                }
                return vec;
            }
        });
        AEInfo[] resp = new AEInfo[vec.size()];
        vec.copyInto(resp);
        return resp;
    }

    @Override
    public Response getFindResponse(Dataset command)
    {
        return new FindResponseAccess(command);
    }

    @Override
    public Response getMoveResponse(Command command, Dataset dataset, String aetitle, String ip)
    {
        return new MoveResponseAccess(command, dataset, aetitle, ip);
    }

    private Object doStatement(Query query)
    {
        Connection con = getConnection(null);
        try
        {
            Object obj;
            try (Statement statement = con.createStatement())
            {
                ResultSet rs = statement.executeQuery(query.getSQL());
                obj = query.process(rs);
            }
            return obj;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
        finally
        {
            freeConnection(null, con);
        }
    }

    public static void main(String[] args)
    {
        AEInfo[] info = DB.getInstance().getClientAEInfoByIP("200.9.95.172");
        System.out.println("info = " + info[0]);
    }
}