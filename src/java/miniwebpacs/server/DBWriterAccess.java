/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 * ********************************************************************
 */
package miniwebpacs.server;

import java.io.*;
import java.sql.*;
import org.dcm4che.data.*;
import org.dcm4che.dict.Tags;

public class DBWriterAccess extends DBWriter
{
    public DBWriterAccess()
    {
        //Vazio
    }

    @Override
    protected synchronized void update(File file, Dataset ds, String callingAE, String calledAE) throws SQLException
    {
        Connection con = DB.getInstance().getConnection(null);
        try
        {
            // Check if the patient is already in the database.
            String patID = ds.getString(Tags.PatientID);
            int numPatRelationStudy = 0;
            int numPatRelationSeries = 0;
            int numPatRelationImages = 0;
            /*
             SELECT NUMPATRELSTU,
             NUMPATRELSER,
             NUMPATRELIMA
             FROM IMG_PATIENTLEVEL
             WHERE PACI_ID = ?
             */
            String sql = "SELECT NUMPATRELSTU, "
                       + "       NUMPATRELSER, "
                       + "       NUMPATRELIMA  "
                       + "FROM IMG_PATIENTLEVEL "
                       + "WHERE PACI_ID = ? ";
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setString(1, patID);
            ResultSet rs = statement.executeQuery();
            if (!rs.next())
            {
                //It is not in the database. Let's insert it.
                statement.close();
                /*
                 INSERT INTO IMG_PATIENTLEVEL(PACI_ID,
                 PATNAM,
                 PATBIRTIM,
                 PATBIRDAT,
                 PATSEX,
                 NUMPATRELSTU,
                 NUMPATRELSER,
                 NUMPATRELIMA,
                 INSERTDATETIME,
                 OWNER)
                 VALUES (?,?,?,?,?,?,?,?,?,?)
                 */
                sql = "INSERT INTO IMG_PATIENTLEVEL(PACI_ID, "
                    + "                             PATNAM, "
                    + "                             PATBIRTIM, "
                    + "                             PATBIRDAT, "
                    + "                             PATSEX, "
                    + "                             NUMPATRELSTU, "
                    + "                             NUMPATRELSER, "
                    + "                             NUMPATRELIMA, "
                    + "                             INSERTDATETIME, "
                    + "                             OWNER) "
                    + "VALUES (?,?,?,?,?,?,?,?,?,?) ";
                statement = con.prepareStatement(sql);
                statement.setString(1, ds.getString(Tags.PatientID));
                statement.setString(2, ds.getString(Tags.PatientName, "UNKNOWN"));
                statement.setString(3, ds.getString(Tags.PatientBirthTime, ""));
                java.util.Date date = ds.getDate(Tags.PatientBirthDate);
                if (date != null && date.getTime() > 0)
                {
                    System.out.println("date.time=" + date.getTime() + " " + date);
                    statement.setDate(4, new java.sql.Date(date.getTime()));
                }
                else
                {
                    statement.setNull(4, Types.DATE);
                }
                statement.setString(5, ds.getString(Tags.PatientSex));
                statement.setInt(6, 0);
                statement.setInt(7, 0);
                statement.setInt(8, 0);
                statement.setTimestamp(9, new java.sql.Timestamp(System.currentTimeMillis()));
                statement.setString(10, callingAE);
                statement.executeUpdate();
            }
            else
            {
                numPatRelationStudy = rs.getInt(1);
                numPatRelationSeries = rs.getInt(2);
                numPatRelationImages = rs.getInt(3);
            }
            statement.close();
            // Check if the study is already in the database.
            String stuID = ds.getString(Tags.StudyInstanceUID);
            int numStuRelSer = 0;
            int numStuRelIma = 0;
            /*
             SELECT NUMSTURELSER,
             NUMSTURELIMA
             FROM IMG_STUDYLEVEL
             WHERE STUINSUID = ?
             */
            sql = "SELECT NUMSTURELSER, "
                + "       NUMSTURELIMA  "
                + "FROM IMG_STUDYLEVEL "
                + "WHERE STUINSUID = ? ";
            statement = con.prepareStatement(sql);
            statement.setString(1, stuID);
            rs = statement.executeQuery();
            if (!rs.next())
            {
                //insert
                statement.close();
                /*
                 INSERT INTO IMG_STUDYLEVEL(ACCNUM,
                 STUINSUID,
                 STUDES,
                 STUDAT,
                 STUTIM,
                 STUID,
                 PACI_ID,
                 PATAGE,
                 PATWEI,
                 PATSIZ,
                 TP_MOD,
                 NUMSTURELSER,
                 NUMSTURELIMA,
                 REFPHYNAM,
                 INSERTDATETIME,
                 OWNER,
                 SIZE_BYTES,
                 PATH_ESTUDO,
                 FLAG_ONLINE)
                 VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
                 */
                sql = "INSERT INTO IMG_STUDYLEVEL(ACCNUM, "
                    + "                           STUINSUID, "
                    + "                           STUDES, "
                    + "                           STUDAT, "
                    + "                           STUID, "
                    + "                           PACI_ID, "
                    + "                           PATAGE, "
                    + "                           PATWEI, "
                    + "                           PATSIZ, "
                    + "                           TP_MOD, "
                    + "                           NUMSTURELSER, "
                    + "                           NUMSTURELIMA, "
                    + "                           REFPHYNAM, "
                    + "                           INSERTDATETIME, "
                    + "                           OWNER, "
                    + "                           SIZE_BYTES, "
                    + "                           PATH_ESTUDO, "
                    + "                           FLAG_ONLINE) "
                    + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
                statement = con.prepareStatement(sql);
                statement.setString(1, ds.getString(Tags.AccessionNumber, ""));
                statement.setString(2, ds.getString(Tags.StudyInstanceUID));
                statement.setString(3, ds.getString(Tags.StudyDescription, ""));
                java.util.Date date = ds.getDateTime(Tags.StudyDate, Tags.StudyTime);
                if (date != null)
                {
                    statement.setTimestamp(4, new java.sql.Timestamp(date.getTime()));
                }
                else
                {
                    statement.setNull(4, Types.DATE);
                }
                
                statement.setString(5, ds.getString(Tags.StudyID, ""));
                statement.setString(6, ds.getString(Tags.PatientID));
                statement.setString(7, ds.getString(Tags.PatientAge, ""));
                statement.setString(8, ds.getString(Tags.PatientWeight, ""));
                statement.setString(9, ds.getString(Tags.PatientSize, ""));
                statement.setString(10, ds.getString(Tags.Modality));
                statement.setInt(11, 0);
                statement.setInt(12, 0);
                statement.setString(13, ds.getString(Tags.ReferringPhysicianName, ""));
                statement.setTimestamp(14, new java.sql.Timestamp(System.currentTimeMillis()));
                statement.setString(15, callingAE);
                statement.setInt(16, 0);
                // the place of the study
                java.io.File dir = DB.getInstance().getBaseFile(callingAE, calledAE);
                String[] names = Configurations.getInstance().getStringArray("storage.path.study");
                String path = new Util().toDir(ds, names, dir).getAbsolutePath();
                statement.setString(17, path);
                statement.setInt(18, 1);
                statement.executeUpdate();
                statement.close();
                // update patientlevel
                /*
                 UPDATE IMG_PATIENTLEVEL
                 SET NUMPATRELSTU = ?
                 WHERE PACI_ID = ?
                 */
                sql = " UPDATE IMG_PATIENTLEVEL SET NUMPATRELSTU = ? WHERE PACI_ID = ? ";
                statement = con.prepareStatement(sql);
                statement.setInt(1, ++numPatRelationStudy);
                statement.setString(2, patID);
                statement.executeUpdate();
            }
            else
            {
                numStuRelSer = rs.getInt(1);
                numStuRelIma = rs.getInt(2);
            }
            statement.close();
            // Check if the series is already in the database.
            String serID = ds.getString(Tags.SeriesInstanceUID);
            int numSerRelIma = 0;
            /*
             SELECT NUMSERRELIMA
             FROM IMG_SERIESLEVEL
             WHERE SERINSUID = ?
             */
            sql = "SELECT NUMSERRELIMA "
                + "FROM IMG_SERIESLEVEL "
                + "WHERE SERINSUID = ? ";
            statement = con.prepareStatement(sql);
            statement.setString(1, serID);
            rs = statement.executeQuery();
            if (!rs.next())
            {
                //insert series
                /*
                 INSERT INTO IMG_SERIESLEVEL(SERINSUID,
                 STUINSUID,
                 SERNUM,
                 PRONAM,
                 BODPAREXA,
                 SERDES,
                 VIEPOS,
                 NUMSERRELIMA,
                 INSERTDATETIME,
                 TP_MOD,
                 OWNER,
                 SIZE_BYTES,
                 SER_PATH)
                 VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)
                 */
                sql = "INSERT INTO IMG_SERIESLEVEL(SERINSUID, "
                    + "                            STUINSUID, "
                    + "                            SERNUM, "
                    + "                            BODPAREXA, "
                    + "                            SERDES, "
                    + "                            VIEPOS, "
                    + "                            NUMSERRELIMA, "
                    + "                            INSERTDATETIME, "
                    + "                            TP_MOD, "
                    + "                            OWNER, "
                    + "                            SIZE_BYTES, "
                    + "                            SER_PATH) "
                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ";
                statement = con.prepareStatement(sql);
                statement.setString(1, ds.getString(Tags.SeriesInstanceUID));
                statement.setString(2, ds.getString(Tags.StudyInstanceUID));
                statement.setString(3, ds.getString(Tags.SeriesNumber));
                statement.setString(4, ds.getString(Tags.BodyPartExamined, ""));
                statement.setString(5, ds.getString(Tags.SeriesDescription, ""));
                statement.setString(6, ds.getString(Tags.ViewPosition, ""));
                statement.setInt(7, 0);
                statement.setTimestamp(8, new java.sql.Timestamp(System.currentTimeMillis()));
                statement.setString(9, ds.getString(Tags.Modality));
                statement.setString(10, callingAE);
                statement.setInt(11, 0);
                // the place of the series
                File dir = DB.getInstance().getBaseFile(callingAE, calledAE);
                String[] names = Configurations.getInstance().getStringArray("storage.path.series");
                String path = new Util().toDir(ds, names, dir).getAbsolutePath();
                statement.setString(12, path);
                statement.executeUpdate();
                statement.close();
                // update patient
                /*
                 UPDATE IMG_PATIENTLEVEL
                 SET NUMPATRELSER = ?
                 WHERE PACI_ID = ?
                 */
                sql = "UPDATE IMG_PATIENTLEVEL "
                    + "SET NUMPATRELSER = ?  "
                    + "WHERE PACI_ID = ? ";
                statement = con.prepareStatement(sql);
                statement.setInt(1, ++numPatRelationSeries);
                statement.setString(2, patID);
                statement.executeUpdate();
                statement.close();
                // update study
                /*
                 UPDATE IMG_STUDYLEVEL
                 SET NUMSTURELSER = ?
                 WHERE STUINSUID = ?
                 */
                sql = "UPDATE IMG_STUDYLEVEL "
                    + "SET NUMSTURELSER = ?  "
                    + "WHERE STUINSUID = ? ";
                statement = con.prepareStatement(sql);
                statement.setInt(1, ++numStuRelSer);
                statement.setString(2, stuID);
                statement.executeUpdate();
            }
            else
            {
                numSerRelIma = rs.getInt(1);
            }
            statement.close();
            // Check if the image is already in the database.
            String sopID = ds.getString(Tags.SOPInstanceUID);
            /*
             SELECT *
             FROM IMG_IMAGELEVEL
             WHERE SOPINSUID = ?
             */
            sql = "SELECT * "
                + "FROM IMG_IMAGELEVEL "
                + "WHERE SOPINSUID = ? ";
            statement = con.prepareStatement(sql);
            statement.setString(1, sopID);
            rs = statement.executeQuery();
            if (!rs.next())
            {
                //must insert
                /*
                 INSERT INTO IMG_IMAGELEVEL(SOPINSUID,
                 SERINSUID,
                 SOPCLAUID,
                 SAMPERPIX,
                 PHOINT,
                 ROW_NUM,
                 COL_NUM,
                 BITALL,
                 BITSTO,
                 PIXREP,
                 PATORI,
                 IMANUM,
                 INSERTDATETIME,
                 OWNER,
                 SIZE_BYTES,
                 TRANSFER,
                 IMG_PATH)
                 VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
                 */
                sql = "INSERT INTO IMG_IMAGELEVEL(SOPINSUID, "
                    + "                           SERINSUID, "
                    + "                           SOPCLAUID, "
                    + "                           SAMPERPIX, "
                    + "                           PHOINT, "
                    + "                           ROW_NUM, "
                    + "                           COL_NUM, "
                    + "                           BITALL, "
                    + "                           BITSTO, "
                    + "                           PIXREP, "
                    + "                           PATORI, "
                    + "                           IMANUM, "
                    + "                           INSERTDATETIME, "
                    + "                           OWNER, "
                    + "                           SIZE_BYTES, "
                    + "                           TRANSFER, "
                    + "                           IMG_PATH) "
                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
                statement = con.prepareStatement(sql);
                statement.setString(1, ds.getString(Tags.SOPInstanceUID));
                statement.setString(2, ds.getString(Tags.SeriesInstanceUID));
                statement.setString(3, ds.getString(Tags.SOPClassUID));
                statement.setInt(4, ds.getInt(Tags.SamplesPerPixel, 1));
                statement.setString(5, ds.getString(Tags.PhotometricInterpretation));
                statement.setInt(6, ds.getInt(Tags.Rows, -1));
                statement.setInt(7, ds.getInt(Tags.Columns, -1));
                statement.setInt(8, ds.getInt(Tags.BitsAllocated, 8));
                statement.setInt(9, ds.getInt(Tags.BitsStored, 8));
                statement.setInt(10, ds.getInt(Tags.PixelRepresentation, 1));
                statement.setString(11, ds.getString(Tags.PatientOrientation));
                statement.setString(12, ds.getString(Tags.InstanceNumber));
                statement.setTimestamp(13, new java.sql.Timestamp(System.currentTimeMillis()));
                statement.setString(14, callingAE);
                int sz = (int) file.length();
                statement.setInt(15, sz);
                statement.setString(16, ds.getFileMetaInfo().getTransferSyntaxUID());
                statement.setString(17, file.getAbsolutePath());
                statement.executeUpdate();
                statement.close();
                // update patient
                /*
                 UPDATE IMG_PATIENTLEVEL
                 SET NUMPATRELIMA = ?
                 WHERE PACI_ID = ?
                 */
                sql = "UPDATE IMG_PATIENTLEVEL "
                    + "SET NUMPATRELIMA = ?  "
                    + "WHERE PACI_ID = ? ";
                statement = con.prepareStatement(sql);
                statement.setInt(1, ++numPatRelationImages);
                statement.setString(2, patID);
                statement.executeUpdate();
                statement.close();
                // update study
                /*
                 UPDATE IMG_STUDYLEVEL
                 SET NUMSTURELIMA = ?
                 WHERE STUINSUID = ?
                 */
                sql = "UPDATE IMG_STUDYLEVEL "
                    + "SET NUMSTURELIMA = ?  "
                    + "WHERE STUINSUID = ? ";
                statement = con.prepareStatement(sql);
                statement.setInt(1, ++numStuRelIma);
                statement.setString(2, stuID);
                statement.executeUpdate();
                statement.close();
                // update series
                /*
                 UPDATE IMG_SERIESLEVEL
                 SET NUMSERRELIMA = ?
                 WHERE SERINSUID = ?
                */
                sql = "UPDATE IMG_SERIESLEVEL "
                    + "SET NUMSERRELIMA = ?  "
                    + "WHERE SERINSUID = ? ";
                statement = con.prepareStatement(sql);
                statement.setInt(1, ++numSerRelIma);
                statement.setString(2, serID);
                statement.executeUpdate();
            }
            statement.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            DB.getInstance().freeConnection(null, con);
        }
    }
}