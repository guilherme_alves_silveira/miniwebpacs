/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs.server;

import java.io.*;
import java.util.Random;
import org.dcm4che.data.Dataset;
import org.dcm4che.dict.Tags;

public class Util
{
    private static final Random RND = new Random();

    public Util()
    {
        //Vazio
    }

    public File toDir(Dataset ds, String[] names, File baseDir)
    {
        int[] fileIDTags = toTags(names);
        File file = baseDir;
        for (int i = 0; i < fileIDTags.length; ++i)
        {
            String s = ds.getString(fileIDTags[i]);
            if (s == null || s.length() == 0)
            {
                s = "__NULL__";
            }
            char[] in = s.toUpperCase().toCharArray();
            char[] out = new char[Math.min(8, in.length)];
            for (int j = 0; j < out.length; ++j)
            {
                out[j] = in[j] >= '0' && in[j] <= '9'
                        || in[j] >= 'A' && in[j] <= 'Z'
                         ? in[j] : '_';
            }
            s = new String(out);
            file = new File(file, s);
        }
        return file;
    }

    public File toFile(Dataset ds, String[] names, File baseDir)
    {
        return toFile(ds, toTags(names), baseDir);
    }

    public File toFile(Dataset ds, int[] fileIDTags, File baseDir)
    {
        File file = baseDir;
        for (int i = 0; i < fileIDTags.length; ++i)
        {
            file = new File(file, toFileID(ds, fileIDTags[i]));
        }
        File parent = file.getParentFile();
        while (file.exists())
        {
            file = new File(parent,
                            Integer.toHexString(RND.nextInt()).toUpperCase());
        }
        return file;
    }

    private static int[] toTags(String[] names)
    {
        int[] retval = new int[names.length];
        for (int i = 0; i < names.length; ++i)
        {
            retval[i] = Tags.forName(names[i]);
        }
        return retval;
    }

    private String toFileID(Dataset ds, int tag)
    {
        String s = ds.getString(tag);
        if (s == null || s.length() == 0)
        {
            return "__NULL__";
        }
        char[] in = s.toUpperCase().toCharArray();
        char[] out = new char[Math.min(8, in.length)];
        for (int i = 0; i < out.length; ++i)
        {
            out[i] = in[i] >= '0' && in[i] <= '9'
                    || in[i] >= 'A' && in[i] <= 'Z'
                     ? in[i] : '_';
        }
        return new String(out);
    }

}
