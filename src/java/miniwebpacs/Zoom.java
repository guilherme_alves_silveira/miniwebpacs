/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs;

import javax.swing.*;
import java.awt.geom.*;
import java.awt.image.*;

public class Zoom extends JPanel implements ImageProcessor
{
    private boolean changed = true;
    private BufferedImage processedImg = null;
    private JSlider zoomSlider = null;

    public Zoom()
    {
        setLayout(new java.awt.FlowLayout());
        zoomSlider = new JSlider(SwingConstants.HORIZONTAL, 0, 40, 10);
        zoomSlider.setMinorTickSpacing(5);
        zoomSlider.setMajorTickSpacing(10);
        zoomSlider.setPaintTicks(true);
        zoomSlider.setPreferredSize(new java.awt.Dimension(100, 25));
        zoomSlider.addChangeListener(e -> zoomChanged());
        add(new JLabel("Zoom:"));
        add(zoomSlider);
    }

    @Override
    public JComponent getGUI()
    {
        return this;
    }

    public void zoomChanged()
    {
        changed = true;
        MainFrame.getInstance().repaint();
    }

    @Override
    public BufferedImage processImage(BufferedImage img)
    {
        if (changed || processedImg == null || img != processedImg)
        {
            double scale = Math.max(1, zoomSlider.getValue()) / 10.;
            AffineTransformOp op = new AffineTransformOp(
                    AffineTransform.getScaleInstance(scale, scale),
                    AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
            processedImg = op.filter(img, op.createCompatibleDestImage(img, img.getColorModel()));
            changed = false;
            return processedImg;
        }
        else
        {
            return processedImg;
        }
    }

    @Override
    public void undo()
    {
        //Vazio
    }

}
