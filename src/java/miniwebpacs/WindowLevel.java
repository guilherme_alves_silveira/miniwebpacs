/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import org.dcm4che.data.Dataset;
import org.dcm4che.dict.Tags;
import org.dcm4che.image.ColorModelParam;
import org.dcm4che.image.ColorModelFactory;
import java.awt.image.*;

public class WindowLevel implements ImageProcessor
{
    private static final ColorModelFactory CM_FACTORY = ColorModelFactory.getInstance();
    private FlowLayout borderLayout1 = new FlowLayout();
    private JSlider widthSlider = null;
    private JSlider centerSlider = null;
    private ColorModelParam cmParam = null;
    private JLabel wL = new JLabel("Window:");
    private JLabel cL = new JLabel("Center:");
    private JPanel jp = new JPanel();
    private boolean change = false;
    private BufferedImage processedImage = null;

    public WindowLevel(Dataset dataset)
    {
        try
        {
            String pmi = dataset.getString(Tags.PhotometricInterpretation, null);
            if ("MONOCHROME1".equals(pmi) || "MONOCHROME2".equals(pmi))
            {
                cmParam = CM_FACTORY.makeParam(dataset);
                int bits = dataset.getInt(Tags.BitsStored, 8);
                int size = 1 << bits;
                int signed = dataset.getInt(Tags.PixelRepresentation, 0);
                int min = dataset.getInt(Tags.SmallestImagePixelValue,
                                         signed == 0 ? 0 : -(size >> 1));
                int max = dataset.getInt(Tags.LargestImagePixelValue,
                                         signed == 0 ? size - 1 : (size >> 1) - 1);
                int c = (int) cmParam.toMeasureValue((min + max) >> 1);
                int cMin = (int) cmParam.toMeasureValue(min);
                int cMax = (int) cmParam.toMeasureValue(max - 1);
                int wMax = cMax - cMin;
                int w = wMax;

                int nWindow = cmParam.getNumberOfWindows();
                if (nWindow > 0)
                {
                    c = (int) cmParam.getWindowCenter(0);
                    w = (int) cmParam.getWindowWidth(0);
                }
                widthSlider = new JSlider(SwingConstants.HORIZONTAL,
                                          0, Math.max(w, wMax), w);
                widthSlider.setPreferredSize(new Dimension(100, 25));
                widthSlider.addChangeListener(new ChangeListener()
                {
                    public void stateChanged(ChangeEvent e)
                    {
                        windowChanged();
                    }
                });
                jp.add(wL);
                jp.add(widthSlider);
                centerSlider = new JSlider(SwingConstants.HORIZONTAL,
                                           Math.min(c << 1, cMin), Math.max(c << 1, cMax), c);
                centerSlider.setPreferredSize(new Dimension(100, 25));
                centerSlider.addChangeListener(new ChangeListener()
                {
                    public void stateChanged(ChangeEvent e)
                    {
                        windowChanged();
                    }
                });
                jp.add(cL);
                jp.add(centerSlider);
            }
            jbInit();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    void jbInit() throws Exception
    {
        jp.setLayout(borderLayout1);
    }

    public JComponent getGUI()
    {
        return jp;
    }

    public void windowChanged()
    {
        change = true;
        MainFrame.getInstance().repaint();
    }

    @Override
    public BufferedImage processImage(BufferedImage oldImg)
    {
        if (change || processedImage != oldImg || processedImage == null)
        {
            this.cmParam = cmParam.update(centerSlider.getValue(),
                                          widthSlider.getValue(), cmParam.isInverse());
            ColorModel cm = CM_FACTORY.getColorModel(cmParam);
            BufferedImage newImg = new BufferedImage(cm, oldImg.getRaster(), false, null);
            change = false;
            this.processedImage = newImg;
            return newImg;
        }
        else
        {
            return processedImage;
        }
    }

    @Override
    public void undo()
    {

    }

}