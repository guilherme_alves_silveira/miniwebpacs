/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs.webstart;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;

public class CriaArquivoJNLP
{
    private static final String VERSAO = "1.0";
    private String login = null;
    private String stuinsuid = null;
    private String raiz = null;
    private String modeloJNLP = "";
    private String nomeArquivoGerado = "";
    private String codebase = "";

    /**
     * Construtor simples.
     */
    public CriaArquivoJNLP()
    {
        //Vazio
    }

    /**
     * Cria um arquivo JNLP a partir de um modelo, substituindo as variaveis
     * stuinsuid, nome do arquivo e login. Por exemplo:
     * <pre>
     *   &lt;?xml version="1.0" encoding="UTF-8"?&gt;
     *   &lt;jnlp spec="1.0+" codebase="http://localhost:8080" href="${FILENAME}"&gt;
     *     &lt;information&gt;
     *       &lt;title&gt;Nome Versao&lt;/title&gt;
     *       &lt;vendor&gt;Instituto do Coracao&lt;/vendor&gt;
     *       &lt;homepage href="InCorViewer.html" /&gt;
     *       &lt;description&gt;Visualizador de Imagens M�dicas&lt;/description&gt;
     *     &lt;/information&gt;
     *     &lt;resources&gt;
     *       &lt;j2se version="1.3+" /&gt;
     *       &lt;jar href="./IncorViewer.jar" /&gt;
     *     &lt;/resources&gt;
     *     &lt;applet-desc main-class="br.usp.incor.minipep.MainFrame" documentbase="." name="InCorViewer" width="1" height="1"&gt;
     *       &lt;param name="stuinsuid" value="${STUINSUID}"&gt;&lt;/param&gt;
     *     &lt;/applet-desc&gt;
     *   &lt;/jnlp&gt;
     * </pre>
     *
     * @param fileModeloBase Caminho para o arquivo modelo
     *
     * @throws java.io.IOException Falha na leitura do arquivo
     */
    public CriaArquivoJNLP(String fileModeloBase) throws java.io.IOException
    {
        carregaModelo(fileModeloBase);
    }

    public void setModeloJNLP(String jnlp)
    {
        this.modeloJNLP = jnlp;
    }

    public void setCodebase(String base)
    {
        this.codebase = base;
    }

    public void setRaiz(String raiz)
    {
        this.raiz = raiz;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }
    
    public void setStuinsuid(String stuinsuid)
    {
        this.stuinsuid = stuinsuid;
    }

    public String getNomeArquivo()
    {
        return nomeArquivoGerado;
    }

    public void criaArquivo(String seed) throws java.lang.Exception
    {
        String fileName = seed;
        fileName += (int) (1000.0 * Math.random());
        fileName += ".jnlp";
        nomeArquivoGerado = fileName;
        if (raiz.endsWith(File.separator))
        {
            fileName = raiz + fileName;
        }
        else
        {
            fileName = raiz + File.separator + fileName;
        }
        File file = new File(fileName);
        Map<String, String> variaveis = new HashMap<>();
        variaveis.put("STUINSUID", stuinsuid);
        variaveis.put("FILENAME", nomeArquivoGerado);
        variaveis.put("CODEBASE", codebase);
        String str = changeVars(variaveis, modeloJNLP);
        try (BufferedWriter out = new BufferedWriter(new FileWriter(file)))
        {
            out.write(str);
            out.flush();
        }
        
        file.deleteOnExit();
    }

    /**
     * Este metodo deve ser usado da seguinte maneira:
     * <pre>
     * CriaArquivoJNLP cr = new CriaArquivoJNLP();
     * cr.setLogin(meuLogin);
     * cr.setStuinsuid(stuinsuid);
     * pageContext.getSession().setAttribute("pastaModelo", "webstart");// opcional! Default = /
     * pageContext.getSession().setAttribute("pastaArquivos", "temp");// opcional! Default = /
     * pageContext.getSession().setAttribute("arquivoModelo", "meuModelo.xml");// opcional! Default = modelo.jnlp
     * cr.criaArquivo(pageContext);
     * </pre>
     *
     * @param context O contexto da pagina JSP
     *
     * @throws java.lang.Exception Caso ocorra algum erro
     */
    public void criaArquivo(javax.servlet.jsp.PageContext context) throws java.lang.Exception
    {
        codebase = java.net.InetAddress.getLocalHost().getHostAddress();
        String pastaModelo = (String) context.getSession().getAttribute("pastaModelo");
        String pastaArquivos = (String) context.getSession().getAttribute("pastaArquivos");
        String arquivoModelo = (String) context.getSession().getAttribute("arquivoModelo");
        String nome = arquivoModelo == null ? "modelo.jnlp" : arquivoModelo;
        String localModelo = pastaModelo == null ? "/" + nome : "/" + pastaModelo + "/" + nome;
        String localSalvar = pastaArquivos == null ? "/" : "/" + pastaArquivos;
        carregaModelo(context.getServletContext().getRealPath(localModelo));
        setRaiz(context.getServletContext().getRealPath(localSalvar));
        criaArquivo(context.getSession().getId());
    }

    private void carregaModelo(String path) throws java.io.IOException
    {
        java.io.BufferedInputStream in = new java.io.BufferedInputStream(
                new java.io.FileInputStream(path));
        int sz = in.available();
        byte[] array = new byte[sz];
        in.read(array);
        in.close();
        modeloJNLP = new String(array);
    }

    /**
     * <p>
     * Chupinhado do projeto AntEater (http://aft.sourceforge.net) [ram] que,
     * por sua vez, foi chupinhado do Avalon Excalibur.</p>
     *
     * <p>
     * Replace ${variables} in a string with values from a Map.</p>
     *
     * @param map a map of key-value pairs.
     * @param rawValue String containing ${variable} references to replace
     *
     * @return <code>rawValue</code> string with variables interpolated
     *
     * @exception RuntimeException if any variables are undefined.
     */
    private String changeVars(Map<String, String> map, String rawValue)
    {
        // Code nicked from Avalon Excalibur's Configuration classes
        StringBuilder result = new StringBuilder("");
        int i = 0;
        int j = -1;
        while ((j = rawValue.indexOf("${", i)) > -1)
        {
            if (i < j)
            {
                result.append(rawValue.substring(i, j));
            }
            int k = rawValue.indexOf('}', j);
            String ctxName = rawValue.substring(j + 2, k);
            int l = ctxName.indexOf(":");
            if (l != -1)
            {
                // Special handling of ${prefix:variable} vars.  Eg, if run=20,
                // ${run is :run} becomes 'run is 20'.  Blank if undefined
                final String prefixText = ctxName.substring(0, l);
                final String ctxSubName = ctxName.substring(l + 1);
                final String ctx = map.get(ctxSubName);
                if (ctx != null)
                {
                    result.append(prefixText);
                    result.append(ctx);
                }
            }
            else
            {
                final Object ctx = map.get(ctxName);
                if (ctx == null)
                {
                    throw new RuntimeException("Property '" + ctxName + "' does not exist");
                }
                result.append(ctx.toString());
            }
            i = k + 1;
        }
        
        if (i < rawValue.length())
        {
            result.append(rawValue.substring(i, rawValue.length()));
        }
        
        return result.toString();
    }

}