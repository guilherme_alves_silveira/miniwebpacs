/**
 * ********************************************************************
 *                                                                    *
 * Mini PACS with WEB interface for small-sized hospitals. * Copyright (C) 2004
 * Funda��o Zerbini * * This program is free software; you can redistribute it
 * and/or * modify it under the terms of the GNU General Public License as *
 * published by the Free Software Foundation; either version 2 of * the License,
 * or (at your option) any later version. * * This program is distributed in the
 * hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the
 * implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the * GNU General Public License for more details. * * You should have
 * received a copy of the GNU General Public License * along with this program;
 * if not, write to the Free Software * Foundation, Inc., 59 Temple Place -
 * Suite 330, Boston, MA * 02111-1307, USA. * * MiniWEBPACS -
 * http://miniwebpacs.sourceforge.net * Ramon A. Moreno -
 * ramon.moreno@incor.usp.br * *
 *********************************************************************
 */
package miniwebpacs;

import java.awt.*;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JSlider;
import javax.swing.JToolBar;
import javax.swing.ImageIcon;
import javax.imageio.ImageReader;
import java.awt.image.VolatileImage;
import java.awt.image.BufferedImage;
import java.awt.event.*;
import javax.swing.event.*;
import org.dcm4che.data.Dataset;

public class PanelDinamico extends JPanel implements Runnable, Viewer
{

    private int size = 0;
    private VolatileImage backBuffer = null;
    private BufferedImage[] sprite = null;
    private int pos = 0;
    private int width, height;
    private Thread thread = null;
    private JToolBar controls = new JToolBar();
    private ImageIcon iconPlay = new ImageIcon(getClass().getResource("VCRForward.gif"));
    private ImageIcon iconStop = new ImageIcon(getClass().getResource("VCRStop.gif"));
    private JButton play = new JButton("Play", iconPlay);
    private JButton stop = new JButton("Stop", iconStop);
    private JSlider posBar = new JSlider(JSlider.HORIZONTAL, 0, 100, 0);
    private WindowLevel wl = null;

    /**
     * Adaptado de VolatileDuke.java da Sun.
     *
     * @param reader ImageReader
     */
    public PanelDinamico(int rd)
    {
        try
        {
            Dataset dataset = MainFrame.getInstance().getFileDataSource().getDataset(rd);
            wl = new WindowLevel(dataset);
            ImageReader reader = MainFrame.getInstance().getFileDataSource().getReader(rd);
            size = reader.getNumImages(true);
            posBar.setMaximum(size);
            sprite = new BufferedImage[size];
            for (int i = 0; i < size; i++)
            {
                sprite[i] = reader.read(i);
            }
            play.setEnabled(false);
            play.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    controlPlay();
                }
            });
            stop.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    controlStop();
                }
            });
            posBar.addChangeListener(new ChangeListener()
            {
                public void stateChanged(ChangeEvent e)
                {
                    controlSetPos();
                }
            });
            controls.add(play);
            controls.add(stop);
            controls.add(posBar);
            controls.add(wl.getGUI());
        }
        catch (Exception ex)
        {
            MainFrame.getInstance().showException(ex);
        }
    }

    public synchronized void setFrame(int fr)
    {
        if (fr < 0 || fr > size)
        {
            return;
        }
        pos = fr;
        if (thread == null)
        {//ou seja, est� parado...
            repaint();
        }
    }

    public void start()
    {
        thread = new Thread(this);
        thread.setPriority(Thread.MIN_PRIORITY);
        thread.start();
    }

    public void stop()
    {
        thread = null;
    }

    public void run()
    {
        Thread me = Thread.currentThread();
        try
        {
            while (thread == me)
            {
                repaint();
                pos = (pos + 1) % size;
                posBar.setValue(pos);
                Thread.sleep(25);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void paint(Graphics g)
    {
        if (backBuffer == null || width != getWidth() || height != getHeight())
        {
            width = getWidth();
            height = getHeight();
            createBackBuffer();
        }
        do
        {
            // First, we validate the back buffer
            int valCode = backBuffer.validate(getGraphicsConfiguration());
            if (valCode == VolatileImage.IMAGE_RESTORED)
            {
                  // This case is just here for illustration
                // purposes.  Since we are
                // recreating the contents of the back buffer
                // every time through this loop, we actually
                // do not need to do anything here to recreate
                // the contents.  If our VImage was an image that
                // we were going to be copying _from_, then we
                // would need to restore the contents at this point
            }
            else if (valCode == VolatileImage.IMAGE_INCOMPATIBLE)
            {
                createBackBuffer();
            }
              // Now we've handled validation, get on with the rendering

              //
            // rendering to the back buffer:
            Graphics gBB = backBuffer.getGraphics();
            gBB.setColor(Color.WHITE);
            gBB.fillRect(1, 1, getWidth() - 1, getHeight() - 1);
            gBB.setColor(Color.BLACK);
            int locPos = pos;
            BufferedImage processed = wl.processImage(sprite[locPos]);
            gBB.drawImage(processed, 0, 0, this);

            // copy from the back buffer to the screen
            g.drawImage(backBuffer, 0, 0, this);

            // Now we are done; or are we?  Check contentsLost() and loop as necessary
        } while (backBuffer.contentsLost());
    }

    void createBackBuffer()
    {
        backBuffer = createVolatileImage(width, height);
    }

    @Override
    public JToolBar getControls()
    {
        return controls;
    }

    @Override
    public JComponent getGUI()
    {
        return this;
    }
    
    void controlPlay()
    {
        start();
        play.setEnabled(false);
        stop.setEnabled(true);
    }

    void controlStop()
    {
        stop();
        stop.setEnabled(false);
        play.setEnabled(true);
    }

    void controlSetPos()
    {
        if (!posBar.getValueIsAdjusting())
        {
            int pos = posBar.getValue();
            setFrame(pos);
        }
    }
}