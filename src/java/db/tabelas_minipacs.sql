--
-- PostgreSQL database dump
--

SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 3 (OID 2200)
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;

SET search_path = public, pg_catalog;

--
-- TOC entry 19 (OID 17645)
-- Name: img_patientlevel; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE img_patientlevel (
    paci_id character varying(64) NOT NULL,
    patnam character varying(64) NOT NULL,
    patsex character varying(16) NOT NULL,
    numpatrelstu numeric,
    numpatrelser numeric,
    numpatrelima numeric,
    "owner" character varying(16),
    patbirdat date,
    insertdatetime timestamp without time zone,
    patbirtim character varying(12)
);


--
-- TOC entry 20 (OID 17652)
-- Name: img_studylevel; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE img_studylevel (
    paci_id character varying(64) NOT NULL,
    patage character varying(4),
    patwei character varying(16),
    numsturelima numeric,
    tp_mod character varying(5),
    accnum character varying(16),
    stuid character varying(16) NOT NULL,
    stuinsuid character varying(64) NOT NULL,
    refphynam character varying(64),
    studes character varying(64),
    patsiz character varying(16),
    numsturelser numeric,
    "owner" character varying(16),
    size_bytes numeric,
    path_estudo character varying(100),
    flag_online numeric(1,0),
    studat date,
    stutim character varying(12),
    insertdatetime timestamp without time zone
);


--
-- TOC entry 21 (OID 17684)
-- Name: img_laudo; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE img_laudo (
    data date,
    status numeric,
    stuinsuid character varying(64) NOT NULL,
    usuario character varying(100),
    mini_laudo character varying(10000)
);


--
-- TOC entry 22 (OID 17691)
-- Name: img_tipo_pacs; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE img_tipo_pacs (
    tp_mod character varying(5) NOT NULL,
    nome character varying(200)
);


--
-- TOC entry 24 (OID 25791)
-- Name: img_serieslevel; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE img_serieslevel (
    serinsuid character varying(64) NOT NULL,
    stuinsuid character varying(64) NOT NULL,
    sernum character varying(20),
    bodparexa character varying(40),
    serdes character varying(64),
    viepos character varying(64),
    numserrelima bigint,
    insertdatetime timestamp without time zone,
    tp_mod character varying(5),
    "owner" character varying(16),
    size_bytes bigint,
    ser_path character varying(255)
) WITHOUT OIDS;


--
-- TOC entry 25 (OID 25799)
-- Name: img_imagelevel; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE img_imagelevel (
    sopinsuid character varying(64) NOT NULL,
    serinsuid character varying(64) NOT NULL,
    sopclauid character varying(64),
    samperpix bigint,
    phoint character varying(16),
    row_num bigint,
    col_num bigint,
    bitall bigint,
    bitsto bigint,
    pixrep bigint,
    patori character varying(16),
    imanum character varying(16),
    insertdatetime timestamp without time zone,
    "owner" character varying(16),
    size_bytes numeric,
    transfer character varying(64),
    img_path character varying(255)
) WITHOUT OIDS;


--
-- TOC entry 26 (OID 25810)
-- Name: img_configuracoes; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE img_configuracoes (
    cfg_nome character varying(150) NOT NULL,
    cfg_sequencia bigint NOT NULL,
    cfg_tipo character varying(50),
    cfg_separador character varying(5),
    cfg_valor character varying(255),
    cfg_descricao character varying(255)
) WITHOUT OIDS;


--
-- TOC entry 27 (OID 25817)
-- Name: img_ae_cliente; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE img_ae_cliente (
    aec_aetitle character varying(64) NOT NULL,
    aec_ip character varying(64),
    aec_port bigint,
    aec_descricao character varying(255),
    aec_arc_id bigint,
    aec_id bigint DEFAULT nextval('sq_img_ae_cliente'::text) NOT NULL
) WITHOUT OIDS;


--
-- TOC entry 28 (OID 25821)
-- Name: img_ae_servidor; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE img_ae_servidor (
    aes_aetitle character varying(64) NOT NULL,
    aes_ip character varying(64),
    aes_port bigint,
    aes_descricao character varying(255),
    aes_tipo_servidor bigint,
    aes_save_path character varying(255),
    aes_id bigint DEFAULT nextval('sq_img_ae_servidor'::text) NOT NULL
) WITHOUT OIDS;


--
-- TOC entry 29 (OID 25825)
-- Name: img_aec_x_aes; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE img_aec_x_aes (
    axa_can_write bigint,
    axa_save_path character varying(255),
    axa_aec_id bigint NOT NULL,
    axa_aes_id bigint NOT NULL
) WITHOUT OIDS;


--
-- TOC entry 30 (OID 25837)
-- Name: img_area_cliente; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE img_area_cliente (
    arc_id bigint NOT NULL,
    arc_nome character varying(255)
) WITHOUT OIDS;


--
-- TOC entry 31 (OID 25845)
-- Name: img_tipo_servidor; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE img_tipo_servidor (
    tps_id bigint NOT NULL,
    tps_nome character varying(50)
) WITHOUT OIDS;


--
-- TOC entry 32 (OID 25853)
-- Name: img_map_type; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE img_map_type (
    mat_id bigint NOT NULL,
    mat_des character varying(64)
) WITHOUT OIDS;


--
-- TOC entry 4 (OID 25857)
-- Name: sq_img_map_tables; Type: SEQUENCE; Schema: public; Owner: posgres
--

CREATE SEQUENCE sq_img_map_tables
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1
    CYCLE;


--
-- TOC entry 33 (OID 25861)
-- Name: img_map_tables; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE img_map_tables (
    map_dic_id bigint,
    map_table character varying(64),
    map_column character varying(64),
    map_type bigint,
    map_id bigint DEFAULT nextval('sq_img_map_tables'::text) NOT NULL
) WITHOUT OIDS;


--
-- TOC entry 34 (OID 25866)
-- Name: img_dicom_dict; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE img_dicom_dict (
    dic_id bigint DEFAULT nextval('sq_img_dicom_dict'::text) NOT NULL,
    dic_grupo character varying(4),
    dic_elemento character varying(4),
    dic_vr character varying(3),
    dic_descricao character varying(100)
) WITHOUT OIDS;


--
-- TOC entry 7 (OID 25870)
-- Name: sq_img_dicom_dict; Type: SEQUENCE; Schema: public; Owner: posgres
--

CREATE SEQUENCE sq_img_dicom_dict
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1
    CYCLE;


--
-- TOC entry 35 (OID 25881)
-- Name: img_midia_type; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE img_midia_type (
    mit_type_id bigint DEFAULT nextval('sq_img_midia_type'::text) NOT NULL,
    mit_name character varying(100),
    mit_capacity numeric
) WITHOUT OIDS;


--
-- TOC entry 9 (OID 25888)
-- Name: sq_img_midia_type; Type: SEQUENCE; Schema: public; Owner: posgres
--

CREATE SEQUENCE sq_img_midia_type
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1
    CYCLE;


--
-- TOC entry 36 (OID 25891)
-- Name: img_midias; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE img_midias (
    mid_id bigint DEFAULT nextval('sq_img_midias'::text) NOT NULL,
    mid_label character varying(64),
    mid_type_id bigint,
    mid_description character varying(255),
    mid_location character varying(100),
    mid_operator character varying(100)
) WITHOUT OIDS;


--
-- TOC entry 11 (OID 25895)
-- Name: sq_img_midias; Type: SEQUENCE; Schema: public; Owner: posgres
--

CREATE SEQUENCE sq_img_midias
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1
    CYCLE;


--
-- TOC entry 37 (OID 25902)
-- Name: img_mid_x_stu; Type: TABLE; Schema: public; Owner: posgres
--

CREATE TABLE img_mid_x_stu (
    mxs_id bigint DEFAULT nextval('sq_img_mid_x_stu'::text) NOT NULL,
    mxs_mid_id bigint,
    mxs_stuinsuid character varying(64)
) WITHOUT OIDS;


--
-- TOC entry 13 (OID 25906)
-- Name: sq_img_mid_x_stu; Type: SEQUENCE; Schema: public; Owner: posgres
--

CREATE SEQUENCE sq_img_mid_x_stu
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1
    CYCLE;

--
-- TOC entry 15 (OID 25917)
-- Name: sq_img_ae_cliente; Type: SEQUENCE; Schema: public; Owner: posgres
--

CREATE SEQUENCE sq_img_ae_cliente
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 17 (OID 25919)
-- Name: sq_img_ae_servidor; Type: SEQUENCE; Schema: public; Owner: posgres
--

CREATE SEQUENCE sq_img_ae_servidor
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Data for TOC entry 59 (OID 17691)
-- Name: img_tipo_pacs; Type: TABLE DATA; Schema: public; Owner: posgres1
--

COPY img_tipo_pacs (tp_mod, nome) FROM stdin;
PT	PET
CT	Tomografia Computadorizada
MN	Medicina Nuclear
MR	Ressonância Magnética
SC	Secondary Capture
US	Ultrasom
XA	Angiografia
CR	O que é mesmo?
\.


--
-- Data for TOC entry 62 (OID 25810)
-- Name: img_configuracoes; Type: TABLE DATA; Schema: public; Owner: posgres
--

COPY img_configuracoes (cfg_nome, cfg_sequencia, cfg_tipo, cfg_separador, cfg_valor, cfg_descricao) FROM stdin;
storage.path	4	string	\N	InstanceNumber	File Name
storage.path.study	0	string	\N	Modality	Caminho para o estudo!!!! 1o. Folder
storage.path.study	1	string	\N	StudyDate	2o. Folder
storage.path.study	2	string	\N	StudyID	3o. Folder
storage.path.series	0	string	\N	Modality	Caminho para a serie!!! 1o. Folder
storage.path.series	1	string	\N	StudyDate	2o. Folder
storage.path.series	2	string	\N	StudyID	3o. Folder
storage.path.series	3	string	\N	SeriesNumber	4o. Folder
sop.find	0	string	\N	StudyRootQueryRetrieveInformationModelFIND	UIDs para Find
syntax.find	0	string	\N	ExplicitVRLittleEndian	\N
syntax.find	1	string	\N	ImplicitVRLittleEndian	\N
sop.find	1	string	\N	PatientRootQueryRetrieveInformationModelFIND	\N
sop.find	2	string	\N	PatientStudyOnlyQueryRetrieveInformationModelFIND	\N
sop.find	3	string	\N	PatientRootQueryRetrieveInformationModelMOVE	\N
sop.find	4	string	\N	StudyRootQueryRetrieveInformationModelMOVE	\N
syntax.img	0	string	\N	ExplicitVRLittleEndian	Sintaxes de Transferencias aceitas pelo servidor
syntax.img	1	string	\N	ExplicitVRBigEndian	\N
syntax.img	2	string	\N	ImplicitVRLittleEndian	\N
syntax.img	3	string	\N	JPEGLossless14	\N
syntax.img	4	string	\N	JPEGLossless	\N
syntax.img	5	string	\N	JPEGLSLossless	\N
syntax.img	6	string	\N	JPEG2000Lossless	\N
syntax.img	7	string	\N	RLELossless	\N
syntax.img	8	string	\N	JPEGBaseline	\N
syntax.img	9	string	\N	JPEGExtended	\N
syntax.img	10	string	\N	JPEGLSLossy	\N
syntax.img	11	string	\N	JPEG2000Lossy	\N
sop.storage	0	string	\N	HardcopyGrayscaleImageStorage	UIDs aceitas pelo servidor p/ Storage
sop.storage	1	string	\N	HardcopyColorImageStorage	\N
sop.storage	2	string	\N	ComputedRadiographyImageStorage	\N
sop.storage	3	string	\N	DigitalXRayImageStorageForPresentation	\N
sop.storage	4	string	\N	DigitalXRayImageStorageForProcessing	\N
sop.storage	5	string	\N	DigitalMammographyXRayImageStorageForPresentation	\N
sop.storage	6	string	\N	DigitalMammographyXRayImageStorageForProcessing	\N
sop.storage	7	string	\N	DigitalIntraoralXRayImageStorageForPresentation	\N
sop.storage	8	string	\N	DigitalIntraoralXRayImageStorageForProcessing	\N
sop.storage	9	string	\N	CTImageStorage	\N
sop.storage	10	string	\N	UltrasoundMultiframeImageStorageRetired	\N
sop.storage	11	string	\N	UltrasoundMultiframeImageStorage	\N
sop.storage	12	string	\N	MRImageStorage	\N
sop.storage	13	string	\N	EnhancedMRImageStorage	\N
sop.storage	14	string	\N	MRSpectroscopyStorage	\N
sop.storage	15	string	\N	NuclearMedicineImageStorageRetired	\N
sop.storage	16	string	\N	UltrasoundImageStorageRetired	\N
sop.storage	17	string	\N	UltrasoundImageStorage	\N
sop.storage	18	string	\N	SecondaryCaptureImageStorage	\N
sop.storage	19	string	\N	MultiframeSingleBitSecondaryCaptureImageStorage	\N
sop.storage	20	string	\N	MultiframeGrayscaleByteSecondaryCaptureImageStorage	\N
sop.storage	21	string	\N	MultiframeGrayscaleWordSecondaryCaptureImageStorage	\N
sop.storage	22	string	\N	MultiframeColorSecondaryCaptureImageStorage	\N
sop.storage	23	string	\N	XRayAngiographicImageStorage	\N
sop.storage	24	string	\N	XRayRadiofluoroscopicImageStorage	\N
sop.storage	25	string	\N	XRayAngiographicBiPlaneImageStorageRetired	\N
sop.storage	26	string	\N	NuclearMedicineImageStorage	\N
sop.storage	27	string	\N	VLImageStorageRetired	\N
sop.storage	28	string	\N	VLMultiframeImageStorageRetired	\N
sop.storage	29	string	\N	VLEndoscopicImageStorage	\N
sop.storage	30	string	\N	VLMicroscopicImageStorage	\N
sop.storage	31	string	\N	VLSlideCoordinatesMicroscopicImageStorage	\N
sop.storage	32	string	\N	VLPhotographicImageStorage	\N
sop.storage	33	string	\N	PositronEmissionTomographyImageStorage	\N
sop.storage	34	string	\N	RTImageStorage	\N
storage.path	0	string	\N	Modality	1o. Folder
storage.path	1	string	\N	StudyDate	2o. Folder
storage.path	2	string	\N	StudyID	3o. Folder
storage.path	3	string	\N	SeriesNumber	4o. Folder
\.


--
-- Data for TOC entry 66 (OID 25837)
-- Name: img_area_cliente; Type: TABLE DATA; Schema: public; Owner: posgres
--

COPY img_area_cliente (arc_id, arc_nome) FROM stdin;
1	Hemodinamica
2	Ressonancia
3	Tomografia
4	Medicina Nuclear
5	Informatica
\.


--
-- Data for TOC entry 67 (OID 25845)
-- Name: img_tipo_servidor; Type: TABLE DATA; Schema: public; Owner: posgres
--

COPY img_tipo_servidor (tps_id, tps_nome) FROM stdin;
1	Armazenamento
2	Impressao
3	Worklist
\.


--
-- Data for TOC entry 68 (OID 25853)
-- Name: img_map_type; Type: TABLE DATA; Schema: public; Owner: posgres
--

COPY img_map_type (mat_id, mat_des) FROM stdin;
0	String
1	Integer
2	Date
\.


--
-- Data for TOC entry 69 (OID 25861)
-- Name: img_map_tables; Type: TABLE DATA; Schema: public; Owner: posgres
--

COPY img_map_tables (map_dic_id, map_table, map_column, map_type, map_id) FROM stdin;
166	IMG_PATIENTLEVEL	PACI_ID	0	2
165	IMG_PATIENTLEVEL	PATNAM	0	3
168	IMG_PATIENTLEVEL	PATBIRDAT	2	4
169	IMG_PATIENTLEVEL	PATBIRTIM	0	5
170	IMG_PATIENTLEVEL	PATSEX	0	6
491	IMG_PATIENTLEVEL	NUMPATRELSTU	1	7
492	IMG_PATIENTLEVEL	NUMPATRELSER	1	8
493	IMG_PATIENTLEVEL	NUMPATRELIMA	1	9
78	IMG_STUDYLEVEL	ACCNUM	0	10
456	IMG_STUDYLEVEL	STUINSUID	0	11
108	IMG_STUDYLEVEL	STUDES	0	12
458	IMG_STUDYLEVEL	STUID	0	14
175	IMG_STUDYLEVEL	PATAGE	0	15
177	IMG_STUDYLEVEL	PATWEI	0	16
176	IMG_STUDYLEVEL	PATSIZ	0	17
83	IMG_STUDYLEVEL	TP_MOD	0	18
494	IMG_STUDYLEVEL	NUMSTURELSER	1	19
495	IMG_STUDYLEVEL	NUMSTURELIMA	1	20
92	IMG_STUDYLEVEL	REFPHYNAM	0	21
457	IMG_SERIESLEVEL	SERINSUID	0	22
459	IMG_SERIESLEVEL	SERNUM	0	23
201	IMG_SERIESLEVEL	BODPAREXA	0	24
110	IMG_SERIESLEVEL	SERDES	0	25
412	IMG_SERIESLEVEL	VIEPOS	0	26
62	IMG_IMAGELEVEL	SOPINSUID	0	27
61	IMG_IMAGELEVEL	SOPCLAUID	0	28
508	IMG_IMAGELEVEL	SAMPERPIX	1	29
509	IMG_IMAGELEVEL	PHOINT	0	30
514	IMG_IMAGELEVEL	ROW_NUM	1	31
515	IMG_IMAGELEVEL	COL_NUM	1	32
526	IMG_IMAGELEVEL	BITALL	1	33
527	IMG_IMAGELEVEL	BITSTO	1	34
529	IMG_IMAGELEVEL	PIXREP	1	35
467	IMG_IMAGELEVEL	PATORI	0	36
461	IMG_IMAGELEVEL	IMANUM	0	37
84	IMG_STUDYLEVEL	TP_MOD	0	38
496	IMG_SERIESLEVEL	NUMSERRELIMA	1	39
63	IMG_STUDYLEVEL	STUDAT	2	13
\.


--
-- Data for TOC entry 70 (OID 25866)
-- Name: img_dicom_dict; Type: TABLE DATA; Schema: public; Owner: posgres
--

COPY img_dicom_dict (dic_id, dic_grupo, dic_elemento, dic_vr, dic_descricao) FROM stdin;
1	0000	0000	UL	CMD Group Length
2	0000	0002	UI	CMD Affected SOP Class UID
3	0000	0003	UI	CMD Requested SOP Class UID
4	0000	0100	US	CMD Command Field
5	0000	0110	US	CMD Message ID
6	0000	0120	US	CMD Message ID Responded to
7	0000	0600	AE	CMD Move Destination
8	0000	0700	US	CMD Priority
9	0000	0800	US	CMD Data Set Type
10	0000	0900	US	CMD Status
11	0000	0901	AT	CMD Offending Element
12	0000	0902	LO	CMD Error Comment
13	0000	0903	US	CMD Error ID
14	0000	1001	UI	CMD SOP Requested Instance UID
15	0000	1000	UI	CMD SOP Affected Instance UID
16	0000	1002	US	CMD Event Type ID
17	0000	1008	US	CMD Action Type ID
18	0000	1020	US	CMD Remaining Suboperations
19	0000	1021	US	CMD Completed Suboperations
20	0000	1022	US	CMD Failed Suboperations
21	0000	1023	US	CMD Warning Suboperations
22	0000	1030	AE	CMD AE Title
23	0000	1031	US	CMD Message ID
24	0000	1005	AT	CMD Attribute Identifier List
25	0002	0000	UL	META Group Length
26	0002	0001	OB	META File Meta Information Version
27	0002	0002	UI	META Media Stored SOP Class UID
28	0002	0003	UI	META Media Stored SOP Instance UID
29	0002	0010	UI	META Transfer Syntax UID
30	0002	0012	UI	META Implementation Class UID
31	0002	0013	SH	META Implementation Version Name
32	0002	0016	AE	META Source Application Entity Title
33	0002	0100	UI	META Private Information Creator
34	0002	0102	OB	META Private Information
35	0004	1130	CS	DIR File-set ID
36	0004	1141	CS	DIR File-set descriptor ID
37	0004	1142	CS	DIR Specific character set
38	0004	1200	UL	DIR Offset of the first dir of root dir entity
39	0004	1202	UL	DIR Offset of the last dir of root dir entity
40	0004	1212	US	DIR File-set consistency flag
41	0004	1220	SQ	DIR Directory record sequence
42	0004	1400	UL	DIR Offset of next directory record
43	0004	1410	US	DIR Record in use flag
44	0004	1420	UL	DIR Offset of referenced lower-level dir entity
45	0004	1430	CS	DIR Directory Record Type
46	0004	1432	UI	DIR Private Record UID
47	0004	1500	CS	DIR Referenced File ID
48	0004	1504	UL	DIR Directory Record Offset
49	0004	1510	UI	DIR Referenced SOP Class UID in File
50	0004	1511	UI	DIR Referenced SOP Instance UID in File
51	0004	1512	UI	DIR Referenced Transfer Syntax in File
52	0004	1600	UL	DIR Number of References
53	0008	0000	UL	ID Group Length
54	0008	0001	UL	ID Length to End (RET)
55	0008	0005	CS	ID Specific Character Set
56	0008	0008	CS	ID Image Type
57	0008	0010	RE	 ID Recognition Code (RET)
58	0008	0012	DA	ID Instance Creation Date
59	0008	0013	TM	ID Instance Creation Time
60	0008	0014	UI	ID Instance Creator UID
61	0008	0016	UI	ID SOP Class UID
62	0008	0018	UI	ID SOP Instance UID
63	0008	0020	DA	ID Study Date
64	0008	0021	DA	ID Series Date
65	0008	0022	DA	ID Acquisition Date
66	0008	0023	DA	ID Image Date
67	0008	0024	DA	ID Overlay Date
68	0008	0025	DA	ID Curve Date
69	0008	0030	TM	ID Study Time
70	0008	0031	TM	ID Series Time
71	0008	0032	TM	ID Acquisition Time
72	0008	0033	TM	ID Image Time
73	0008	0034	TM	ID Overlay Time
74	0008	0035	TM	ID Curve Time
75	0008	0040	RE	 ID Data Set Type (RET)
76	0008	0041	RE	 ID Data Set Subtype (RET)
77	0008	0042	CS	ID Nuc Med Series Type (RET)
78	0008	0050	SH	ID Accession Number
79	0008	0052	CS	ID Query Level
80	0008	0054	AE	ID Retrieve AE Title
81	0008	0056	CS	ID Instance Availability
82	0008	0058	UI	ID Failed SOP Instances
83	0008	0060	CS	ID Modality
84	0008	0061	CS	ID Modalities in Study
85	0008	0062	SQ	ID Modality Subtype
86	0008	0068	CS	ID Presentation Intent Type
87	0008	0064	CS	ID Conversion Type
88	0008	0070	LO	ID Manufacturer
89	0008	0080	LO	ID Institution Name
90	0008	0081	ST	ID Institution Address
91	0008	0082	SQ	ID Institution Code Sequence
92	0008	0090	PN	ID Referring Physician's Name
93	0008	0092	ST	ID Referring Physician's Address
94	0008	0094	SH	ID Referring Physician's Telephone
95	0008	0100	SH	ID Code Value
96	0008	0102	SH	ID Coding Scheme Designator
97	0008	0103	SH	ID Coding Scheme Version
98	0008	0104	LO	ID Code Meaning
99	0008	0105	CS	ID Mapping Resource
100	0008	0106	DT	ID Context Group Version
101	0008	010b	CS	ID Code Set Extension Flag
102	0008	010c	UI	ID Private Coding Scheme Creator UID
103	0008	010d	UI	ID Coding Scheme Creator UID
104	0008	010e	SQ	ID Mapping Resource Sequence
105	0008	010f	CS	ID Context Identifier
106	0008	1000	LO	ID Network ID (RET)
107	0008	1010	SH	ID Station Name
108	0008	1030	LO	ID Study Description
109	0008	1032	SQ	ID Procedure Code Sequence
110	0008	103e	LO	ID Series Description
111	0008	1040	LO	ID Institutional Department Name
112	0008	1048	PN	ID Physician of Record
113	0008	1050	PN	ID Performing Physician's Name
114	0008	1060	PN	ID Name of Physician(s) Reading Study
115	0008	1070	PN	ID Operator's Name
116	0008	1080	LO	ID Admitting Diagnoses Description
117	0008	1084	SQ	ID Admitting Diagnosis Code Sequence
118	0008	1090	LO	ID Manufacturer Model Name
119	0008	1100	SQ	ID Referenced Results Sequence
120	0008	1110	SQ	ID Referenced Study Sequence
121	0008	1111	SQ	ID Referenced Study Component Sequence
122	0008	1115	SQ	ID Referenced Series Sequence
123	0008	1120	SQ	ID Referenced Patient Sequence
124	0008	1125	SQ	ID Referenced Visit Sequence
125	0008	1130	SQ	ID Referenced Overlay Sequence
126	0008	1140	SQ	ID Referenced Image Sequence
127	0008	1145	SQ	ID Referenced Curve Sequence
128	0008	1148	SQ	ID Referenced Previous Waveform
129	0008	114a	SQ	ID Referenced Simultaneous Waveforms
130	0008	114c	SQ	ID Referenced Subsequent Waveform
131	0008	1150	UI	ID Referenced SOP Class UID
132	0008	1155	UI	ID Referenced SOP Instance UID
133	0008	1160	IS	ID Referenced Frame Number
134	0008	1195	UI	ID Transaction UID
135	0008	1197	US	ID Failure Reason
136	0008	1198	SQ	ID Failed SOP Sequence
137	0008	1199	SQ	ID Referenced SOP Sequence
138	0008	2110	CS	ID Lossy Image Compression (RET)
139	0008	2111	ST	ID Derivation Description
140	0008	2112	SQ	ID Source Image Sequence
141	0008	2120	SH	ID Stage Name
142	0008	2122	IS	ID Stage Number
143	0008	2124	IS	ID Number of Stages
144	0008	2128	IS	ID View Number
145	0008	2129	IS	ID Number of Event Timers
146	0008	212a	IS	ID Number of Views in Stage
147	0008	2130	DS	ID Event Elapsed Time(s)
148	0008	2132	LO	ID Event Event Timer Name(s)
149	0008	2142	IS	ID Start Trim
150	0008	2143	IS	ID Stop Trim
151	0008	2144	IS	ID Recommended Display Frame Rate
152	0008	2200	CS	ID Transducer Position (RET)
153	0008	2204	CS	ID Transducer Orientation (RET)
154	0008	2208	CS	ID Anatomic Structure (RET)
155	0008	2218	SQ	ID Anatomic Region of Interest Sequence
156	0008	2220	SQ	ID Anatomic Region Modifier Sequence
157	0008	2228	SQ	ID Primary Anatomic Structure Sequence
158	0008	2230	SQ	ID Primary Anatomic Structure Modifier Sequence
159	0008	2240	SQ	ID Transducer Position Sequence
160	0008	2242	SQ	ID Transducer Position Modifer Sequence
161	0008	2244	SQ	ID Transducer Orientation Sequence
162	0008	2246	SQ	ID Transducer Orientation Modifer Sequence
163	0008	4000	RE	 ID Comments (RET)
164	0010	0000	UL	PAT Group Length
165	0010	0010	PN	PAT Patient Name
166	0010	0020	LO	PAT Patient ID
167	0010	0021	LO	PAT Issuer of Patient ID
168	0010	0030	DA	PAT Patient Birthdate
169	0010	0032	TM	PAT Patient Birth Time
170	0010	0040	CS	PAT Patient Sex
171	0010	0050	SQ	PAT Patient's Insurance Plan Code Sequence
172	0010	1000	LO	PAT Other Patient IDs
173	0010	1001	PN	PAT Other Patient Names
174	0010	1005	PN	PAT Patient's Birth Name
175	0010	1010	AS	PAT Patient Age
176	0010	1020	DS	PAT Patient Size
177	0010	1030	DS	PAT Patient Weight
178	0010	1040	LO	PAT Patient Address
179	0010	1050	RE	 PAT Insurance Plan Identifier
180	0010	1060	PN	PAT Patient's Mother's Birth Name
181	0010	1080	LO	PAT Military Rank
182	0010	1081	LO	PAT Branch of Service
183	0010	1090	LO	PAT Medical Record Locator
184	0010	2000	LO	PAT Medical Alerts
185	0010	2110	LO	PAT Contrast Allergies
186	0010	2150	LO	PAT Country of Residence
187	0010	2152	LO	PAT Region of Residence
188	0010	2154	SH	PAT Patient's Telephone Numbers
189	0010	2160	SH	PAT Ethnic Group
190	0010	2180	SH	PAT Occupation
191	0010	21a0	CS	PAT Smoking Status
192	0010	21b0	LT	PAT Additional Patient History
193	0010	21c0	US	PAT Pregnancy Status
194	0010	21d0	DA	PAT Last Menstrual Date
195	0010	21f0	LO	PAT Religious Preference
196	0010	4000	LT	PAT Comments
197	0018	0000	UL	ACQ Group Length
198	0018	0010	LO	ACQ Contrast/Bolus Agent
199	0018	0012	SQ	ACQ Contrast/Bolus Agent Sequence
200	0018	0014	SQ	ACQ Contrast/Bolus Administration Route Seq
201	0018	0015	CS	ACQ Body Part Examined
202	0018	0020	CS	ACQ Scanning Sequence
203	0018	0021	CS	ACQ Sequence Variant
204	0018	0022	CS	ACQ Scan Options
205	0018	0023	CS	ACQ MR Acquisition Type
206	0018	0024	SH	ACQ Sequence Name
207	0018	0025	CS	ACQ Angio Flag
208	0018	0026	SQ	ACQ Intervention Drug Information Sequence
209	0018	0027	TM	ACQ Intervention Drug Stop Time
210	0018	0028	DS	ACQ Intervention Drug Dose
211	0018	0029	SQ	ACQ Intervention Drug Code Sequence
212	0018	002a	SQ	ACQ Additional Drug Sequence
213	0018	0030	LO	ACQ Radionuclide (RET)
214	0018	0031	LO	ACQ Radiopharmaceutical
215	0018	0032	DS	ACQ Energy Window Centerline (RET)
216	0018	0033	DS	ACQ Energy Window Total Width (RET)
217	0018	0034	LO	ACQ Intervention Drug Name
218	0018	0035	TM	ACQ Intervention Drug Start Time
219	0018	0036	SQ	ACQ Interventional Therapy Sequence
220	0018	0037	CS	ACQ Therapy type
221	0018	0038	CS	ACQ Interventional status
222	0018	0039	CS	ACQ Therapy descriptionm
223	0018	0040	IS	ACQ Cine Rate
224	0018	0050	DS	ACQ Slice Thickness
225	0018	0060	DS	ACQ KVP
226	0018	0070	IS	ACQ Counts Accumulated
227	0018	0071	CS	ACQ Acquisition Termination Condition
228	0018	0072	DS	ACQ Effective Series Duration
229	0018	0073	CS	ACQ Start Condition
230	0018	0074	IS	ACQ Start Condition Data
231	0018	0075	IS	ACQ Termination Condition Data
232	0018	0080	DS	ACQ Repetition Time
233	0018	0081	DS	ACQ Echo Time
234	0018	0082	DS	ACQ Inversion Time
235	0018	0083	DS	ACQ Number of Averages
236	0018	0084	DS	ACQ Imaging Frequency
237	0018	0085	SH	ACQ Imaged Nucleus
238	0018	0086	IS	ACQ Echo Number
239	0018	0087	DS	ACQ Magnetic Field Strength
240	0018	0088	DS	ACQ Spacing Between Slices
241	0018	0089	IS	ACQ Number of Phase Encoding Steps
242	0018	0090	DS	ACQ Data Collection Diameter
243	0018	0091	IS	ACQ Echo Train Length
244	0018	0093	DS	ACQ Percent Sampling
245	0018	0094	DS	ACQ Percent Phase Field of View
246	0018	0095	DS	ACQ Pixel Bandwidth
247	0018	1000	LO	ACQ Device Serial Number
248	0018	1004	LO	ACQ Plate ID
249	0018	1010	LO	ACQ Secondary Capture Device ID
250	0018	1012	DA	ACQ Date of Secondary Capture
251	0018	1014	TM	ACQ Time of Secondary Capture
252	0018	1016	LO	ACQ Secondary Capture Device Manufacturer
253	0018	1018	LO	ACQ Secondary Capture Device Model Name
254	0018	1019	LO	ACQ Secondary Capture Device Software Version
255	0018	1020	LO	ACQ Software Version
256	0018	1022	SH	ACQ Video Image Format Acquired
257	0018	1023	LO	ACQ Digital Image Format Acquired
258	0018	1030	LO	ACQ Protocol Name
259	0018	1040	LO	ACQ Contrast/Bolus Route
260	0018	1041	DS	ACQ Contrast/Bolus Volume
261	0018	1042	TM	ACQ Contrast/Bolus Start Time
262	0018	1043	TM	ACQ Contrast/Bolus Stop Time
263	0018	1044	DS	ACQ Contrast/Bolus Total Dose
264	0018	1045	IS	ACQ Syringe Counts
265	0018	1046	DS	ACQ Contrast Flow Rate (ml/sec)
266	0018	1047	DS	ACQ Contrast Flow Duration (sec)
267	0018	1048	CS	ACQ Contrast Bolus Ingredient
268	0018	1049	DS	ACQ Contrast Bolus Ingredient Concentration
269	0018	1050	DS	ACQ Spatial Resolution
270	0018	1060	DS	ACQ Trigger Time
271	0018	1061	LO	ACQ Trigger Source or Type
272	0018	1062	IS	ACQ Nominal Interval
273	0018	1063	DS	ACQ Frame Time
274	0018	1064	LO	ACQ Framing Type
275	0018	1065	DS	ACQ Frame Time Vector
276	0018	1066	DS	ACQ Frame Delay
277	0018	1067	DS	ACQ Image Trigger Delay
278	0018	1068	DS	ACQ Group Time Offset
279	0018	1069	DS	ACQ Trigger Time Offset
280	0018	106a	CS	ACQ Synchronization Trigger
281	0018	106b	UI	ACQ Synchronization Frame of Reference
282	0018	106e	UL	ACQ Trigger Sample Position
283	0018	1070	LO	ACQ Radiopharmaceutical Route
284	0018	1071	DS	ACQ Radiopharmaceutical Volume
285	0018	1072	TM	ACQ Radiopharmaceutical Start Time
286	0018	1073	TM	ACQ Radiopharmaceutical Stop Time
287	0018	1074	DS	ACQ Radionuclide Total Dose
288	0018	1075	DS	ACQ Radionuclide Half Life
289	0018	1076	DS	ACQ Radionuclide Positron Fraction
290	0018	1077	DS	ACQ Radiopharmaceutical Specific Activity
291	0018	1080	CS	ACQ Beat Rejection Flag
292	0018	1081	IS	ACQ Low R-R Value
293	0018	1082	IS	ACQ High R-R Value
294	0018	1083	IS	ACQ Intervals Acquired
295	0018	1084	IS	ACQ Intervals Rejected
296	0018	1085	LO	ACQ PVC Rejection
297	0018	1086	IS	ACQ Skip Beats
298	0018	1088	IS	ACQ Heart Rate
299	0018	1090	IS	ACQ Cardiac Number of Images
300	0018	1094	IS	ACQ Trigger Window
301	0018	1100	DS	ACQ Reconstruction Diameter
302	0018	1110	DS	ACQ Distance Source-Detector
303	0018	1111	DS	ACQ Distance Source-Patient
304	0018	1114	DS	ACQ Estimated Radiographic Mag Factor
305	0018	1120	DS	ACQ Gantry/Detector Tilt
306	0018	1121	DS	ACQ Gantry/Detector Slew
307	0018	1130	DS	ACQ Table Height
308	0018	1131	DS	ACQ Table Traverse
309	0018	1134	CS	ACQ Table Motion (STATIC, DYNAMIC)
310	0018	1135	DS	ACQ Table Vertical Increment (mm)
311	0018	1136	DS	ACQ Table Lateral Increment (mm)
312	0018	1137	DS	ACQ Table Longitudinal Increment (mm)
313	0018	1138	DS	ACQ Table Angle (relative to horizontal: deg)
314	0018	1140	CS	ACQ Rotation Direction
315	0018	1141	DS	ACQ Angular Position
316	0018	1142	DS	ACQ Radial Position
317	0018	1143	DS	ACQ Scan Arc
318	0018	1144	DS	ACQ Angular Step
319	0018	1145	DS	ACQ Center of Rotation Offset
320	0018	1146	DS	ACQ Rotation Offset (RET)
321	0018	1147	CS	ACQ Field of View Shape
322	0018	1149	IS	ACQ Field of View Dimension(s)
323	0018	1150	IS	ACQ Exposure Time
324	0018	1151	IS	ACQ X-ray Tube Current
325	0018	1152	IS	ACQ Exposure
326	0018	1154	DS	ACQ Average width of X-Ray pulse (ms)
327	0018	1155	CS	ACQ General level of X-Ray dose exposure
328	0018	115a	CS	ACQ X-Ray radiation mode (CONTINUOUS, PULSED)
329	0018	115e	DS	ACQ X-Ray dose to which patient was exposed
330	0018	1160	SH	ACQ Filter Type, extremity
331	0018	1161	LO	ACQ Type of filter(s) inserted into X-Ray beam
332	0018	1162	DS	ACQ Intensifier Size (mm)
333	0018	1164	DS	ACQ Image Pixel Spacing
334	0018	1166	CS	ACQ Grid (IN, NONE)
335	0018	1170	IS	ACQ Generator Power
336	0018	1180	SH	ACQ Collimator/Grid Name
337	0018	1181	CS	ACQ Collimator Type
338	0018	1182	IS	ACQ Focal Distance
339	0018	1183	DS	ACQ X Focus Center
340	0018	1184	DS	ACQ Y Focus Center
341	0018	1190	DS	ACQ Focal Spot
342	0018	1200	DA	ACQ Date of Last Calibration
343	0018	1201	TM	ACQ Time of Last Calibration
344	0018	1210	SH	ACQ Convolution Kernel
345	0018	1240	RE	 ACQ Upper/Lower Pixel Values (RET)
346	0018	1242	IS	ACQ Actual Frame Duration
347	0018	1243	IS	ACQ Count Rate
348	0018	1244	US	ACQ Preferred Playback Sequencing
349	0018	1250	SH	ACQ Receiving Coil
350	0018	1251	SH	ACQ Transmitting Coil
351	0018	1260	SH	ACQ Plate Type
352	0018	1261	LO	ACQ Phosphor Type
353	0018	1300	DS	ACQ Scan Velocity
354	0018	1301	CS	ACQ Whole Body Technique
355	0018	1302	IS	ACQ Scan Length
356	0018	1310	US	ACQ Acquisition Matrix
357	0018	1312	CS	ACQ Phase Encoding Direction
358	0018	1314	DS	ACQ Flip Angle
359	0018	1315	CS	ACQ Variable Flip Angle
360	0018	1316	DS	ACQ SAR
361	0018	1318	DS	ACQ DB/DT
362	0018	1400	LO	ACQ Acquisition Device Processing Description
363	0018	1401	LO	ACQ Acquisition Device Processing Code
364	0018	1402	CS	ACQ Cassette Orientation
365	0018	1403	CS	ACQ Cassette Size
366	0018	1404	US	ACQ Exposures on Plate
367	0018	1405	IS	ACQ Relative X-ray Exposure
368	0018	1450	CS	ACQ Column Angulation
369	0018	1460	DS	ACQ Tomo Layer Height (mm)
370	0018	1470	DS	ACQ Tomo Angle
371	0018	1480	DS	ACQ Tomo Time
372	0018	1500	CS	ACQ Positioner Motion
373	0018	1510	DS	ACQ Positioner Primary Angle
374	0018	1511	DS	ACQ Positioner Secondary Angle
375	0018	1520	DS	ACQ Positioner Primary Angle Increment
376	0018	1521	DS	ACQ Positioner Secondary Angle Increment
377	0018	1530	DS	ACQ Detector Primary Angle
378	0018	1531	DS	ACQ Detector Secondary Angle
379	0018	1600	CS	ACQ Shutter Shape
380	0018	1602	IS	ACQ Shutter Left Vertical Edge
381	0018	1604	IS	ACQ Shutter Right Vertical Edge
382	0018	1606	IS	ACQ Shutter Upper Horizontal Edge
383	0018	1608	IS	ACQ Shutter Lower Horizontal Edge
384	0018	1610	IS	ACQ Center of Circular Shutter
385	0018	1612	IS	ACQ Radius of Circular Shutter
386	0018	1620	IS	ACQ Vertices of the Polygonal Shutter
387	0018	1700	CS	ACQ Collimator Shape
388	0018	1702	IS	ACQ Collimator Left Vertical Edge
389	0018	1704	IS	ACQ Collimator Right Vertical Edge
390	0018	1706	IS	ACQ Collimator Upper Horizontal Edge
391	0018	1708	IS	ACQ Collimator Lower Horizontal Edge
392	0018	1710	IS	ACQ Center of Circular Collimator
393	0018	1712	IS	ACQ Radius of Circular Collimator
394	0018	1720	IS	ACQ Vertices of the Polygonal Collimator
395	0018	1800	CS	ACQ Acquisition Time Synchronized
396	0018	1801	SH	ACQ Time Source
397	0018	1802	CS	ACQ Time Distribution Protocol
398	0018	4000	RE	 ACQ Comments
399	0018	5000	SH	ACQ Output Power
400	0018	5010	LO	ACQ Transducer Data
401	0018	5012	DS	ACQ Focus Depth
402	0018	5020	LO	ACQ Processing Function
403	0018	5021	LO	ACQ Postprocessing Function
404	0018	5022	DS	ACQ Mechanical Index
405	0018	5024	DS	ACQ Thermal Index
406	0018	5026	DS	ACQ Cranial Thermal Index
407	0018	5027	DS	ACQ Soft Tissue Thermal Index
408	0018	5028	DS	ACQ Soft Tissue-focus Thermal Index
409	0018	5029	CS	ACQ Soft Tissue-surface Thermal Index
410	0018	5050	IS	ACQ Depth of Scan Field
411	0018	5100	CS	ACQ Patient Position
412	0018	5101	CS	ACQ View Position
413	0018	5210	DS	ACQ Image Transformation Matrix
414	0018	5212	DS	ACQ Image Translation Vector
415	0018	6000	DS	ACQ Sensitivity
416	0018	6011	SQ	ACQ Ultrasound Region Sequence
417	0018	6012	US	ACQ Region Spatial Format
418	0018	6014	US	ACQ Region Data Type
419	0018	6016	UL	ACQ Region Flags
420	0018	6018	UL	ACQ Region Location Min X(0)
421	0018	601a	UL	ACQ Region Location Min Y(0)
422	0018	601c	UL	ACQ Region Location Max X(1)
423	0018	601e	UL	ACQ Region Location Max Y(1)
424	0018	6020	SL	ACQ Reference Pixel X
425	0018	6022	SL	ACQ Reference Pixel Y
426	0018	6024	US	ACQ Physical Units X Direction
427	0018	6026	US	ACQ Physical Units Y Direction
428	0018	6028	FD	ACQ Reference Pixel Physical Value X
429	0018	602a	FD	ACQ Reference Pixel Physical Value Y
430	0018	602c	FD	ACQ Physical Delta X
431	0018	602e	FD	ACQ Physical Delta Y
432	0018	6030	UL	ACQ Transducer Frequency
433	0018	6031	CS	ACQ Transducer Type
434	0018	6032	UL	ACQ Pulse Repetition Frequency
435	0018	6034	FD	ACQ Doppler Correction Angle
436	0018	6036	FD	ACQ Sterring Angle
437	0018	6038	UL	ACQ Doppler Sample Volume X Position
438	0018	603a	UL	ACQ Doppler Sample Volume Y Position
439	0018	603c	UL	ACQ TM-Line Position X(0)
440	0018	603e	UL	ACQ TM-Line Position Y(0)
441	0018	6040	UL	ACQ TM-Line Position X(1)
442	0018	6042	UL	ACQ TM-Line Position Y(1)
443	0018	6044	US	ACQ Pixel Component Organization
444	0018	6046	UL	ACQ Pixel Component Mask
445	0018	6048	UL	ACQ Pixel Component Range Start
446	0018	604a	UL	ACQ Pixel Component Range Stop
447	0018	604c	US	ACQ Pixel Component Physical Units
448	0018	604e	US	ACQ Pixel Component Data Type
449	0018	6050	UL	ACQ Number of Table Break Points
450	0018	6052	UL	ACQ Table of X Break Points
451	0018	6054	FD	ACQ Table of Y Break Points
452	0018	6056	UL	ACQ Number of Table Entries
453	0018	6058	UL	ACQ Table of Pixel Values
454	0018	605a	FL	ACQ Table of Parameter Values
455	0020	0000	UL	REL Group Length
456	0020	000d	UI	REL Study Instance UID
457	0020	000e	UI	REL Series Instance UID
458	0020	0010	SH	REL Study ID
459	0020	0011	IS	REL Series Number
460	0020	0012	IS	REL Acquisition Number
461	0020	0013	IS	REL Image Number
462	0020	0014	IS	REL Isotope Number (RET)
463	0020	0015	IS	REL Phase Number (RET)
464	0020	0016	IS	REL Interval Number (RET)
465	0020	0017	IS	REL Time Slot Number (RET)
466	0020	0018	IS	REL Angle Number (RET)
467	0020	0020	CS	REL Patient Orientation
468	0020	0022	IS	REL Overlay Number
469	0020	0024	IS	REL Curve Number
470	0020	0026	IS	REL Looup Table Number
471	0020	0030	RE	 REL Image Position (RET)
472	0020	0032	DS	REL Image Position Patient
473	0020	0035	RE	 REL Image Orientation
474	0020	0037	DS	REL Image Orientation (Patient)
475	0020	0050	RE	 REL Location (RET)
476	0020	0052	UI	REL Frame of Reference UID
477	0020	0060	CS	REL Laterality
478	0020	0070	RE	 REL Image Geometry Type (RET)
479	0020	0080	RE	 REL Masking Image (RET)
480	0020	0100	IS	REL Temporal Position Identifier
481	0020	0105	IS	REL Number of Temporal Positions
482	0020	0110	DS	REL Temporal Resolution
483	0020	1000	IS	REL Series in Study
484	0020	1001	RE	 REL Acquisitions in Series
485	0020	1002	IS	REL Images in Acquisition
486	0020	1004	IS	REL Acquisitions in Study
487	0020	1020	RE	 REL Reference (RET)
488	0020	1040	LO	REL Position Reference Indicator
489	0020	1041	DS	REL Slice Location
490	0020	1070	IS	REL Other Study Numbers
491	0020	1200	IS	REL Number of Patient Related Studies
492	0020	1202	IS	REL Number of Patient Related Series
493	0020	1204	IS	REL Number of Patient Related Instances
494	0020	1206	IS	REL Number of Study Related Series
495	0020	1208	IS	REL Number of Study Related Instances
496	0020	1209	IS	REL Number of Series Related Instances
497	0020	3100	RE	 REL Source Image IDs (RET)
498	0020	3401	RE	 REL Modifying Device ID (RET)
499	0020	3402	RE	 REL Modified Image ID (RET)
500	0020	3403	RE	 REL Modified Image Date (RET)
501	0020	3404	RE	 REL Modifying Device Mfr (RET)
502	0020	3405	RE	 REL Modified Image Time
503	0020	3406	RE	 REL Modified Image Description (RET)
504	0020	4000	LT	REL Image Comments
505	0020	5000	RE	 REL Original Image ID (RET)
506	0020	5002	RE	 REL Orig Image ID Nomenclature (RET)
507	0028	0000	UL	IMG Group Length
508	0028	0002	US	IMG Samples Per Pixel
509	0028	0004	CS	IMG Photometric Interpretation
510	0028	0005	RE	 IMG Image Dimensions (RET)
511	0028	0006	US	IMG Planar Configuration
512	0028	0008	IS	IMG Number of Frames
513	0028	0009	AT	IMG Frame Increment Pointer
514	0028	0010	US	IMG Rows
515	0028	0011	US	IMG Columns
516	0028	0012	US	IMG Planes
517	0028	0014	US	IMG Ultrasound Color Data Present
518	0028	0030	DS	IMG Pixel Spacing
519	0028	0031	DS	IMG Zoom Factor
520	0028	0032	DS	IMG Zoom Center
521	0028	0034	IS	IMG Pixel Aspect Ratio
522	0028	0040	RE	 IMG Image Format (RET)
523	0028	0050	RE	 IMG Manipulated Image (RET)
524	0028	0051	CS	IMG Corrected Image
525	0028	0060	RE	 IMG Compression Code
526	0028	0100	US	IMG Bits Allocated
527	0028	0101	US	IMG Bits Stored
528	0028	0102	US	IMG High Bit
529	0028	0103	US	IMG Pixel Representation
530	0028	0104	RE	 IMG Smallest Pixel Value (RET)
531	0028	0105	RE	 IMG Largest Pixel Vaue (RET)
532	0028	0106	CT	 IMG Smallest Image Pixel Value
533	0028	0107	CT	 IMG Largest Image Pixel Value
534	0028	0108	CT	 IMG Smallest Pixel Value in Series
535	0028	0109	CT	 IMG Largest Pixel Value in Series
536	0028	0110	CT	 IMG Smallest Pixel Value in Plane
537	0028	0111	CT	 IMG Largest Pixel Value in Plane
538	0028	0120	CT	 IMG Pixel Padding Value
539	0028	0122	CT	 IMG Waveform Padding Value
540	0028	0200	RE	 IMG Image Location
541	0028	0300	CS	IMG Quality Control Image
542	0028	0301	CS	IMG Burned In Annotation
543	0028	1040	CS	IMG Pixel Intensity Relationship
544	0028	1041	SS	IMG Pixel Intensity Relationship Sign
545	0028	1050	DS	IMG Window Center
546	0028	1051	DS	IMG Window Width
547	0028	1052	DS	IMG Rescale Intercept
548	0028	1053	DS	IMG Rescale Slope
549	0028	1054	LO	IMG Rescale Type
550	0028	1055	LO	IMG Window Center & Width Explanation
551	0028	1080	RE	 IMG Gray Scale (RET)
552	0028	1090	CS	IMG Recommended Viewing Mode
553	0028	1100	RE	 IMG Lookup Table Desc-Gray (RET)
554	0028	1101	US	IMG Lookup Table Desc-Red
555	0028	1102	US	IMG Lookup Table Desc-Green
556	0028	1103	US	IMG Lookup Table Desc-Blue
557	0028	1199	UI	IMG Palette Color Lookup Table UID
558	0028	1200	RE	 IMG Lookup Data-Gray
559	0028	1201	CT	 IMG Lookup Data-Red
560	0028	1202	CT	 IMG Lookup Data-Green
561	0028	1203	CT	 IMG Lookup Data-Blue
562	0028	1221	OW	IMG Segmented Red Palette Color LUT Data
563	0028	1222	OW	IMG Segmented Green Palette Color LUT Data
564	0028	1223	OW	IMG Segmented Blue Palette Color LUT Data
565	0028	2110	CS	IMG Lossy Image Compression
566	0028	3000	SQ	IMG Modality LUT Sequence
567	0028	3002	CT	 IMG LUT Descriptor
568	0028	3003	LO	IMG LUT Explanation
569	0028	3004	LO	IMG Modality LUT Type
570	0028	3006	CT	 IMG LUT Data
571	0028	3010	SQ	IMG VOI LUT Sequence
572	0028	4000	RE	 IMG Comments (RET)
573	0028	5000	SQ	IMG Bi-Plane Acquisition Sequence
574	0028	6010	US	IMG Representative Frame Number
575	0028	6020	US	IMG Frame Numbers of Interest
576	0028	6022	LO	IMG Frame of Interest Description
577	0028	6030	US	IMG Mask Pointer(s)
578	0028	6040	US	IMG R Wave Pointer
579	0028	6100	SQ	IMG Mask Subtraction Sequence
580	0028	6101	CS	IMG Mask Operation
581	0028	6102	US	IMG Applicable Frame Range
582	0028	6110	US	IMG Mask Frame Numbers
583	0028	6112	US	IMG Contrast Frame Averaging
584	0028	6114	FL	IMG Mask Sub-pixel shift
585	0028	6120	SS	IMG TID Offset
586	0028	6190	ST	IMG Mask Operation Explanation
587	0032	0000	UL	SDY Study Group length
588	0032	000a	CS	SDY Study Status ID
589	0032	000c	CS	SDY Study Priority ID
590	0032	0012	LO	SDY Study ID Issuer
591	0032	0032	DA	SDY Study Verified Date
592	0032	0033	TM	SDY Study Verified Time
593	0032	0034	DA	SDY Study Read Date
594	0032	0035	TM	SDY Study Read Time
595	0032	1000	DA	SDY Scheduled Study Start Date
596	0032	1001	TM	SDY Scheduled Study Start Time
597	0032	1010	DA	SDY Scheduled Study Stop Date
598	0032	1011	TM	SDY Scheduled Study Stop Time
599	0032	1020	LO	SDY Scheduled Study Location
600	0032	1021	AE	SDY Scheduled Study Location AE Title(s)
601	0032	1030	LO	SDY Study Reason
602	0032	1032	PN	SDY Requesting Physician
603	0032	1033	LO	SDY Requesting Service
604	0032	1040	DA	SDY Study Arrival Date
605	0032	1041	TM	SDY Study Arrival Time
606	0032	1050	DA	SDY Study Completion Date
607	0032	1051	TM	SDY Study Completion Time
608	0032	1055	CS	SDY Study Component Status ID
609	0032	1060	LO	SDY Requested Procedure Description
610	0032	1064	SQ	SDY Requested Procedure Code Seq
611	0032	1070	LO	SDY Requested Contrast Agent
612	0032	4000	LT	SDY Comments
613	0038	0000	UL	VIS Group Length
614	0038	0004	SQ	VIS Referenced Patient Alias Sequence
615	0038	0008	CS	VIS Visit Status ID
616	0038	0010	LO	VIS Admission ID
617	0038	0011	LO	VIS Issuer of Admission ID
618	0038	0016	LO	VIS Route of Admission
619	0038	001a	DA	VIS Scheduled Admission Date
620	0038	001b	TM	VIS Scheduled Admission Time
621	0038	001c	DA	VIS Scheduled Discharge Date
622	0038	001d	TM	VIS Scheduled Discharge Time
623	0038	001e	LO	VIS Scheduled Patient Institution Residence
624	0038	0020	DA	VIS Admitting Date
625	0038	0021	TM	VIS Admitting Time
626	0038	0030	DA	VIS Discharge Date
627	0038	0032	TM	VIS Discharge Time
628	0038	0040	LO	VIS Discharge Diagnosis Description
629	0038	0044	SQ	VIS Discharge Diagnosis Code Sequence
630	0038	0050	LO	VIS Special Needs
631	0038	0300	LO	VIS Current Patient Location
632	0038	0400	LO	VIS Patient's Institution Residence
633	0038	0500	LO	VIS Patient State
634	0038	4000	LT	VIS Comments
635	003a	0000	UL	WAV Group Length
636	003a	0002	SQ	WAV Waveform Sequence
637	003a	0005	US	WAV Number of Channels
638	003a	0010	UL	WAV Number of Samples
639	003a	001a	DS	WAV Sampling Frequency
640	003a	0020	SH	WAV Group Label
641	003a	0103	CS	WAV Data Value Representation
642	003a	0200	SQ	WAV Channel Definition
643	003a	0202	IS	WAV Channel Number
644	003a	0203	SH	WAV Channel Label
645	003a	0205	CS	WAV Channel Status
646	003a	0208	SQ	WAV Waveform Source
647	003a	0209	SQ	WAV Waveform Source Modifiers
648	003a	020a	SQ	WAV Differential Waveform Source
649	003a	020b	SQ	WAV Differential Waveform Source Modifiers
650	003a	0210	DS	WAV Channel Sensitivity
651	003a	0211	SQ	WAV Channel Sensitivity Units
652	003a	0212	DS	WAV Channel Sensitivity Correction Factor
653	003a	0213	DS	WAV Channel Baseline
654	003a	0214	DS	WAV Channel Time Skew
655	003a	0215	DS	WAV Channel Sample Skew
656	003a	0218	DS	WAV Channel Offset
657	003a	021a	US	WAV Bits Per Sample
658	003a	0216	CT	 WAV Channel Minimum Value
659	003a	0217	CT	 WAV Channel Maximum Value
660	003a	0220	DS	WAV Filter Low Frequency
661	003a	0221	DS	WAV Filter High Frequency
662	003a	0222	DS	WAV Notch Filter Frequency
663	003a	0223	DS	WAV Notch Filter Bandwidth
664	003a	1000	CT	 WAV Waveform Data
665	0040	0000	UL	PRC Group Length
666	0040	0001	AE	PRC Scheduled Station AE Title
667	0040	0002	DA	PRC Scheduled Procedure Step Start Date
668	0040	0003	TM	PRC Scheduled Procedure Step Start Time
669	0040	0004	DA	PRC Scheduled Procedure Step End Date
670	0040	0005	TM	PRC Scheduled Procedure Step End Time
671	0040	0006	PN	PRC Scheduled Performing Physician's Name
672	0040	0007	LO	PRC Scheduled Step Description
673	0040	0008	SQ	PRC Scheduled Action Item Code Sequence
674	0040	0009	SH	PRC Scheduled Procedure Step ID
675	0040	0010	SH	PRC Scheduled Station Name
676	0040	0011	SH	PRC Scheduled Procedure Step Location
677	0040	0012	LO	PRC Pre-Medication
678	0040	0020	CS	PRC SPStep Status
679	0040	0220	SQ	PRC Ref Standalone SOP Inst Seq
680	0040	0241	AE	PRC Performed Station AE Title
681	0040	0242	SH	PRC Performed Station Name
682	0040	0243	SH	PRC Performed Location
683	0040	0244	DA	PRC PPS Start Date
684	0040	0245	TM	PRC PPS Start Time
685	0040	0250	DA	PRC PPS End Date
686	0040	0251	TM	PRC PPS End Time
687	0040	0252	CS	PRC PPS Status
688	0040	0253	CS	PRC PPS ID
689	0040	0254	LO	PRC PPS Description
690	0040	0255	LO	PRC Perf Procedure Type Description
691	0040	0260	SQ	PRC Perf AI Sequence
692	0040	0270	SQ	PRC Scheduled Step Attr Seq
693	0040	0275	SQ	PRC Request Attributes Seq
694	0040	0280	ST	PRC Comments on PPS
695	0040	0293	SQ	PRC Quantity Sequence
696	0040	0294	DS	PRC Quantity
697	0040	0295	SQ	PRC Measuring Units Sequence
698	0040	0296	SQ	PRC Billing Item Seq
699	0040	0300	US	PRC Total Time Fluoroscopy
700	0040	0301	US	PRC Total Number Exposures
701	0040	0302	US	PRC Entrance Dose
702	0040	0303	US	PRC Exposed Area
703	0040	0306	DS	PRC Distance Source to Entrance
704	0040	0310	ST	PRC Comments on Radiation Dose
705	0040	0320	SQ	PRC Billing Proc Step Seq
706	0040	0321	SQ	PRC Film Consumption Seq
707	0040	0324	SQ	PRC Billing Supplies/Devices Seq
708	0040	0330	SQ	PRC Ref Procedure Step Seq
709	0040	0340	SQ	PRC Performed Series Seq
710	0040	0100	SQ	PRC Scheduled Procedure Step Sequence
711	0040	0400	LT	PRC Comments on the Scheduled Procedure Step
712	0040	050a	LO	PRC Specimen Accession Number
713	0040	0550	SQ	PRC Specimen Sequence
714	0040	0551	LO	PRC Specimen Identifier
715	0040	0552	SQ	PRC Specimen Description Sequence
716	0040	0553	ST	PRC Specimen Description
717	0040	0555	SQ	PRC Acquisition Context Sequence
718	0040	0556	ST	PRC Acquisition Context Description
719	0040	059a	SQ	PRC Specimen Type Code Sequence
720	0040	06fa	LO	PRC Slide Identifier
721	0040	071a	SQ	PRC Image Center Point Coordinates Sequence
722	0040	072a	DS	PRC X offset in Slide Coordinate System
723	0040	073a	DS	PRC Y offset in Slide Coordinate System
724	0040	074a	DS	PRC Z offset in Slide Coordinate System
725	0040	08d8	SQ	PRC Pixel Spacing Sequence
726	0040	08da	SQ	PRC Coordinate System Axis Code Sequence
727	0040	08ea	SQ	PRC Measurement Units Code Sequence
728	0040	09f8	SQ	PRC Vital Stain Code Sequence
729	0040	1001	SH	PRC Requested Procedure ID
730	0040	1002	LO	PRC Reason for the Requested Procedure
731	0040	1003	SH	PRC Patient Transport Arrangements
732	0040	1004	LO	PRC Patient Transport Arrangements
733	0040	1005	LO	PRC Requested Procedure Location
734	0040	1006	SH	PRC Placer Order Number / Procedure
735	0040	1007	SH	PRC Filler Order Number / Procedure
736	0040	1008	LO	PRC Confidentiality Code
737	0040	1009	SH	PRC  Reporting Priority
738	0040	1010	PN	PRC Names of Intended Recipients of Results
739	0040	1400	LT	PRC Requested Procedure Comments
740	0040	2001	LO	PRC Reason for teh Imaging Service Request
741	0040	2004	DA	PRC Issue Date of Imaging Service Request
742	0040	2005	TM	PRC Issue Time of Imaging Service Request
743	0040	2006	SH	PRC Placer Order Number/Imaging Service Request
744	0040	2007	SH	PRC Filler Order Number/Imaging Service Request
745	0040	2008	PN	PRC Order Entered By
746	0040	2009	SH	PRC Order Enterer's Location
747	0040	2010	SH	PRC Order Callback Phone Number
748	0040	2016	LO	PRC Placer Order Number/ISR
749	0040	2017	LO	PRC Filler Order Number/ISR
750	0040	2400	LT	PRC Imaging Service Request Comments
751	0040	3001	LO	PRC Confidientiality Constraint Patient Data...
752	0040	a010	CS	PRC Relationship Type
753	0040	a027	LO	PRC Verifying Organization
754	0040	a030	DT	PRC Verification DateTime
755	0040	a032	DT	PRC Observation DateTime
756	0040	a040	CS	PRC Value Type
757	0040	a043	SQ	PRC Concept-name Code Sequence
758	0040	a050	CS	PRC Continuity of Content
759	0040	a073	SQ	PRC Verifying Observer Sequence
760	0040	a075	PN	PRC Verifying Observer Name
761	0040	a088	SQ	PRC Verifying Observer Identification Code Seq
762	0040	a0a0	CS	PRC Referenced Type of Data
763	0040	a0b0	US	PRC Referenced Waveform Channels
764	0040	a120	DT	PRC Date Time
765	0040	a121	DA	PRC Date
766	0040	a122	TM	PRC Time
767	0040	a123	PN	PRC Person Name
768	0040	a124	UI	PRC UID
769	0040	a130	CS	PRC Temporal Range Type
770	0040	a132	UL	PRC Referenced Sample Offsets
771	0040	a138	DS	PRC Referenced Time Offsets
772	0040	a13a	DT	PRC Referenced Datetime
773	0040	a160	UT	PRC Text Value
774	0040	a168	SQ	PRC Concept Code Sequence
775	0040	a16a	ST	PRC Bibliographics Citation
776	0040	a180	US	PRC Annotation Group Number
777	0040	a195	SQ	PRC Concept-name Code Sequence Modifier
778	0040	a300	SQ	PRC Measured Value Sequence
779	0040	a30a	DS	PRC Numeric Value
780	0040	a353	ST	PRC Address
781	0040	a354	LO	PRC Telephone Number
782	0040	a360	SQ	PRC Predecessor Documents Sequence
783	0040	a370	SQ	PRC Referenced Request Sequence
784	0040	a372	SQ	PRC Performed Procedure Code Sequence
785	0040	a375	SQ	PRC Current Reqeusted Procedure Evidence Seq
786	0040	a385	SQ	PRC Pertinent Other Evidence Sequence
787	0040	a491	CS	PRC Completion Flag
788	0040	a492	LO	PRC Completion Flag Description
789	0040	a493	CS	PRC Verification Flag
790	0040	a504	SQ	PRC Content Template Sequence
791	0040	a525	SQ	PRC Identical Documents Sequence
792	0040	a730	SQ	PRC Content Sequence
793	0040	a992	ST	PRC Uniform Resource Locator
794	0040	b020	SQ	PRC Annotation Sequence
795	0040	db00	CS	PRC Template Identifier
796	0040	db06	DT	PRC Template Version
797	0040	db07	DT	PRC Template Local Version
798	0040	db0b	CS	PRC Template Extension Flag
799	0040	db0c	UI	PRC Template Extension Organization UID
800	0040	db0d	UI	PRC Template Extension Creator UID
801	0040	db73	UL	PRC Referenced Content Item Identifier
802	0050	0004	CS	DEV Calibration Object
803	0050	0010	SQ	DEV Device Sequence
804	0050	0014	DS	DEV Device Length
805	0050	0016	DS	DEV Device Diameter
806	0050	0017	CS	DEV Device Diameter Units
807	0050	0018	DS	DEV Device Volume
808	0050	0019	DS	DEV Inter-Marker Distance
809	0050	0020	LO	DEV Device Description
810	0054	0000	UL	NMI Group Length
811	0054	0010	US	NMI Energy Window Vector
812	0054	0011	US	NMI Number of Energy Windows
813	0054	0012	SQ	NMI Energy Window Information Sequence
814	0054	0013	SQ	NMI Energy Window Range Sequence
815	0054	0014	DS	NMI Energy Window Lower Limit
816	0054	0015	DS	NMI Energy Window Upper Limit
817	0054	0016	SQ	NMI Radiopharmaceutical Information Sequence
818	0054	0017	IS	NMI Residual Syringe Counts
819	0054	0018	SH	NMI Energy Window Name
820	0054	0020	US	NMI Detector Vector
821	0054	0021	US	NMI Number of Detectors
822	0054	0022	SQ	NMI Detector Information Sequence
823	0054	0030	US	NMI Phase Vector
824	0054	0031	US	NMI Number of Phases
825	0054	0032	SQ	NMI Phase Information Sequence
826	0054	0033	US	NMI Number of Frames in Phase
827	0054	0036	IS	NMI Phase Delay
828	0054	0038	IS	NMI Pause between Frames
829	0054	0050	US	NMI Rotation Vector
830	0054	0051	US	NMI Number of rotations
831	0054	0052	SQ	NMI Rotation Information Sequence
832	0054	0053	US	NMI Number of frames in rotation
833	0054	0060	US	NMI R-R Interval Vector
834	0054	0061	US	NMI Number of R-R Intervals
835	0054	0062	SQ	NMI Gated Information Sequence
836	0054	0063	SQ	NMI Data Information Sequence
837	0054	0070	US	NMI Time Slot Vector
838	0054	0071	US	NMI Number of Time Slots
839	0054	0072	SQ	NMI Time Slot Information Sequence
840	0054	0073	DS	NMI Time Slot Time
841	0054	0080	US	NMI Slice Vector
842	0054	0081	US	NMI Number of Slices
843	0054	0090	US	NMI Angular View Vector
844	0054	0100	US	NMI Time Slice Vector
845	0054	0101	US	NMI Number of Time Slices
846	0054	0200	DS	NMI Start Angle
847	0054	0202	CS	NMI Type of Detector Motion
848	0054	0210	IS	NMI Trigger Vector
849	0054	0211	US	NMI Number of Triggers in Phase
850	0054	0220	SQ	NMI View Code Sequence
851	0054	0222	SQ	NMI View Angulation Modifer Code Sequence
852	0054	0300	SQ	NMI Radionuclide Code Sequence
853	0054	0302	SQ	NMI Radiopharmaceutical Route Code Sequence
854	0054	0304	SQ	NMI Radiopahrmaceutical Code Sequence
855	0054	0306	SQ	NMI Calibration Data Sequence
856	0054	0308	US	NMI Energy Window Number
857	0054	0400	SH	NMI Image ID
858	0054	0410	SQ	NMI Patient Orientation Code Sequence
859	0054	0412	SQ	NMI Patient Orientation Modifier Code Sequence
860	0054	0414	SQ	NMI Patient Gantry Relationship Code Sequence
861	0054	1000	CS	NMI Series Type
862	0054	1001	CS	NMI Units
863	0054	1002	CS	NMI Counts Source
864	0054	1004	CS	NMI Reprojection Method
865	0054	1100	CS	NMI Randoms Correction Method
866	0054	1101	LO	NMI Attenuation Correction Method
867	0054	1102	CS	NMI Decay Correction
868	0054	1103	LO	NMI Reconstruction Method
869	0054	1104	LO	NMI Detector Lines of Response Used
870	0054	1105	LO	NMI Scatter Correction Method
871	0054	1200	DS	NMI Axial Acceptance
872	0054	1201	IS	NMI Axial Mash
873	0054	1202	IS	NMI Transverse Mash
874	0054	1203	DS	NMI Detector Element Size
875	0054	1210	DS	NMI Coincidence Window Width
876	0054	1220	CS	NMI Secondary Counts Type
877	0054	1300	DS	NMI Frame Reference Time
878	0054	1310	IS	NMI Primary (Prompts) Counts Accumulated
879	0054	1311	IS	NMI Secondary Counts Accumulated
880	0054	1320	DS	NMI Slice Sensitivity Factor
881	0054	1321	DS	NMI Decay Factor
882	0054	1322	DS	NMI Dose Calibration Factor
883	0054	1323	DS	NMI Scatter Fraction Factor
884	0054	1324	DS	NMI Dead Time Factor
885	0054	1330	US	NMI Image Index
886	0054	1400	CS	NMI Counts Included
887	0054	1401	CS	NMI Dead Time Correction Flag
888	0070	0000	UL	GRP Group Length
889	0070	0022	FL	GRP Graphic Data
890	0070	0023	CS	GRP Graphic Type
891	0070	0024	CS	GRP Graphic Filled
892	0070	0041	CS	GRP Image Horizontal Flip
893	0070	0042	US	GRP Image Rotation
894	0070	0052	SL	GRP Displayed Area Top LH Corner
895	0070	0053	SL	GRP Displayed Area Bottom RH Corner
896	0070	005a	SQ	GRP Display Area Selection Seq
897	0070	0060	SQ	GRP Graphic Layer Sequence
898	0070	0062	IS	GRP Graphic Layer Order
899	0070	0066	US	GRP Graphic Layer Rec Disp GS Val
900	0070	0067	US	GRP Graphic Layer Rec Disp RGB Val
901	0070	0068	LO	GRP Graphic Layer Description
902	0070	0080	CS	GRP Presentation Label
903	0070	0081	LO	GRP Presentation Description
904	0070	0082	DA	GRP Presentation Creation Date
905	0070	0083	TM	GRP Presentation Creation Time
906	0070	0084	PN	GRP Presentation Creators Name
907	0070	0100	CS	GRP Presentation Size Mode
908	0070	0101	DS	GRP Presentation Pixel Spacing
909	0070	0102	IS	GRP Presentation Pixel Aspect Ratio
910	0070	0103	FL	GRP Presentation Pixel Magnification Ratio
911	0088	0000	UL	MED Media Group Length
912	0088	0130	SH	MED Storage Media File-set ID
913	0088	0140	UI	MED Storage Media File-setUID
914	0088	0200	SQ	MED Icon Image Sequence
915	0088	0904	LO	MED Topic Title
916	0088	0906	ST	MED Topic Subject
917	0088	0910	LO	MED Topic Author
918	0088	0912	LO	MED Topic Keywords
919	2000	0000	UL	BFS Group Length
920	2000	0010	IS	BFS Number of copies printed for each film
921	2000	0020	CS	BFS Specifies priority of print job
922	2000	0030	CS	BFS Medium on which page will be printed
923	2000	0040	CS	BFS Film destination
924	2000	0050	LO	BFS Human readable label to identify film
925	2000	0060	IS	BFS Amount of mem allocated for film session
926	2000	0500	SQ	BFS seq of UIDs of diff FILMBOX instances
927	2010	0000	UL	BFB Group Length
928	2010	0010	ST	BFB Type of image display format
929	2010	0030	CS	BFB Id of annotation display format
930	2010	0040	CS	BFB Film orientation
931	2010	0050	CS	BFB Film size identification
932	2010	0060	CS	BFB Interpol. type by which printer mag image
933	2010	0080	CS	BFB Specifies type of interpolation function
934	2010	0100	CS	BFB density of film areas around/between images
935	2010	0110	CS	BFB density of image box area having no image
936	2010	0120	US	BFB Minimum density of images on the film
937	2010	0130	US	BFB Maximum density of images on the film
938	2010	0140	CS	BFB specifies whether to trim or not
939	2010	0150	ST	BFB ID of configuration table
940	2010	0500	SQ	BFB seq. of film session instance
941	2010	0510	SQ	BFB seq. of basic image box SOP instance
942	2010	0520	SQ	BFB seq. of basic annotation box SOP instance
943	2020	0000	UL	BIB Group Length
944	2020	0010	US	BIB Specifies position of the image in the film
945	2020	0020	CS	BIB Specifies image polarity
946	2020	0030	DS	BIB Requested image size
947	2020	0110	SQ	BIB Preformatted Greyscale image
948	2020	0111	SQ	BIB Preformatted Color image
949	2020	0130	SQ	BIB Referenced Image Overlay Box seq
950	2020	0140	SQ	BIB Referenced VOI LUT seq.
951	2030	0000	UL	BAB Group Length
952	2030	0010	US	BAB posn of the annot. box in parent film box
953	2030	0020	LO	BAB text string
954	2040	0000	UL	IOB Group Length
955	2040	0010	SQ	IOB Ref Overlay Plane Sequence
956	2040	0011	US	IOB Ref Overlay Plane Groups
957	2040	0060	CS	IOB Overlay Magnification Type
958	2040	0070	CS	IOB Overlay Smoothing Type
959	2040	0080	CS	IOB Overlay Foreground Density
960	2040	0090	CS	IOB Overlay Mode
961	2040	0100	CS	IOB Threshold Density
962	2040	0500	SQ	IOB Ref Image Box Sequence (RET)
963	2100	0000	UL	PJ Group Length
964	2100	0020	CS	PJ execution status of print job
965	2100	0030	CS	PJ additional information
966	2100	0040	DA	PJ date of print job creation
967	2100	0050	TM	PJ time of print job creation
968	2100	0070	AE	PJ Appln entity title that issued the print opn
969	2100	0500	SQ	PJ Referenced print job seq.
970	2110	0000	UL	PRINTER Group Length
971	2110	0010	CS	PRINTER printer device status
972	2110	0020	CS	PRINTER additional information
973	2110	0030	LO	PRINTER printer name
974	2110	0099	SH	Printer Queue ID
975	3002	0000	UL	RT Group Length
976	3002	0002	SH	RT Image Label
977	3002	0003	LO	RT Image Name
978	3002	0004	ST	RT Image Description
979	3002	000a	CS	RT Reported Values Origin
980	3002	000c	CS	RT Image Plane
981	3002	000e	DS	RT X-Ray Image Receptor Angle
982	3002	0010	DS	RT Image Orientation
983	3002	0011	DS	RT Image Plane Pixel Spacing
984	3002	0012	DS	RT Image Position
985	3002	0020	SH	RT Radiation Machine Name
986	3002	0022	DS	RT Radiation Machine SAD
987	3002	0024	DS	RT Radiation Machine SSD
988	3002	0026	DS	RT Image SID
989	3002	0028	DS	RT Source to Reference Object Distance
990	3002	0029	IS	RT Fraction Number
991	3002	0030	SQ	RT Exposure Sequence
992	3002	0032	DS	RT Meterset Exposure
993	3004	0000	UL	DVH Group Length
994	3004	0001	CS	DVH Type
995	3004	0002	CS	DVH Dose Units
996	3004	0004	CS	DVH Dose Type
997	3004	0006	LO	DVH Dose Comment
998	3004	0008	DS	DVH Normalization Point
999	3004	000a	CS	DVH Dose Summation Type
1000	3004	000c	DS	DVH Grid Frame Offset Vector
1001	3004	000e	DS	DVH Dose Grid Scaling
1002	3004	0010	SQ	DVH RT Dose ROI Sequence
1003	3004	0012	DS	DVH Dose Value
1004	3004	0040	DS	DVH Normalization Point
1005	3004	0042	DS	DVH Normalization Dose Value
1006	3004	0050	SQ	DVH Sequence
1007	3004	0052	DS	DVH Dose Scaling
1008	3004	0054	CS	DVH Volume Units
1009	3004	0056	IS	DVH Number of Bins
1010	3004	0058	DS	DVH Data
1011	3004	0060	SQ	DVH Referenced ROI Sequence
1012	3004	0062	CS	DVH ROI Contribution Type
1013	3004	0070	DS	DVH Minimum Dose
1014	3004	0072	DS	DVH Maximum Dose
1015	3004	0074	DS	DVH Mean Dose
1016	3006	0000	UL	SSET Group Length
1017	3006	0002	SH	SSET Structure Set Label
1018	3006	0004	LO	SSET Structure Set Name
1019	3006	0006	ST	SSET Structure Set Description
1020	3006	0008	DA	SSET Structure Set Date
1021	3006	0009	TM	SSET Structure Set Time
1022	3006	0010	SQ	SSET Referenced Frame of Reference Sequence
1023	3006	0012	SQ	SSET RT Referenced Study Sequence
1024	3006	0014	SQ	SSET RT Referenced Series Sequence
1025	3006	0016	SQ	SSET Contour Image Sequence
1026	3006	0020	SQ	SSET Structure Set ROI Sequence
1027	3006	0022	IS	SSET ROI Number
1028	3006	0024	UI	SSET Referenced Frame of Reference UID
1029	3006	0026	LO	SSET ROI Name
1030	3006	0028	ST	SSET ROI Description
1031	3006	002a	IS	SSET ROI Display Color
1032	3006	002c	DS	SSET ROI Volume
1033	3006	0030	SQ	SSET RT Related ROI Sequence
1034	3006	0033	CS	SSET RT ROI Relationship
1035	3006	0036	CS	SSET ROI Generation Algorithm
1036	3006	0038	LO	SSET ROI Generation Description
1037	3006	0039	SQ	SSET ROI Contour Sequence
1038	3006	0040	SQ	SSET Contour Sequence
1039	3006	0042	CS	SSET Contour Geometric Type
1040	3006	0044	DS	SSET Contour Slab Thickness
1041	3006	0045	DS	SSET Contour Offset Vector
1042	3006	0046	IS	SSET Number of Contour Points
1043	3006	0050	DS	SSET Contour Data
1044	3006	0080	SQ	SSET RT ROI Observations Sequence
1045	3006	0082	IS	SSET Observation Number
1046	3006	0084	IS	SSET Referenced ROI Number
1047	3006	0085	SH	SSET ROI Observation Label
1048	3006	0086	SQ	SSET RT ROI Identification Code Sequence
1049	3006	0088	ST	SSET ROI Observation Description
1050	3006	00a0	SQ	SSET Relation RT ROI Observations Sequence
1051	3006	00a4	CS	SSET RT ROI Interpreted Type
1052	3006	00a6	PN	SSET ROI Interpreter
1053	3006	00b0	SQ	SSET ROI Physical Properties Sequence
1054	3006	00b2	CS	SSET ROI Physical Property
1055	3006	00b4	DS	SSET ROI Physical Property Value
1056	3006	00c0	SQ	SSET Frame of Referenced Relationship Sequence
1057	3006	00c2	UI	SSET Related Frame of Reference UID
1058	3006	00c4	CS	SSET Frame of Reference Transformation Type
1059	3006	00c6	DS	SSET Frame of Reference Transformation Matrix
1060	3006	00c8	LO	SSET Frame of Reference Transformation Comment
1061	300a	0000	UL	     Group Length
1062	300a	0002	SH	     RT Plan Label
1063	300a	0003	LO	     RT Plan Name
1064	300a	0004	ST	     RT Plan Description
1065	300a	0006	DA	     RT Plan Date
1066	300a	0007	TM	     RT Plan Time
1067	300a	0009	LO	     RT Treatment Protocols
1068	300a	000a	CS	     Treatment Intent
1069	300a	000b	LO	     Treatment Sites
1070	300a	000c	CS	     RT Plan Geometry
1071	300a	000e	ST	     Prescription Description
1072	300a	0010	SQ	     Dose Reference Sequence
1073	300a	0012	IS	     Dose Reference Number
1074	300a	0014	CS	     Dose Reference Structure Type
1075	300a	0016	LO	     Dose Reference Description
1076	300a	0018	DS	     Dose Reference Point Coordinates
1077	300a	001a	DS	     Nominal Prior Dose
1078	300a	0020	CS	     Dose Reference Type
1079	300a	0021	DS	     Constraint Weight
1080	300a	0022	DS	     Delivery Warning Dose
1081	300a	0023	DS	     Delivery Maximum Dose
1082	300a	0025	DS	     Target Minimum Dose
1083	300a	0026	DS	     Target Prescription Dose
1084	300a	0027	DS	     Target Maximum Dose
1085	300a	0028	DS	     Target Underdose Volume Fraction
1086	300a	002a	DS	     Organ at Risk Full-volume Dose
1087	300a	002b	DS	     Organ at Risk Limit Dose
1088	300a	002c	DS	     Organ at Risk Maximum Dose
1089	300a	002d	DS	     Organ at Risk Overdose Volume Fraction
1090	300a	0040	SQ	     Tolerance Table Sequence
1091	300a	0042	IS	     Tolerance Table Number
1092	300a	0043	SH	     Tolerance Table Label
1093	300a	0044	DS	     Gantry Angle Tolerance
1094	300a	0046	DS	     Beam Limiting Device Angle Tolerance
1095	300a	0048	SQ	     Beam Limiting Device Tolerance Sequence
1096	300a	004a	DS	     Beam Limiting Device Position Tolerance
1097	300a	004c	DS	     Patient Support Angle Tolerance
1098	300a	004e	DS	     Table Top Eccentric Angle Tolerance
1099	300a	0051	DS	     Table Top Vertical Position Tolerance
1100	300a	0052	DS	     Table Top Longitudinal Position Tolerance
1101	300a	0053	DS	     Table Top Lateral Position Tolerance
1102	300a	0055	CS	     RT Plan Relationship
1103	300a	0070	SQ	     Fraction Group Sequence
1104	300a	0071	IS	     Fraction Group Number
1105	300a	0078	IS	     Number of Fractions Planned
1106	300a	0079	IS	     Number of Fractions Per Day
1107	300a	007a	IS	     Repeat Fraction Cycle Length
1108	300a	007b	LT	     Fraction Pattern
1109	300a	0080	IS	     Number of Beams
1110	300a	0082	DS	     Beam Dose Specification Point
1111	300a	0084	DS	     Beam Dose
1112	300a	0086	DS	     Beam Meterset
1113	300a	00a0	IS	     Number of Brachy Application Setups
1114	300a	00a2	DS	     Brachy App Setup Dose Specification Point
1115	300a	00a4	DS	     Brachy Application Setup Dose
1116	300a	00b0	SQ	     Beam Sequence
1117	300a	00b2	SH	     Treatment Machine Name
1118	300a	00b3	CS	     Primary Dosimeter Unit
1119	300a	00b4	DS	     Source-Axis Distance
1120	300a	00b6	SQ	     Beam Limiting Device Sequence
1121	300a	00b8	CS	     RT Beam Limiting Device Type
1122	300a	00ba	DS	     Source to Beam Limiting Device Distance
1123	300a	00bc	IS	     Number of Leaf/Jaw Pairs
1124	300a	00be	DS	     Leaf Position Boundaries
1125	300a	00c0	IS	     Beam Number
1126	300a	00c2	LO	     Beam Name
1127	300a	00c3	ST	     Beam Description
1128	300a	00c4	CS	     Beam Type
1129	300a	00c6	CS	     Radiation Type
1130	300a	00c8	IS	     Reference Image Number
1131	300a	00ca	SQ	     Planned Verification Image Sequence
1132	300a	00cc	LO	     Imaging Device-Specific Acq Parameters
1133	300a	00ce	CS	     Treatment Delivery Type
1134	300a	00d0	IS	     Number of Wedges
1135	300a	00d1	SQ	     Wedge Sequence
1136	300a	00d2	IS	     Wedge Number
1137	300a	00d3	CS	     Wedge Type
1138	300a	00d4	SH	     Wedge ID
1139	300a	00d5	IS	     Wedge Angle
1140	300a	00d6	DS	     Wedge Factor
1141	300a	00d8	DS	     Wedge Orientation
1142	300a	00da	DS	     Source to Wedge Tray Distance
1143	300a	00e0	IS	     Number of Compensators
1144	300a	00e1	SH	     Material ID
1145	300a	00e2	DS	     Total Compensator Tray Factor
1146	300a	00e3	SQ	     Compensator Sequence
1147	300a	00e4	IS	     Compensator Number
1148	300a	00e5	SH	     Compensator ID
1149	300a	00e6	DS	     Source to Compensator Tray Distance
1150	300a	00e7	IS	     Compensator Rows
1151	300a	00e8	IS	     Compensator Columns
1152	300a	00e9	DS	     Compensator Pixel Spacing
1153	300a	00ea	DS	     Compensator Position
1154	300a	00eb	DS	     Compensator Transmission Data
1155	300a	00ec	DS	     Compensator Thickness Data
1156	300a	00ed	IS	     Number of Boli
1157	300a	00f0	IS	     Number of Blocks
1158	300a	00f2	DS	     Total Block Tray Factor
1159	300a	00f4	SQ	     Block Sequence
1160	300a	00f5	SH	     Block Tray ID
1161	300a	00f6	DS	     Source to Block Tray Distance
1162	300a	00f8	CS	     Block Type
1163	300a	00fa	CS	     Block Divergence
1164	300a	00fc	IS	     Block Number
1165	300a	00fe	LO	     Block Name
1166	300a	0100	DS	     Block Thickness
1167	300a	0102	DS	     Block Transmission
1168	300a	0104	IS	     Block Number of Points
1169	300a	0106	DS	     Block Data
1170	300a	0107	SQ	     Applicator Sequence
1171	300a	0108	SH	     Applicator ID
1172	300a	0109	CS	     Applicator Type
1173	300a	010a	LO	     Applicator Description
1174	300a	010c	DS	     Cumulative Dose Reference COefficient
1175	300a	010e	DS	     Final Cumulative Meterset Weight
1176	300a	0110	IS	     Number of Control Points
1177	300a	0111	SQ	     Control Point Sequence
1178	300a	0112	IS	     Control Point Index
1179	300a	0114	DS	     Nominal Beam Energy
1180	300a	0115	DS	     Dose Rate Set
1181	300a	0116	SQ	     Wedge Position Sequence
1182	300a	0118	CS	     Wedge Position
1183	300a	011a	SQ	     Beam Limiting Device Position Sequence
1184	300a	011c	DS	     Leaf/Jaw Positions
1185	300a	011e	DS	     Gantry Angle
1186	300a	011f	CS	     Gantry Rotation Direction
1187	300a	0120	DS	     Beam Limiting Device Angle
1188	300a	0121	CS	     Beam Limiting Device Rotation Direction
1189	300a	0122	DS	     Patient Support Angle
1190	300a	0123	CS	     Patient Support Rotation Direction
1191	300a	0124	DS	     Table Top Eccentric Axis Distance
1192	300a	0125	DS	     Table Top Eccentric Angle
1193	300a	0126	CS	     Table Top Eccentric Rotation Direction
1194	300a	0128	DS	     Table Top Vertical Position
1195	300a	0129	DS	     Table Top Longitudinal Position
1196	300a	012a	DS	     Table Top Lateral Position
1197	300a	012c	DS	     Isocenter Position
1198	300a	012e	DS	     Surface Entry Point
1199	300a	0130	DS	     Source to Surface Distance
1200	300a	0134	DS	     Cumulative Meterset Weight
1201	300a	0180	SQ	     Patient Setup Sequence
1202	300a	0182	IS	     Patient Setup Number
1203	300a	0184	LO	     Patient Additional Position
1204	300a	0190	SQ	     Fixation Device Sequence
1205	300a	0192	CS	     Fixation Device Type
1206	300a	0194	SH	     Fixation Device Label
1207	300a	0196	ST	     Fixation Device Description
1208	300a	0198	SH	     Fixation Device Position
1209	300a	01a0	SQ	     Shielding Device Sequence
1210	300a	01a2	CS	     Shielding Device Type
1211	300a	01a4	SH	     Shielding Device Label
1212	300a	01a6	ST	     Shielding Device Description
1213	300a	01a8	SH	     Shielding Device Position
1214	300a	01b0	CS	     Setup Technique
1215	300a	01b2	ST	     Setup Technique Description
1216	300a	01b4	SQ	     Setup Device Sequence
1217	300a	01b6	CS	     Setup Device Type
1218	300a	01b8	SH	     Setup Device Label
1219	300a	01ba	ST	     Setup Device Description
1220	300a	01bc	DS	     Setup Device Parameter
1221	300a	01d0	ST	     Setup Reference Description
1222	300a	01d2	DS	     Table Top Vertical Setup Displacement
1223	300a	01d4	DS	     Table Top Longitudinal Setup Displacement
1224	300a	01d6	DS	     Table Top Lateral Setup Displacement
1225	300a	0200	CS	     Brachy Treatment Technique
1226	300a	0202	CS	     Brachy Treatment Type
1227	300a	0206	SQ	     Treatment Machine Sequence
1228	300a	0210	SQ	     Source Sequence
1229	300a	0212	IS	     Source Number
1230	300a	0214	CS	     Source Type
1231	300a	0216	LO	     Source Manufacturer
1232	300a	0218	DS	     Active Source Diameter
1233	300a	021a	DS	     Active Source Length
1234	300a	0222	DS	     Source Encapsulation Nominal Thickness
1235	300a	0224	DS	     Source Encapsulation Nominal Transmission
1236	300a	0226	LO	     Source Isotope Name
1237	300a	0228	DS	     Source Isotope Half Life
1238	300a	022a	DS	     Reference Air Kerma Rate
1239	300a	022c	DA	     Air Kerma Rate Reference Date
1240	300a	022e	TM	     Air Kerma Rate Reference Time
1241	300a	0230	SQ	     Application Setup Sequence
1242	300a	0232	CS	     Application Setup Type
1243	300a	0234	IS	     Application Setup Number
1244	300a	0236	LO	     Application Setup Name
1245	300a	0238	LO	     Application Setup Manufacturer
1246	300a	0240	IS	     Template Number
1247	300a	0242	SH	     Template Type
1248	300a	0244	LO	     Template Name
1249	300a	0250	DS	     Total Reference Air Kerma
1250	300a	0260	SQ	     Brachy Acessory Device Sequence
1251	300a	0262	IS	     Brachy Accessory Device Number
1252	300a	0263	SH	     Brachy Accessory Device ID
1253	300a	0264	CS	     Brachy Accessory Device Type
1254	300a	0266	LO	     Brachy Accessory Device Name
1255	300a	026a	DS	     Brachy Accessory Device Nominal Thickness
1256	300a	026c	DS	     Brachy Acc'ry Device Nominal Transmission
1257	300a	0280	SQ	     Channel Sequence
1258	300a	0282	IS	     Channel Number
1259	300a	0284	DS	     Channel Length
1260	300a	0286	DS	     Channel Total Time
1261	300a	0288	CS	     Source Movement Type
1262	300a	028a	IS	     Number of Pulses
1263	300a	028c	DS	     Pulse Repetition Interval
1264	300a	0290	IS	     Source Applicator Number
1265	300a	0291	SH	     Source Applicator ID
1266	300a	0292	CS	     Source Applicator Type
1267	300a	0294	LO	     Source Applicator Name
1268	300a	0296	DS	     Source Applicator Length
1269	300a	0298	LO	     Source Applicator Manufacturer
1270	300a	029c	DS	     Source Applicator Wall Nominal Thickness
1271	300a	029e	DS	     Src Applicator Wall Nominal Transmission
1272	300a	02a0	DS	     Source Applicator Step Size
1273	300a	02a2	IS	     Transfer Tube Number
1274	300a	02a4	DS	     Transfer Tube Length
1275	300a	02b0	SQ	     Channel Shield Sequence
1276	300a	02b2	IS	     Channel Shield Number
1277	300a	02b3	SH	     Channel Shield ID
1278	300a	02b4	LO	     Channel Shield Name
1279	300a	02b8	DS	     Channel Shield Nominal Thickness
1280	300a	02ba	DS	     Channel Shield Nominal Transmission
1281	300a	02c8	DS	     Final Cumulative Time Weight
1282	300a	02d0	SQ	     Brachy Control Point Sequence
1283	300a	02d2	DS	   Control Point Relative Position
1284	300a	02d4	DS	     Control Point 3D Position
1285	300a	02d6	DS	     Cumulative Time Weight
1286	300c	0000	UL	     Group Length
1287	300c	0002	SQ	     Referenced RT Plan Sequence
1288	300c	0004	SQ	     Referenced Beam Sequence
1289	300c	0006	IS	     Referenced Beam Number
1290	300c	0007	IS	     Referenced Reference Image Number
1291	300c	0008	DS	     Start Cumulative Meterset Weight
1292	300c	0009	DS	     End Cumulative Meterset Weight
1293	300c	000a	SQ	     Referenced Brachy Application Setup Seq
1294	300c	000c	IS	     Referenced Brachy Application Setup Number
1295	300c	000e	IS	     Referenced Source Number
1296	300c	0020	SQ	     Referenced Fraction Group Sequence
1297	300c	0022	IS	     Referenced Fraction Group Number
1298	300c	0040	SQ	     Referenced Verification Image Sequence
1299	300c	0042	SQ	     Referenced Reference Image Sequence
1300	300c	0050	SQ	     Referenced Dose Reference Sequence
1301	300c	0051	IS	     Referenced Dose Reference Numer
1302	300c	0055	SQ	     Brachy Referenced Dose Reference Sequence
1303	300c	0060	SQ	     Referenced Structure Set Sequence
1304	300c	006a	IS	     Referenced Patient Setup Number
1305	300c	0080	SQ	     Referenced Dose Sequence
1306	300c	00a0	IS	     Referenced Tolerance Table Number
1307	300c	00b0	SQ	     Referenced Bolus Sequence
1308	300c	00c0	IS	     Referenced Wedge Number
1309	300c	00d0	IS	     Referenced Compensator Number
1310	300c	00e0	IS	     Referenced Block Number
1311	300c	00f0	IS	     Referenced Control Point Index
1312	300e	0000	UL	     Group Length
1313	300e	0002	CS	     Approval Status
1314	300e	0004	DA	     Review Date
1315	300e	0005	TM	     Review Time
1316	300e	0008	PN	     Reviewer Name
1317	4008	0000	UL	RES Group Length
1318	4008	0040	SH	RES Results ID
1319	4008	0042	LO	RES Results ID Issuer
1320	4008	0050	SQ	RES Referenced Interpretation Sequence
1321	4008	0100	DA	RES Interpretation Recorded Date
1322	4008	0101	TM	RES Interpretation Recorded Time
1323	4008	0102	PN	RES Interpretation Recorder
1324	4008	0103	LO	RES Reference to Recorded Sound
1325	4008	0108	DA	RES Interpretation Transcription Date
1326	4008	0109	TM	RES Interpretation Transcription Time
1327	4008	010a	PN	RES Interpretation Transcriber
1328	4008	010b	ST	RES Interpretation Text
1329	4008	010c	PN	RES Interpretation Author
1330	4008	0111	SQ	RES Interpretation Approver Sequence
1331	4008	0112	DA	RES Interpretation Approval Date
1332	4008	0113	TM	RES Interpretation Approval Time
1333	4008	0114	PN	RES Physician Approving Interpretation
1334	4008	0115	LT	RES Diagnosis
1335	4008	0117	SQ	RES Diagnosis Code Sequence
1336	4008	0118	SQ	RES Results Distribution List Sequence
1337	4008	0119	PN	RES Distribution Name
1338	4008	011a	LO	RES Distribution Address
1339	4008	0200	SH	RES Interpretation ID
1340	4008	0202	LO	RES Interpretation ID Issuer
1341	4008	0210	CS	RES Interpretation Type ID
1342	4008	0212	CS	RES Interpretation Status ID
1343	4008	0300	ST	RES Impressions
1344	4008	4000	ST	RES Comments
1345	5000	0000	UL	CRV Group Length
1346	5000	0005	US	CRV Curve Dimensions
1347	5000	0010	US	CRV Number of points
1348	5000	0020	CS	CRV Type of Data
1349	5000	0022	LO	CRV Curve Description
1350	5000	0030	SH	CRV Axis Units
1351	5000	0040	SH	CRV Axis Labels
1352	5000	0103	US	CRV Data Value Representation
1353	5000	0104	US	CRV Minimum Coordinate Value
1354	5000	0105	US	CRV Maximum Coordinate Value
1355	5000	0106	SH	CRV Curve Range
1356	5000	0110	US	CRV Data Descriptor
1357	5000	0112	US	CRV Coordinate Start Value
1358	5000	0114	US	CRV Coordinate Step Value
1359	5000	2000	US	CRV Audio Type
1360	5000	2002	US	CRV Audio Sample Format
1361	5000	2004	US	CRV Number of Channels
1362	5000	2006	UL	CRV Number of Samples
1363	5000	2008	UL	CRV Sample Rate
1364	5000	200a	UL	CRV Total Time
1365	5000	200c	OW	CRV Audio Sample Data
1366	5000	200e	LT	CRV Audio Comments
1367	5000	2500	LO	CRV Curve Label
1368	5000	2600	SQ	CRV Referenced Overlay Sequence
1369	5000	2610	US	CRV Referenced Overlay Group
1370	5000	3000	OW	CRV Curve Data
1371	6000	0000	UL	OLY Group Length
1372	6000	0010	US	OLY Rows
1373	6000	0011	US	OLY Columns
1374	6000	0012	US	OLY Planes
1375	6000	0015	IS	OLY Number of frames in Overlay
1376	6000	0022	LO	OLY Overlay Description
1377	6000	0040	CS	OLY Type
1378	6000	0045	LO	OLY Subtype
1379	6000	0050	SS	OLY Origin
1380	6000	0051	US	OLY Image Frame Origin
1381	6000	0052	US	OLY Overlay Plane Origin
1382	6000	0060	LO	OLY Compression Code (RET)
1383	6000	0100	US	OLY Overlay Bits Allocated
1384	6000	0102	US	OLY Overlay Bit Position
1385	6000	0110	LO	OLY Overlay Format (RET)
1386	6000	0200	US	OLY Overlay Location (RET)
1387	6000	1100	US	OLY Overlay Descriptor - Gray
1388	6000	1101	US	OLY Overlay Descriptor - Red
1389	6000	1102	US	OLY Overlay Descriptor - Green
1390	6000	1103	US	OLY Overlay Descriptor - Blue
1391	6000	1200	US	OLY Overlays - Gray
1392	6000	1201	US	OLY Overlays - Red
1393	6000	1202	US	OLY Overlays - Green
1394	6000	1203	US	OLY Overlays - Blue
1395	6000	1301	IS	OLY ROI Area
1396	6000	1302	DS	OLY ROI Mean
1397	6000	1303	DS	OLY ROI Standard Deviation
1398	6000	1500	LO	OLY Overlay Label
1399	6000	3000	OW	OLY Data
1400	6000	4000	LO	OLY Comments (RET)
1401	7fe0	0000	UL	PXL Group Length
1402	7fe0	0010	OT	PXL Pixel Data
\.


--
-- TOC entry 38 (OID 17650)
-- Name: pk_pat; Type: CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_patientlevel
    ADD CONSTRAINT pk_pat PRIMARY KEY (paci_id);


--
-- TOC entry 39 (OID 17660)
-- Name: pk_stu; Type: CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_studylevel
    ADD CONSTRAINT pk_stu PRIMARY KEY (stuinsuid);


--
-- TOC entry 40 (OID 17689)
-- Name: pk_img_laudo; Type: CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_laudo
    ADD CONSTRAINT pk_img_laudo PRIMARY KEY (stuinsuid);

--
-- TOC entry 41 (OID 17693)
-- Name: pk41052317977426_img_tipo_pacs; Type: CONSTRAINT; Schema: public; Owner: posgres1
--

ALTER TABLE ONLY img_tipo_pacs
    ADD CONSTRAINT pk41052317977426_img_tipo_pacs PRIMARY KEY (tp_mod);


--
-- TOC entry 42 (OID 25793)
-- Name: pk_serinsuid; Type: CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_serieslevel
    ADD CONSTRAINT pk_serinsuid PRIMARY KEY (serinsuid);


--
-- TOC entry 43 (OID 25804)
-- Name: pk_sopinsuid; Type: CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_imagelevel
    ADD CONSTRAINT pk_sopinsuid PRIMARY KEY (sopinsuid);


--
-- TOC entry 44 (OID 25815)
-- Name: pk_nm_seq; Type: CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_configuracoes
    ADD CONSTRAINT pk_nm_seq PRIMARY KEY (cfg_nome, cfg_sequencia);


--
-- TOC entry 48 (OID 25839)
-- Name: pk_arc_id; Type: CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_area_cliente
    ADD CONSTRAINT pk_arc_id PRIMARY KEY (arc_id);


--
-- TOC entry 49 (OID 25847)
-- Name: pk_tps_id; Type: CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_tipo_servidor
    ADD CONSTRAINT pk_tps_id PRIMARY KEY (tps_id);


--
-- TOC entry 50 (OID 25855)
-- Name: pk_mat_id; Type: CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_map_type
    ADD CONSTRAINT pk_mat_id PRIMARY KEY (mat_id);


--
-- TOC entry 51 (OID 25864)
-- Name: pk_map_id; Type: CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_map_tables
    ADD CONSTRAINT pk_map_id PRIMARY KEY (map_id);


--
-- TOC entry 52 (OID 25868)
-- Name: pk_dic_id; Type: CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_dicom_dict
    ADD CONSTRAINT pk_dic_id PRIMARY KEY (dic_id);


--
-- TOC entry 53 (OID 25886)
-- Name: pk_mit_id; Type: CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_midia_type
    ADD CONSTRAINT pk_mit_id PRIMARY KEY (mit_type_id);


--
-- TOC entry 54 (OID 25893)
-- Name: pk_mid_id; Type: CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_midias
    ADD CONSTRAINT pk_mid_id PRIMARY KEY (mid_id);


--
-- TOC entry 55 (OID 25904)
-- Name: img_mid_x_stu_pkey; Type: CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_mid_x_stu
    ADD CONSTRAINT img_mid_x_stu_pkey PRIMARY KEY (mxs_id);


--
-- TOC entry 45 (OID 25921)
-- Name: img_ae_cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_ae_cliente
    ADD CONSTRAINT img_ae_cliente_pkey PRIMARY KEY (aec_id);


--
-- TOC entry 46 (OID 25923)
-- Name: img_ae_servidor_pkey; Type: CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_ae_servidor
    ADD CONSTRAINT img_ae_servidor_pkey PRIMARY KEY (aes_id);


--
-- TOC entry 47 (OID 25927)
-- Name: px_aec_aes; Type: CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_aec_x_aes
    ADD CONSTRAINT px_aec_aes PRIMARY KEY (axa_aes_id, axa_aec_id);


--
-- TOC entry 74 (OID 17662)
-- Name: stu_pat_fk; Type: FK CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_studylevel
    ADD CONSTRAINT stu_pat_fk FOREIGN KEY (paci_id) REFERENCES img_patientlevel(paci_id);


--
-- TOC entry 75 (OID 25795)
-- Name: fk_stuinsuid; Type: FK CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_serieslevel
    ADD CONSTRAINT fk_stuinsuid FOREIGN KEY (stuinsuid) REFERENCES img_studylevel(stuinsuid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 76 (OID 25806)
-- Name: fk_serinsuid; Type: FK CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_imagelevel
    ADD CONSTRAINT fk_serinsuid FOREIGN KEY (serinsuid) REFERENCES img_serieslevel(serinsuid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 77 (OID 25841)
-- Name: fk_arc_id; Type: FK CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_ae_cliente
    ADD CONSTRAINT fk_arc_id FOREIGN KEY (aec_arc_id) REFERENCES img_area_cliente(arc_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 78 (OID 25849)
-- Name: fk_TPS_id; Type: FK CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_ae_servidor
    ADD CONSTRAINT "fk_TPS_id" FOREIGN KEY (aes_tipo_servidor) REFERENCES img_tipo_servidor(tps_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 81 (OID 25898)
-- Name: fk_midia_type_id; Type: FK CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_midias
    ADD CONSTRAINT fk_midia_type_id FOREIGN KEY (mid_type_id) REFERENCES img_midia_type(mit_type_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 82 (OID 25909)
-- Name: fk_stuinsuid; Type: FK CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_mid_x_stu
    ADD CONSTRAINT fk_stuinsuid FOREIGN KEY (mxs_stuinsuid) REFERENCES img_studylevel(stuinsuid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 83 (OID 25913)
-- Name: fk_mid_id; Type: FK CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_mid_x_stu
    ADD CONSTRAINT fk_mid_id FOREIGN KEY (mxs_mid_id) REFERENCES img_midias(mid_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 79 (OID 25929)
-- Name: fk_aes_id; Type: FK CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_aec_x_aes
    ADD CONSTRAINT fk_aes_id FOREIGN KEY (axa_aes_id) REFERENCES img_ae_servidor(aes_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 80 (OID 25933)
-- Name: fk_aec_id; Type: FK CONSTRAINT; Schema: public; Owner: posgres
--

ALTER TABLE ONLY img_aec_x_aes
    ADD CONSTRAINT fk_aec_id FOREIGN KEY (axa_aec_id) REFERENCES img_ae_cliente(aec_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 5 (OID 25857)
-- Name: sq_img_map_tables; Type: SEQUENCE SET; Schema: public; Owner: posgres
--

SELECT pg_catalog.setval('sq_img_map_tables', 1, false);


--
-- TOC entry 8 (OID 25870)
-- Name: sq_img_dicom_dict; Type: SEQUENCE SET; Schema: public; Owner: posgres
--

SELECT pg_catalog.setval('sq_img_dicom_dict', 1402, true);


--
-- TOC entry 10 (OID 25888)
-- Name: sq_img_midia_type; Type: SEQUENCE SET; Schema: public; Owner: posgres
--

SELECT pg_catalog.setval('sq_img_midia_type', 1, false);


--
-- TOC entry 12 (OID 25895)
-- Name: sq_img_midias; Type: SEQUENCE SET; Schema: public; Owner: posgres
--

SELECT pg_catalog.setval('sq_img_midias', 1, false);


--
-- TOC entry 14 (OID 25906)
-- Name: sq_img_mid_x_stu; Type: SEQUENCE SET; Schema: public; Owner: posgres
--

SELECT pg_catalog.setval('sq_img_mid_x_stu', 1, false);


--
-- TOC entry 16 (OID 25917)
-- Name: sq_img_ae_cliente; Type: SEQUENCE SET; Schema: public; Owner: posgres
--

SELECT pg_catalog.setval('sq_img_ae_cliente', 2, true);


--
-- TOC entry 18 (OID 25919)
-- Name: sq_img_ae_servidor; Type: SEQUENCE SET; Schema: public; Owner: posgres
--

SELECT pg_catalog.setval('sq_img_ae_servidor', 1, true);

--
-- TOC entry 2 (OID 2200)
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'Standard public schema';

--
-- TOC entry 6 (OID 25857)
-- Name: SEQUENCE sq_img_map_tables; Type: COMMENT; Schema: public; Owner: posgres
--

COMMENT ON SEQUENCE sq_img_map_tables IS 'autonumeracao';


