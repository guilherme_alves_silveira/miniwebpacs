<jsp:useBean id="LoginBeanId" scope="session" class="prontuariobsb.LoginBean" />
<%
  String erro = "";
  try{
    LoginBeanId.process(pageContext);
  }
  catch(Exception e){
    erro = e.getMessage();
  }
%>

<html>
  <body>
<table width="670" border="0" align="center">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><img src="../pacsbarrapurpura670.gif" width="670" height="29"></td>
  </tr>
</table>
<table width="670" border="0" align="center" bgcolor="#d1e1e4">
  <tr> 
    <td width="718"><div align="center"><font color="#660033" face="Arial, Helvetica, sans-serif"></font></div></td>
  </tr>
  <tr> 
    <td><div align="center"><font color="#660033" face="Arial, Helvetica, sans-serif"><strong>P&aacute;gina 
        de Login</strong></font></div></td>
  </tr>
  <tr> 
    <td> <form name="form1" action="Login.jsp" method="POST">
        <table align="center" bgcolor="#8db1ba">
          <TR> 
            <td colspan="4">&#xA0;</td>
          </TR>
          <TR> 
            <td width="20">&#xA0;</td>
            <td><font face="Arial, Helvetica, sans-serif" size="2" color="#000000"><strong>Usu�rio: 
              </strong></font></td>
            <td><input TYPE="TEXT" NAME="userID" VALUE="" MAXLENGTH="50"/></td>
            <td width="20">&#xA0;</td>
          </TR>
          <TR> 
            <td>&#xA0;</td>
            <td><font face="Arial, Helvetica, sans-serif" size="2" color="#000000"><strong>Senha: 
              </strong></font></td>
            <td><input TYPE="PASSWORD" NAME="password" VALUE="" MAXLENGTH="50"/></td>
            <td>&#xA0;</td>
          </TR>
          <TR> 
            <td>&#xA0;</td>
            <TD> </TD>
            <TD> <table width="100%">
                <tr> 
                  <td align="left"> <input type="submit" name="Submit" value="Enviar"/> 
                  </td>
                  <td align="right"> <input type="reset" value="Limpar"/> </td>
                </tr>
              </table></TD>
            <td>&#xA0;</td>
          </TR>
          <tr> 
            <td colspan="4">&#xA0;<b><%=erro%></b></td>
          </tr>
        </table>
      </form></td>
  </tr>
  <tr> 
    <td><pre><font color="#666666" size="1" face="Arial, Helvetica, sans-serif">         O<strong><font color="#660033"> </font></strong></font><font color="#666666" face="Arial, Helvetica, sans-serif"><strong><font color="#660033" size="1">miniWeb-PACS</font></strong></font><font color="#666666" size="1" face="Arial, Helvetica, sans-serif"> &eacute; um software livre distribuido pela licen&ccedil;a <a href="http://www.gnu.org/copyleft/gpl.html">GNU General Public License</a>. Utilizando-o voc&ecirc; estar&aacute; automaticamente 
         concordando com os termos da licen&ccedil;a.

        <font color="#660033"><strong>AUS&Ecirc;NCIA DE GARANTIAS</strong></font>
        - Uma vez que o programa &eacute; licenciado sem &ocirc;nus, n&atilde;o h&aacute; qualquer garantia para o programa, na extens&atilde;o permitida  pelas  leis  apli- 
          c&aacute;veis. Exceto quando expresso de forma escrita, os detentores dos direitos autorais e/ou terceiros disponibilizam o  programa  &quot;da  
          maneira como  est&aacute;&quot;, sem qualquer tipo de garantias, expressas ou impl&iacute;citas, incluindo, mas n&atilde;o limitado a, as garantias impl&iacute;citas 
          de comercializa&ccedil;&atilde;o e  as de adequa&ccedil;&atilde;o a qualquer prop&oacute;sito. O risco total com a qualidade e desempenho do programa &eacute; seu. Se 
          o programa se mostrar defeituoso, voc&ecirc; assume os custos de todas as manuten&ccedil;&otilde;es, reparos e corre&ccedil;&otilde;es.

       - Em nenhuma ocasi&atilde;o, a menos que exigido pelas leis aplic&aacute;veis ou acordo escrito, os detentores dos direitos autorais, ou qualquer 
         outra parte que possa modificar e/ou redistribuir o programa conforme permitido acima, ser&atilde;o responsabilizados por voc&ecirc; por danos, 
         incluindo qualquer dano em geral, especial, acidental   ou conseq&uuml;ente,  resultantes  do uso ou  incapacidade de uso do programa
         (incluindo, mas n&atilde;o limitado a, a perda de dados ou dados tornados incorretos, ou perdas sofridas  por  voc&ecirc; ou  por outras partes, 
         da possibilidade ou falhas do programa ao operar com qualquer outro programa), mesmo que tal detentor ou parte tenham sido avi-
         sados de tais danos. <br>          </font></pre></td>
  </tr>
  <tr>
    <td><div align="right">
        <pre><font color="#666666" size="1" face="Arial, Helvetica, sans-serif">mini-WEBPACS | Vers&atilde;o 1.0/2004</font></pre>
      </div></td>
  </tr>
</table>


    
  </body>
 </html>