<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<html>
<head>
<title><bean:message key="page.title"/></title>
<link rel=stylesheet href=../style.css type=text/css> 
</head>
<body>
<table align="center" style=text-align:center;>
<tr><td>

<table style=width:640;>
<tr><td style=font-size:10px;>
<bean:message key="page.title"/>
</td></tr>
<tr>
<td class=tableHeader>
<bean:message key="errors.page"/>
</td></tr>
</table>

<% if (request.getAttribute("javax.servlet.error.status_code")!=null) { %>
<table border=0 style=width:640;margin-top:10;><tr><td style=font-size:12px;>

<table border=1 style=width:640;margin-top:10;>
<tr><td >Error:</td>
<td style="text-align:center">
<%= request.getAttribute("javax.servlet.error.status_code")%>
</td></tr>
<% if (request.getAttribute("javax.servlet.error.message")!=null) { %>
<tr><td >Message:</td>
<td style="text-align:center">
<%= request.getAttribute("javax.servlet.error.message")%>
</td></tr>
<% } %>
<% if (request.getAttribute("javax.servlet.error.servlet_name")!=null) { %>
<tr><td >Servlet Name:</td>
<td style="text-align:center">
<%= request.getAttribute("javax.servlet.error.servlet_name")%>
</td></tr>
<% } %>
<% if (request.getAttribute("javax.servlet.error.exception_type")!=null) { %>
<tr><td >Exception Type:</td>
<td style="text-align:center">
<%= request.getAttribute("javax.servlet.error.exception_type")%>
</td></tr>
<% } %>
<% if (request.getAttribute("javax.servlet.error.exception")!=null) { %>
<tr><td >Exception:</td>
<td style="text-align:center">
<%= request.getAttribute("javax.servlet.error.exception")%>
</td></tr>
<% } %>
</table>
</td></tr></table>
<% } %>

<% String exception = (String)session.getAttribute("exception");
   if (exception!=null) {
      %>
<table style=width:640;margin-top:10;><tr><td style=font-size:12px;>
      <%= exception %>
</td></tr></table>
      <%
   }
%>

<table style=width:640;>
<tr>
<td style=border-color:black;border-top-style:solid;border-top-width:1;padding-top:5;font-size:11px;text-align:center;>
<a href=javascript:history.go(-1)><bean:message key="jsp.back"/></a>
</td></tr>
</table>
</body>	
</html>