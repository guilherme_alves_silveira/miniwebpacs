<jsp:useBean id="LoginBeanId" scope="session" class="prontuariobsb.LoginBean" />
<%
  if (!LoginBeanId.isValid(pageContext)) {
    pageContext.forward("../Login.jsp");
    return;
  }
%>

<%@ include file="../system/HeaderStrutsForm.jsp" %>
<html>
  <head>
    <title>
      <bean:message key="page.title"/>
    </title>
    <link rel=stylesheet href=..\css\crayon.css type=text/css>
    <bean:message key="page.script"/>
  </head>
  <center>
    <body>
<table width="640" border="0">
  <tr>
    <td>������������������������������                         </td>
    <td><img src="../pacsbarrapurpura700.gif" width="700" height="29"></td>
    <td> </td>
  </tr>
</table>
      <table class="mainTable" style='width:640;'>
        <tr>
          <td>
            <table class="headerTable">
              <tr>
                <td class='td10'>
                  Usu�rio:<%=LoginBeanId.getUserName()%>
                </td>
                <td>
                  <div class='div10'>
                    <%= com.egen.util.text.FormatDate.getTimestamp() %>
                    <img 
                        style='cursor:hand'
                        src='../img/ballhelp.png'
                        border='0'
                        onclick="javascript:window.open('../lov/HelpForm.jsp?help=<p><strong>P�GINA DE PESQUISA</p></strong>Esta � a p�gina principal do mini-WebPacs.S�o poss�veis quatro modalidades de busca, que foram agrupadas duas a duas nas abas denominadas <strong>Busca Nome</strong> e <strong>Busca Exame.</strong><p><strong>Aba Busca Nome</p></strong>Esta aba cont�m os formul�rios de busca por <strong>Nome do Paciente</strong> e <strong>Matr�cula.</strong><br><strong>Como fazer uma pesquisa?</br></strong>Se desejar fazer a pesquisa por<strong> Nome do Paciente</strong>, digite o nome no espa�o correspondente, conforme est� exemplificado na pr�pria p�gina. Clique no bot�o Pesquisar.Caso deseje fazer a busca pela <strong>Matr�cula do Paciente</strong>, digite o n�mero no campo matr�cula e clique no bot�o Pesquisar.<br><strong>Resultado Busca por Nome ou Matr�cula do paciente.</br></strong> O <strong>resultado</strong> da consulta � mostrado na mesma p�gina, logo abaixo dos formul�rios de busca. Os dados s�o organizados em forma de tabela e para acess�-los, clique no nome do paciente procurado.<p><strong>Aba Busca Exame</p></strong>Esta aba cont�m os formul�rios de <strong>Busca por N� do Exame</strong> e <strong>Busca por Modalidade</strong>.<br><strong>Como fazer uma pesquisa?</br></strong>Primeiramente selecione a aba Busca Exame que encontra-se na parte superior da p�gina, na cor p�rpura. Em seguida, escolha a op��o de busca desejada.<br><strong>Se desejar pesquisar por n� do exame</strong>, voc� utilizar� o 1� formul�rio. Digite o n� do exame e selecione a modalidade, clicando sobre o nome desejado. Clique em Pesquisar.<br><strong>Se desejar pesquisar por modalidade</strong>, voc� utilizar� o 2� formul�rio. Selecione o per�odo(n� de dias) desejado e selecione a modalidade. Clique em Pesquisar.<br>O <strong>resultado</strong> � apresentado abaixo dos formul�rios de busca. Acesse os dados clicando sobre o nome do paciente, na coluna paciente.<p><strong>LINKS DA P�GINA</p></strong><strong>Ajuda</strong>. Link de acesso � documenta��o visual completa do mini-WebPacs.','def','scrollbars,height=400,width=340,TOP=0,LEFT=0')">

                  </div>
                </td>
              </tr>
            </table>
            <br>
            <div class="MEGmain">
              <table id="tabmenuMain" class="tab" style='width:640;'>
                <thead>
                  <tr id="toprowMain">
                    <td class="tab" nowrap id="Maintab0">
                      <div onclick="switchOn('Maintab0','contentsMaintab0','toprowMain','contentscellMain');">
                        �Busca por Nome
                      </div>
                    </td>
                    <td class="tab" nowrap id="Maintab1">
                      <div onclick="switchOn('Maintab1','contentsMaintab1','toprowMain','contentscellMain');">
                        �Busca por Exame
                      </div>
                    </td>
                    <td class="tabspacer">
                      �
                    </td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class='contentscell' id="contentscellMain" colspan="100">
                      <div class="contents"  id="contentsMaintab0">
                        <BR>
                        <html:form action="system/Mini_WebPacsForm.do" method="POST" scope="session" style=" background-color: #EAF0F2; border-style: groove; " styleClass="baseForm" target="_self">
                          <table class="bannerTable" style='width:640;'>
                            <tr>
                              <td class=tableHeader>
                                Busca por Nome do Paciente
                              </td>
                            </tr>
                          </table>
                          <table class="messageTable" style='width:640;'>
                            <tr>
                              <td class="messageHeader">
                              </td>
                            </tr>
                          </table>
                          <table class="itemTable" style='width:640;'>
                            <tr>
                              <td class="formLabel">
                              </td>
                              <td class="formField">
                                <fieldset class="fieldsetField">
                                  <legend class="legendField">
                                    <span class="spamFormLabel" style="  color:#000000;font-weight:bold; ">
                                      Paciente
                                    </span>
                                  </legend>
                                  <html:text property="patnam" styleClass="baseField" size="64" maxlength="64"/>
                                </fieldset>
                              </td>
                            </tr>
                            <html:hidden property="pos"/>
                          </table>
                          <table class="buttonTable" style='width:640;'>
                            <tr>
                              <td class="buttonItem">
                                <html:submit accesskey="s" value="Pesquisar" styleClass="baseButton" property="select_action">
                                </html:submit>
                                <html:submit value="Pos" styleClass="myhidden" property="pos_action">
                                </html:submit>
                                <html:submit accesskey="r" value="Limpar" styleClass="baseButton" property="resetfull_action">
                                </html:submit>
                              </td>
                            </tr>
                          </table>
                          <table id="Table.Obs.Table" class="obsTable" style='width:640;'>
                            <tr id="Tr.Obs.Table" >
                              <td id="Td.Obs.Table" class=observacao>
                                <font color="##696969">
                                  <strong>
                                    ATEN��O: Exemplo de Busca por nome do Paciente: Jose Pereira da Silva ou Jose%
                                    <strong>
                                    </font>
                                  </td>
                                </tr>
                              </table>
                              <br>
                            </html:form>

                            <BR>
                            <html:form action="system/Mini_WebPacsForm.do" method="POST" scope="session" style="border-style: groove; " styleClass="baseForm" target="_self">
                              <table class="bannerTable" style='width:640;'>
                                <tr>
                                  <td class=tableHeader>
                                    Busca por Matr�cula
                                  </td>
                                </tr>
                              </table>
                              <table class="messageTable" style='width:640;'>
                                <tr>
                                  <td class="messageHeader">
                                  </td>
                                </tr>
                              </table>
                              <table class="itemTable" style='width:640;'>
                                <tr>
                                  <td class="formLabel">
                                  </td>
                                  <td class="formField">
                                    <fieldset class="fieldsetField">
                                      <legend class="legendField">
                                        <span class="spamFormLabel" style="  color:#000000;font-weight:bold; ">
                                          Matr�cula
                                        </span>
                                      </legend>
                                      <html:text property="paci_id" styleClass="baseField" size="12" maxlength="64"/>
                                    </fieldset>
                                  </td>
                                </tr>
                                <html:hidden property="pos1"/>
                              </table>
                              <table class="buttonTable" style='width:640;'>
                                <tr>
                                  <td class="buttonItem">
                                    <html:submit accesskey="s" value="Pesquisar" styleClass="baseButton" property="select1_action">
                                    </html:submit>
                                    <html:submit value="Pos" styleClass="myhidden" property="pos1_action">
                                    </html:submit>
                                    <html:submit accesskey="r" value="Limpar" styleClass="baseButton" property="resetfull1_action">
                                    </html:submit>
                                  </td>
                                </tr>
                              </table>
                            </html:form>

                            <%
                            {
                              %>
                              <BR>
                              <form class="baseForm" >
                                <table class="bannerTable" style=width:640;>
                                  <tr>
                                    <td class="tableHeader">
                                      Resultado Busca por Nome ou Matr�cula do Paciente
                                    </td>
                                  </tr>
                                </table>
                                <table class="messageTable" style=width:640;>
                                  <tr>
                                    <td class=columHeader>
                                      <div style=font-size:11px;font-weight:normal;>
                                        <html:errors/>
                                      </div>
                                    </td>
                                  </tr>
                                </table>
                                <table id="TRbl_report_Img_patientlevel" class="reportTable" style=width:640;   frame=below>
                                  <%
                                  int counterbl_report_Img_patientlevel=0;
                                  %>
                                  <%
                                  {
                                    com.egen.mini_WEBPACS.dbobj.table.Img_patientlevel t_img_patientlevel = new com.egen.mini_WEBPACS.dbobj.table.Img_patientlevel();
                                    java.util.Vector res_Img_patientlevel = (java.util.Vector)session.getAttribute("res_Img_patientlevel");
                                    if (res_Img_patientlevel!=null && res_Img_patientlevel.size()>0) {
                                      if ( ((String)request.getParameter("_ordered"))!=null && ((String)request.getParameter("_ordered")).length()>0 ) {
                                        boolean reverse = false;
                                        if ( ((String)request.getParameter("_reverse"))!=null && ((String)request.getParameter("_reverse")).length()>0 && ((String)request.getParameter("_reverse")).equals("true") ){
                                          reverse = true;
                                        }
                                        try {
                                          res_Img_patientlevel = com.egen.util.text.Sort.execute(res_Img_patientlevel,((String)request.getParameter("_ordered")),reverse);
                                        } catch (Exception e) {}
                                      }
                                      java.lang.String patnam1 =  null;
                                      java.lang.String paci_id1 =  null;
                                      java.sql.Date patbirdat1 =  null;
                                      int i_bl_report_Img_patientlevel = 0;
                                      t_img_patientlevel = (com.egen.mini_WEBPACS.dbobj.table.Img_patientlevel)res_Img_patientlevel.elementAt(i_bl_report_Img_patientlevel);
                                      patnam1 = t_img_patientlevel.getPatnam()==null?"":t_img_patientlevel.getPatnam();
                                      paci_id1 = t_img_patientlevel.getPaci_id()==null?"":t_img_patientlevel.getPaci_id();
                                      patbirdat1 = (java.sql.Date)t_img_patientlevel.getPatbirdat();
                                      %>
                                      <tr class="reportHeader" >
                                        <td >
                                          <span class="spamHeaderLabel" style="text-align:left;width:100%;">
                                            Paciente
                                          </span>
                                        </td>
                                        <td >
                                          <span class="spamHeaderLabel" style="text-align:left;width:100%;">
                                            Matr�cula
                                          </span>
                                        </td>
                                        <td >
                                          <span class="spamHeaderLabel" style="text-align:left;width:100%;">
                                            Nascimento
                                          </span>
                                        </td>
                                      </tr>
                                      <%
                                      while (i_bl_report_Img_patientlevel<res_Img_patientlevel.size()){
                                        counterbl_report_Img_patientlevel++;
                                        String style="";
                                        if (!((i_bl_report_Img_patientlevel%2)!=0)) {
                                          style="class=rowColor";
                                        } else {
                                          style="class=rowBlank";
                                        }
                                        %>
                                        <tr <%= style %> id='TRbl_report_Img_patientlevel<%=counterbl_report_Img_patientlevel%>' >
                                          <td class="reportcolumn"  >
                                            <a href="../system/Lista_Exa_PatForm.jsp?paci_id=<%=paci_id1%>" target="_self" STYLE="cursor:hand" title="Ver lista de exames deste paciente." >
                                              <span class="spamColumnLabel" style="text-align:left; color:#483D8B;font-weight:bold;">
                                                <%= patnam1 %>
                                              </span>
                                            </a>
                                          </td>
                                          <td class="reportcolumn"  >
                                            <span class="spamColumnLabel" style="text-align:left;">
                                              <%= paci_id1 %>
                                            </span>
                                          </td>
                                          <td class="reportcolumn"  >
                                            <span class="spamColumnLabel" style="text-align:left;">
                                              <%= com.egen.util.text.FormatDate.format(patbirdat1, "dd/MM/yyyy") %>
                                            </span>
                                          </td>
                                        </tr>
                                        <%
                                        if (++i_bl_report_Img_patientlevel >= res_Img_patientlevel.size()) {
                                          break;
                                        }
                                        t_img_patientlevel = (com.egen.mini_WEBPACS.dbobj.table.Img_patientlevel)res_Img_patientlevel.elementAt(i_bl_report_Img_patientlevel);
                                        patnam1 = t_img_patientlevel.getPatnam()==null?"":t_img_patientlevel.getPatnam();
                                        paci_id1 = t_img_patientlevel.getPaci_id()==null?"":t_img_patientlevel.getPaci_id();
                                        patbirdat1 = (java.sql.Date)t_img_patientlevel.getPatbirdat();
                                      }
                                    }
                                  }
                                  %>
                                </table>
                              </form>
                              <%
                            }
                            %>
                          </div>
                          <div class="contents"  id="contentsMaintab1">
                            <BR>
                            <html:form action="system/Mini_WebPacsForm.do" method="POST" scope="session" style=" background-color: #EAF0F2;; border-style: groove;" styleClass="baseForm" target="_self">
                              <table class="bannerTable" style='width:640;'>
                                <tr>
                                  <td class=tableHeader>
                                    Busca por N� do Exame
                                  </td>
                                </tr>
                              </table>
                              <table class="messageTable" style='width:640;'>
                                <tr>
                                  <td class="messageHeader">
                                  </td>
                                </tr>
                              </table>
                              <table class="itemTable" style='width:640;'>
                                <tr>
                                  <td class="formLabel">
                                    <span class="spamFormLabel" style=" font-weight:bold; ">
                                      N� do Exame
                                    </span>
                                  </td>
                                  <td class="formField">
                                    <html:text property="stuid" styleClass="baseField" size="16" maxlength="16"/>
                                  </td>
                                </tr>
                                <tr>
                                  <td class="formLabel">
                                    <span class="spamFormLabel" style=" font-weight:bold; ">
                                      Modalidade
                                    </span>
                                  </td>
                                  <td class="formField">
                                    <html:select property="tp_mod" styleClass="baseField" size="5">
                                      <html:options property="tp_modList" labelProperty="tp_modLabelList" styleClass="baseOptions"/>
                                    </html:select>
                                  </td>
                                </tr>
                                <html:hidden property="pos2"/>
                              </table>
                              <table class="buttonTable" style='width:640;'>
                                <tr>
                                  <td class="buttonItem">
                                    <html:submit accesskey="s" value="Pesquisar" styleClass="baseButton" property="select12_action">
                                    </html:submit>
                                    <html:submit value="Pos" styleClass="myhidden" property="pos12_action">
                                    </html:submit>
                                    <html:submit accesskey="r" value="Limpar" styleClass="baseButton" property="resetfull12_action">
                                    </html:submit>
                                  </td>
                                </tr>
                              </table>
                            </html:form>

                            <BR>
                            <html:form action="system/Mini_WebPacsForm.do" method="POST" scope="session" style="border-style: groove;" styleClass="baseForm" target="_self">
                              <table class="bannerTable" style='width:640;'>
                                <tr>
                                  <td class=tableHeader>
                                    Busca por Modalidade
                                  </td>
                                </tr>
                              </table>
                              <table class="messageTable" style='width:640;'>
                                <tr>
                                  <td class="messageHeader">
                                  </td>
                                </tr>
                              </table>
                              <table class="itemTable" style='width:640;'>
                                <tr>
                                  <td class="formLabel"  >
                                    <strong>
                                      Per�odo
                                    </strong>
                                  </td>
                                  <td >
                                    <html:select property="periodo" styleClass="baseField" size="1">
                                      <html:option styleClass="baseOption" value="5">
                                        5 dias
                                      </html:option>
                                      <html:option styleClass="baseOption" value="15">
                                        15 dias
                                      </html:option>
                                      <html:option styleClass="baseOption" value="30">
                                        30 dias
                                      </html:option>
                                    </html:select>
                                    <tr>
                                      <td class="formLabel">
                                        <span class="spamFormLabel" style=" font-weight:bold; ">
                                          Modalidade
                                        </span>
                                      </td>
                                      <td class="formField">
                                        <html:select property="tp_mod1" styleClass="baseField" size="5">
                                          <html:options property="tp_mod1List" labelProperty="tp_mod1LabelList" styleClass="baseOptions"/>
                                        </html:select>
                                      </td>
                                    </tr>
                                    <html:hidden property="pos3"/>
                                  </table>
                                  <table class="buttonTable" style='width:640;'>
                                    <tr>
                                      <td class="buttonItem">
                                        <html:submit accesskey="s" value="Pesquisar" styleClass="baseButton" property="select123_action">
                                        </html:submit>
                                        <html:submit value="Pos" styleClass="myhidden" property="pos123_action">
                                        </html:submit>
                                        <html:submit accesskey="r" value="Limpar" styleClass="baseButton" property="resetfull123_action">
                                        </html:submit>
                                      </td>
                                    </tr>
                                  </table>
                                </html:form>

                                <%
                                {
                                  %>
                                  <BR>
                                  <form class="baseForm" >
                                    <table class="bannerTable" style=width:640;>
                                      <tr>
                                        <td class="tableHeader">
                                          Resultado Busca por N� ou Modalidade do Exame
                                        </td>
                                      </tr>
                                    </table>
                                    <table class="messageTable" style=width:640;>
                                      <tr>
                                        <td class=columHeader>
                                          <div style=font-size:11px;font-weight:normal;>
                                            <html:errors/>
                                          </div>
                                        </td>
                                      </tr>
                                    </table>
                                    <table id="TRbl_report_Img_studylevel" class="reportTable" style=width:640;   frame=border>
                                      <%
                                      int counterbl_report_Img_studylevel=0;
                                      %>
                                      <%
                                      com.egen.util.jdbc.JdbcUtil j = null;
                                      try {
                                        j = new com.egen.util.jdbc.JdbcUtil();
                                        com.egen.mini_WEBPACS.dbobj.table.Img_studylevel t_img_studylevel = new com.egen.mini_WEBPACS.dbobj.table.Img_studylevel();
                                        java.util.Vector res_Img_studylevel = (java.util.Vector)session.getAttribute("res_Img_studylevel");
                                        if (res_Img_studylevel!=null && res_Img_studylevel.size()>0) {
                                          if ( ((String)request.getParameter("_ordered"))!=null && ((String)request.getParameter("_ordered")).length()>0 ) {
                                            boolean reverse = false;
                                            if ( ((String)request.getParameter("_reverse"))!=null && ((String)request.getParameter("_reverse")).length()>0 && ((String)request.getParameter("_reverse")).equals("true") ){
                                              reverse = true;
                                            }
                                            try {
                                              res_Img_studylevel = com.egen.util.text.Sort.execute(res_Img_studylevel,((String)request.getParameter("_ordered")),reverse);
                                            } catch (Exception e) {}
                                          }
                                          java.lang.String patnam =  null;
                                          java.lang.String paci_id =  null;
                                          java.lang.String stuid =  null;
                                          java.lang.String nome =  null;
                                          java.sql.Date studat =  null;
                                          java.lang.String tp_mod =  null;
                                          java.lang.String stuinsuid =  null;
                                          java.lang.String patbirdat =  null;
                                          int i_bl_report_Img_studylevel = 0;
                                          t_img_studylevel = (com.egen.mini_WEBPACS.dbobj.table.Img_studylevel)res_Img_studylevel.elementAt(i_bl_report_Img_studylevel);
                                          paci_id = t_img_studylevel.getPaci_id()==null?"":t_img_studylevel.getPaci_id();
                                          stuid = t_img_studylevel.getStuid()==null?"":t_img_studylevel.getStuid();
                                          studat = (java.sql.Date)t_img_studylevel.getStudat();
                                          tp_mod = t_img_studylevel.getTp_mod()==null?"":t_img_studylevel.getTp_mod();
                                          stuinsuid = t_img_studylevel.getStuinsuid()==null?"":t_img_studylevel.getStuinsuid();
                                          patnam = "";
                                          {
                                            if(paci_id!=null && paci_id.length()>0) {
                                              com.egen.mini_WEBPACS.dbobj.table.Img_patientlevel $cr_db_object = new com.egen.mini_WEBPACS.dbobj.table.Img_patientlevel();
                                              Object[][] where = {
                                              {"paci_id","like",paci_id}
                                              };
                                              java.util.Vector results = j.select($cr_db_object, where, null);
                                              if (results!=null && results.size()>0) {
                                                $cr_db_object = (com.egen.mini_WEBPACS.dbobj.table.Img_patientlevel)results.elementAt(0);
                                                patnam = $cr_db_object.getPatnam() + "";
                                              }
                                            }
                                            };
                                            nome = "";
                                            {
                                              if(tp_mod!=null && tp_mod.length()>0) {
                                                com.egen.mini_WEBPACS.dbobj.table.Img_tipo_pacs $cr_db_object = new com.egen.mini_WEBPACS.dbobj.table.Img_tipo_pacs();
                                                Object[][] where = {
                                                {"tp_mod","like",tp_mod}
                                                };
                                                java.util.Vector results = j.select($cr_db_object, where, null);
                                                if (results!=null && results.size()>0) {
                                                  $cr_db_object = (com.egen.mini_WEBPACS.dbobj.table.Img_tipo_pacs)results.elementAt(0);
                                                  nome = $cr_db_object.getNome() + "";
                                                }
                                              }
                                              };
                                              patbirdat = "";
                                              {
                                                if(paci_id!=null && paci_id.length()>0) {
                                                  com.egen.mini_WEBPACS.dbobj.table.Img_patientlevel $cr_db_object = new com.egen.mini_WEBPACS.dbobj.table.Img_patientlevel();
                                                  Object[][] where = {
                                                  {"paci_id","like",paci_id}
                                                  };
                                                  java.util.Vector results = j.select($cr_db_object, where, null);
                                                  if (results!=null && results.size()>0) {
                                                    $cr_db_object = (com.egen.mini_WEBPACS.dbobj.table.Img_patientlevel)results.elementAt(0);
                                                    patbirdat = $cr_db_object.getPatbirdat() + "";
                                                  }
                                                }
                                                };
                                                %>
                                                <tr class="reportHeader" >
                                                  <td >
                                                    <span class="spamHeaderLabel" style="text-align:left;width:100%;">
                                                      Paciente
                                                    </span>
                                                  </td>
                                                  <td >
                                                    <span class="spamHeaderLabel" style="text-align:left;width:100%;">
                                                      Matr�cula
                                                    </span>
                                                  </td>
                                                  <td >
                                                    <span class="spamHeaderLabel" style="text-align:left;width:100%;">
                                                      N� do Exame
                                                    </span>
                                                  </td>
                                                  <td >
                                                    <span class="spamHeaderLabel" style="text-align:left;width:100%;">
                                                      Modalidade
                                                    </span>
                                                  </td>
                                                  <td >
                                                    <span class="spamHeaderLabel" style="text-align:left;width:100%;">
                                                      Data do Exame
                                                    </span>
                                                  </td>
                                                </tr>
                                                <%
                                                while (i_bl_report_Img_studylevel<res_Img_studylevel.size()){
                                                  counterbl_report_Img_studylevel++;
                                                  String style="";
                                                  if (!((i_bl_report_Img_studylevel%2)!=0)) {
                                                    style="class=rowColor";
                                                  } else {
                                                    style="class=rowBlank";
                                                  }
                                                  %>
                                                  <tr <%= style %> id='TRbl_report_Img_studylevel<%=counterbl_report_Img_studylevel%>' >
                                                    <td class="reportcolumn"  >
                                                      <a 
                                                          href="../system/LaudoForm.do?select_action=&paci_id=<%= paci_id %>&studat=<%= com.egen.util.text.FormatDate.format(studat) %>&stuinsuid=<%= stuinsuid %>&patbirdat=<%= patbirdat %>"
                                                          target="_self"
                                                          STYLE="cursor:hand"
                                                          title="Ver Imagem e/ou laudo."
                                                      >

                                                        <span class="spamColumnLabel" style="text-align:left; color:#483D8B;font-weight:bold;">
                                                          <%= patnam %>
                                                        </span>
                                                      </a>
                                                    </td>
                                                    <td class="reportcolumn"  >
                                                      <span class="spamColumnLabel" style="text-align:left;">
                                                        <%= paci_id %>
                                                      </span>
                                                    </td>
                                                    <td class="reportcolumn"  >
                                                      <span class="spamColumnLabel" style="text-align:left;">
                                                        <%= stuid %>
                                                      </span>
                                                    </td>
                                                    <td class="reportcolumn"  >
                                                      <span class="spamColumnLabel" style="text-align:left;">
                                                        <%= nome %>
                                                      </span>
                                                    </td>
                                                    <td class="reportcolumn"  >
                                                      <span class="spamColumnLabel" style="text-align:left;">
                                                        <%= com.egen.util.text.FormatDate.format(studat, "dd/MM/yyyy") %>
                                                      </span>
                                                    </td>
                                                  </tr>
                                                  <%
                                                  if (++i_bl_report_Img_studylevel >= res_Img_studylevel.size()) {
                                                    break;
                                                  }
                                                  t_img_studylevel = (com.egen.mini_WEBPACS.dbobj.table.Img_studylevel)res_Img_studylevel.elementAt(i_bl_report_Img_studylevel);
                                                  paci_id = t_img_studylevel.getPaci_id()==null?"":t_img_studylevel.getPaci_id();
                                                  stuid = t_img_studylevel.getStuid()==null?"":t_img_studylevel.getStuid();
                                                  studat = (java.sql.Date)t_img_studylevel.getStudat();
                                                  tp_mod = t_img_studylevel.getTp_mod()==null?"":t_img_studylevel.getTp_mod();
                                                  stuinsuid = t_img_studylevel.getStuinsuid()==null?"":t_img_studylevel.getStuinsuid();
                                                  patnam = "";
                                                  {
                                                    if(paci_id!=null && paci_id.length()>0) {
                                                      com.egen.mini_WEBPACS.dbobj.table.Img_patientlevel $cr_db_object = new com.egen.mini_WEBPACS.dbobj.table.Img_patientlevel();
                                                      Object[][] where = {
                                                      {"paci_id","like",paci_id}
                                                      };
                                                      java.util.Vector results = j.select($cr_db_object, where, null);
                                                      if (results!=null && results.size()>0) {
                                                        $cr_db_object = (com.egen.mini_WEBPACS.dbobj.table.Img_patientlevel)results.elementAt(0);
                                                        patnam = $cr_db_object.getPatnam() + "";
                                                      }
                                                    }
                                                    };
                                                    nome = "";
                                                    {
                                                      if(tp_mod!=null && tp_mod.length()>0) {
                                                        com.egen.mini_WEBPACS.dbobj.table.Img_tipo_pacs $cr_db_object = new com.egen.mini_WEBPACS.dbobj.table.Img_tipo_pacs();
                                                        Object[][] where = {
                                                        {"tp_mod","like",tp_mod}
                                                        };
                                                        java.util.Vector results = j.select($cr_db_object, where, null);
                                                        if (results!=null && results.size()>0) {
                                                          $cr_db_object = (com.egen.mini_WEBPACS.dbobj.table.Img_tipo_pacs)results.elementAt(0);
                                                          nome = $cr_db_object.getNome() + "";
                                                        }
                                                      }
                                                      };
                                                      patbirdat = "";
                                                      {
                                                        if(paci_id!=null && paci_id.length()>0) {
                                                          com.egen.mini_WEBPACS.dbobj.table.Img_patientlevel $cr_db_object = new com.egen.mini_WEBPACS.dbobj.table.Img_patientlevel();
                                                          Object[][] where = {
                                                          {"paci_id","like",paci_id}
                                                          };
                                                          java.util.Vector results = j.select($cr_db_object, where, null);
                                                          if (results!=null && results.size()>0) {
                                                            $cr_db_object = (com.egen.mini_WEBPACS.dbobj.table.Img_patientlevel)results.elementAt(0);
                                                            patbirdat = $cr_db_object.getPatbirdat() + "";
                                                          }
                                                        }
                                                        };
                                                      }
                                                    }
                                                  } catch (Exception e) {
                                                    session.setAttribute("exception",com.egen.util.system.Error.getDescription(e));
                                                    %>
                                                    <jsp:forward page="/system/ErrorPage.jsp"/>
                                                    <%
                                                  } finally {
                                                  if(j!=null){j.close(); }
                                                }
                                                %>
                                              </table>
                                            </form>
                                            <%
                                          }
                                          %>
                                        </div>
                                      </table>
                                    </div>
                                    <script type="text/javascript">
                                      <% String Maintab = request.getParameter("Maintab"); if (Maintab==null) { Maintab = "0"; } %>
                                      switchOn('Maintab<%=Maintab%>','contentsMaintab<%=Maintab%>','toprowMain','contentscellMain');
                                    </script>
                                    <br>
                                    <table class="footerTable">
                                      <tr>
                                        <td class="footerTd">
                                          <bean:message key="div.printmenu"/>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </body>
                          </center>
                        </html>
