<%@ page import="java.util.* " %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<html>
<head>
<title><bean:message key="page.title"/></title>
<bean:message key="page.css"/>
<bean:message key="page.script"/>
</head>
<body>
<table  class="mainTable" style='width:640;'>
<tr ><td >
<table  class="headerTable" style='width:640'><tr ><td  class='td10'><bean:message key="page.title"/></td><td><div class='div10'><%= com.egen.util.text.FormatDate.getTimestamp() %></div></td></tr>
</table>
<html:form action="system/LogonForm.do" method="POST" scope="session" styleClass="baseForm" target="_self">
<table  class="bannerTable" style='width:640;'><tr  ><td  class=tableHeader>
Logon
</td></tr></table>
<table  class="messageTable" style='width:640;'><tr  >
<td  class="messageHeader"><html:errors/></td></tr></table>
<table  class="itemTable" style='width:640;'>
<tr><td class="formLabel"  >Username</td><td ><html:text property="username" styleClass="baseField" size="30"/></td></tr>
<tr><td class="formLabel"  >Password</td><td ><html:password property="password" styleClass="baseField" size="30"/></td></tr>
<tr><td class="formLabel"  >Language</td><td ><html:select property="language" styleClass="baseField" size="1"><html:option styleClass="baseOption" value="en">English</html:option><html:option styleClass="baseOption" value="pt">Portugues</html:option></html:select></td></tr>
</table>
<table class="buttonTable" style='width:640;'><tr >
<td  class="buttonItem">
<html:submit value="Logon" accesskey="l" styleClass="baseButton" property="logon_action"></html:submit>
<html:submit value="Logout" accesskey="o" styleClass="baseButton" property="logout_action"></html:submit>
<html:submit accesskey="r" styleClass="baseButton" property="resetfull_action"><bean:message key="jsp.reset"/></html:submit>
</td></tr></table>
</html:form>
<script type="text/javascript" language="JavaScript">
<!--
var focusControl = document.forms[0].elements["username"];
if (focusControl.type != "hidden") {
  focusControl.focus();
}
// -->
</script>

<table  class="footerTable" style='width:640;'>
<tr ><td ><bean:message key="div.print"/></td></tr>
</table>
</body></html>