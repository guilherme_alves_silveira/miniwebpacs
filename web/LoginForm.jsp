<%@ page import="java.util.* " %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%
  session.setAttribute(org.apache.struts.action.Action.LOCALE_KEY,java.util.Locale.US);
  if (request.getParameter("j_username")!=null && request.getParameter("j_password")!=null) {
   String username = request.getParameter("j_username");
   String password = request.getParameter("j_password");
   try {
     session.setAttribute("j_username", username);
     session.setAttribute("j_password", password);
     session.setMaxInactiveInterval(3600);
   } catch (Exception e) {}
 } else {
%>
<html><head>
<title><bean:message key="page.title"/></title>
<link rel=stylesheet href="style.css" type=text/css>
<script laguage="JavaScript">
  function helper(USERNAME, PASSWORD) {
    iframe.location.href="LoginForm.jsp?j_username=" + USERNAME + "&j_password=" + PASSWORD + "";
  }
</script>
</head>
<body onload="javascript:document.mainForm.j_username.focus();">
<table align="center" style=text-align:center;><tr><td>
<table style=width:640;><tr><td style=font-size:10px;><bean:message key="page.title"/></td><td><div style=font-size:10px;font-weight:normal;text-align:right;><%= com.egen.util.text.FormatDate.getTimestamp() %></div></td></tr></table>
<table style=width:640;><tr><td class=tableHeader>
<bean:message key="login.title"/>
</td></tr></table>
<table style=width:640;margin-top:10;><tr><td style=font-size:12px;>
<form name="mainForm" method="POST" action="j_security_check" onsubmit="javascript:helper(document.mainForm.j_username.value, document.mainForm.j_password.value);">
<tr><td class=formLabel><bean:message key="login.username"/></td><td><input type="text" name="j_username"></td></tr>
<tr><td class=formLabel><bean:message key="login.password"/></td><td><input type="password" name="j_password"></td></tr>
</table>
<table style=width:640;><tr>
<td style=border-color:black;border-top-style:solid;border-top-width:1;padding-top:5;font-size:11px;>
<input type="submit" value="<bean:message key="login.login"/>" accesskey="l" onclick="javascript:helper(document.mainForm.j_username.value, document.mainForm.j_password.value);">
<input type="reset" value="<bean:message key="login.reset"/>" accesskey="r">
</td></tr></table>
</form>
<iframe  name="iframe" src="about:blank" height="1" width="1" scrolling="no" frameborder="0" class="inputclass" class="myhidden"> 
</iframe>
</body></html>
<% } %>
