<%@ page import="java.util.* " %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<html>
<head>
<title>Help</title>
<bean:message key="page.css"/>
<bean:message key="page.script"/>
</head>
<body style=margin-top:0px;margin-left:0px;margin-right:0px;margin-bottom:0px;>
<table style=text-align:center; cellpadding='0' cellspacing='0'>
<% 
   String sh = request.getParameter("help");
%>
<tr><td>
<form name="mainForm" focus="help" method="POST" Class="baseForm">
<table style=width:300; cellpadding='0' cellspacing='0' border=1>
<tr>
<td align=center>
<img src='../img/helpsystem.jpg'>
</td></tr>
<tr>
  <td class='tdHelp'>
    <%= sh %>
  </td>
</tr>
</table>
<table style=width:300; cellpadding='0' cellspacing='0'>
<tr>
<td>
<br>
<input type=button value=<bean:message key="jsp.close"/> accesskey="c" onclick="javascript:window.close();" styleClass="baseButton" property="close_action">
</td></tr></table>
</body></html>