package com.egen.mini_WEBPACS.dbobj.table;

/*** MetaData from table mini_pacs.img_imagelevel
     Version: 2.6 -  Date: 20/10/2004 18:08:54 */
public class Img_imagelevelMetaData {

  public static final String SCHEMA = "mini_pacs";

  /*** Table Primary Key fields. */
  public static final String[] PK = {"sopinsuid"};


  /*** Method for getting table metadada. 
     * { type, name, lenght, not_null, html_type, label, format, validate_type, case_restriction } */
  public static String[][] getMetadata() {
    String[][] metadata = {
                            {"java.lang.String","sopinsuid","64","true","text","sopinsuid","","false","","",},
                            {"java.lang.String","serinsuid","64","true","text","serinsuid","","false","","",},
                            {"java.lang.String","sopclauid","64","false","text","sopclauid","","false","","",},
                            {"long","samperpix","8","false","text","samperpix","","false","","",},
                            {"java.lang.String","phoint","16","false","text","phoint","","false","","",},
                            {"long","row_num","8","false","text","row_num","","false","","",},
                            {"long","col_num","8","false","text","col_num","","false","","",},
                            {"long","bitall","8","false","text","bitall","","false","","",},
                            {"long","bitsto","8","false","text","bitsto","","false","","",},
                            {"long","pixrep","8","false","text","pixrep","","false","","",},
                            {"java.lang.String","patori","16","false","text","patori","","false","","",},
                            {"java.lang.String","imanum","16","false","text","imanum","","false","","",},
                            {"java.sql.Timestamp","insertdatetime","26","false","text","insertdatetime","dd/MM/yyyy HH:mm:ss","false","","",},
                            {"java.lang.String","owner","16","false","text","owner","","false","","",},
                            {"double","size_bytes","65530","false","text","size_bytes","","false","","",},
                            {"java.lang.String","transfer","64","false","text","transfer","","false","","",},
                            {"java.lang.String","img_path","255","false","text","img_path","","false","","",}
                          };
    return metadata;
  }

}
