package com.egen.mini_WEBPACS.dbobj.table;

/*** MetaData from table mini_pacs.img_patientlevel
     Version: 2.6 -  Date: 20/10/2004 18:06:59 */
public class Img_patientlevelMetaData {

  public static final String SCHEMA = "mini_pacs";

  /*** Table Primary Key fields. */
  public static final String[] PK = {"paci_id"};


  /*** Method for getting table metadada. 
     * { type, name, lenght, not_null, html_type, label, format, validate_type, case_restriction } */
  public static String[][] getMetadata() {
    String[][] metadata = {
                            {"java.lang.String","paci_id","64","true","text","paci_id","","false","","",},
                            {"java.lang.String","patnam","64","true","text","patnam","","false","","",},
                            {"java.lang.String","patsex","16","true","text","patsex","","false","","",},
                            {"double","numpatrelstu","65530","false","text","numpatrelstu","","false","","",},
                            {"double","numpatrelser","65530","false","text","numpatrelser","","false","","",},
                            {"double","numpatrelima","65530","false","text","numpatrelima","","false","","",},
                            {"java.lang.String","owner","16","false","text","owner","","false","","",},
                            {"java.sql.Date","patbirdat","12","false","text","patbirdat","","false","","",},
                            {"java.sql.Timestamp","insertdatetime","26","false","text","insertdatetime","dd/MM/yyyy HH:mm:ss","false","","",},
                            {"java.lang.String","patbirtim","12","false","text","patbirtim","","false","","",}
                          };
    return metadata;
  }

}
