package com.egen.mini_WEBPACS.dbobj.table;

import java.lang.reflect.*;

/*** DataBase Object from table mini_pacs.img_patientlevel
     Version: 2.6 -  Date: 20/10/2004 18:06:57 */
public class Img_patientlevel implements java.io.Serializable, com.egen.util.jdbc.Table {
  private java.lang.String Paci_id;
  private java.lang.String Patnam;
  private java.lang.String Patsex;
  private double Numpatrelstu;
  private double Numpatrelser;
  private double Numpatrelima;
  private java.lang.String Owner;
  private java.sql.Date Patbirdat;
  private java.sql.Timestamp Insertdatetime;
  private java.lang.String Patbirtim;

  public Img_patientlevel() {
  }

  public java.lang.String getPaci_id() {
    return Paci_id;
  }

  public void setPaci_id(java.lang.String PACI_ID) {
    Paci_id = PACI_ID;
  }

  public java.lang.String getPatnam() {
    return Patnam;
  }

  public void setPatnam(java.lang.String PATNAM) {
    Patnam = PATNAM;
  }

  public java.lang.String getPatsex() {
    return Patsex;
  }

  public void setPatsex(java.lang.String PATSEX) {
    Patsex = PATSEX;
  }

  public double getNumpatrelstu() {
    return Numpatrelstu;
  }

  public void setNumpatrelstu(double NUMPATRELSTU) {
    Numpatrelstu = NUMPATRELSTU;
  }

  public double getNumpatrelser() {
    return Numpatrelser;
  }

  public void setNumpatrelser(double NUMPATRELSER) {
    Numpatrelser = NUMPATRELSER;
  }

  public double getNumpatrelima() {
    return Numpatrelima;
  }

  public void setNumpatrelima(double NUMPATRELIMA) {
    Numpatrelima = NUMPATRELIMA;
  }

  public java.lang.String getOwner() {
    return Owner;
  }

  public void setOwner(java.lang.String OWNER) {
    Owner = OWNER;
  }

  public java.sql.Date getPatbirdat() {
    return Patbirdat;
  }

  public void setPatbirdat(java.sql.Date PATBIRDAT) {
    Patbirdat = PATBIRDAT;
  }

  public java.sql.Timestamp getInsertdatetime() {
    return Insertdatetime;
  }

  public void setInsertdatetime(java.sql.Timestamp INSERTDATETIME) {
    Insertdatetime = INSERTDATETIME;
  }

  public java.lang.String getPatbirtim() {
    return Patbirtim;
  }

  public void setPatbirtim(java.lang.String PATBIRTIM) {
    Patbirtim = PATBIRTIM;
  }

}
