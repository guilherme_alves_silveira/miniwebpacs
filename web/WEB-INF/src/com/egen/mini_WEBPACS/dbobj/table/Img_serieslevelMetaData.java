package com.egen.mini_WEBPACS.dbobj.table;

/*** MetaData from table mini_pacs.img_serieslevel
     Version: 2.6 -  Date: 20/10/2004 18:08:13 */
public class Img_serieslevelMetaData {

  public static final String SCHEMA = "mini_pacs";

  /*** Table Primary Key fields. */
  public static final String[] PK = {"serinsuid"};


  /*** Method for getting table metadada. 
     * { type, name, lenght, not_null, html_type, label, format, validate_type, case_restriction } */
  public static String[][] getMetadata() {
    String[][] metadata = {
                            {"java.lang.String","serinsuid","64","true","text","serinsuid","","false","","",},
                            {"java.lang.String","stuinsuid","64","true","text","stuinsuid","","false","","",},
                            {"java.lang.String","sernum","20","false","text","sernum","","false","","",},
                            {"java.lang.String","bodparexa","40","false","text","bodparexa","","false","","",},
                            {"java.lang.String","serdes","64","false","text","serdes","","false","","",},
                            {"java.lang.String","viepos","64","false","text","viepos","","false","","",},
                            {"long","numserrelima","8","false","text","numserrelima","","false","","",},
                            {"java.sql.Timestamp","insertdatetime","26","false","text","insertdatetime","dd/MM/yyyy HH:mm:ss","false","","",},
                            {"java.lang.String","tp_mod","5","false","text","tp_mod","","false","","",},
                            {"java.lang.String","owner","16","false","text","owner","","false","","",},
                            {"long","size_bytes","8","false","text","size_bytes","","false","","",},
                            {"java.lang.String","ser_path","255","false","text","ser_path","","false","","",}
                          };
    return metadata;
  }

}
