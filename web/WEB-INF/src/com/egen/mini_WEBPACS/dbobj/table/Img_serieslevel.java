package com.egen.mini_WEBPACS.dbobj.table;

import java.lang.reflect.*;

/*** DataBase Object from table mini_pacs.img_serieslevel
     Version: 2.6 -  Date: 20/10/2004 18:08:12 */
public class Img_serieslevel implements java.io.Serializable, com.egen.util.jdbc.Table {
  private java.lang.String Serinsuid;
  private java.lang.String Stuinsuid;
  private java.lang.String Sernum;
  private java.lang.String Bodparexa;
  private java.lang.String Serdes;
  private java.lang.String Viepos;
  private long Numserrelima;
  private java.sql.Timestamp Insertdatetime;
  private java.lang.String Tp_mod;
  private java.lang.String Owner;
  private long Size_bytes;
  private java.lang.String Ser_path;

  public Img_serieslevel() {
  }

  public java.lang.String getSerinsuid() {
    return Serinsuid;
  }

  public void setSerinsuid(java.lang.String SERINSUID) {
    Serinsuid = SERINSUID;
  }

  public java.lang.String getStuinsuid() {
    return Stuinsuid;
  }

  public void setStuinsuid(java.lang.String STUINSUID) {
    Stuinsuid = STUINSUID;
  }

  public java.lang.String getSernum() {
    return Sernum;
  }

  public void setSernum(java.lang.String SERNUM) {
    Sernum = SERNUM;
  }

  public java.lang.String getBodparexa() {
    return Bodparexa;
  }

  public void setBodparexa(java.lang.String BODPAREXA) {
    Bodparexa = BODPAREXA;
  }

  public java.lang.String getSerdes() {
    return Serdes;
  }

  public void setSerdes(java.lang.String SERDES) {
    Serdes = SERDES;
  }

  public java.lang.String getViepos() {
    return Viepos;
  }

  public void setViepos(java.lang.String VIEPOS) {
    Viepos = VIEPOS;
  }

  public long getNumserrelima() {
    return Numserrelima;
  }

  public void setNumserrelima(long NUMSERRELIMA) {
    Numserrelima = NUMSERRELIMA;
  }

  public java.sql.Timestamp getInsertdatetime() {
    return Insertdatetime;
  }

  public void setInsertdatetime(java.sql.Timestamp INSERTDATETIME) {
    Insertdatetime = INSERTDATETIME;
  }

  public java.lang.String getTp_mod() {
    return Tp_mod;
  }

  public void setTp_mod(java.lang.String TP_MOD) {
    Tp_mod = TP_MOD;
  }

  public java.lang.String getOwner() {
    return Owner;
  }

  public void setOwner(java.lang.String OWNER) {
    Owner = OWNER;
  }

  public long getSize_bytes() {
    return Size_bytes;
  }

  public void setSize_bytes(long SIZE_BYTES) {
    Size_bytes = SIZE_BYTES;
  }

  public java.lang.String getSer_path() {
    return Ser_path;
  }

  public void setSer_path(java.lang.String SER_PATH) {
    Ser_path = SER_PATH;
  }

}
