package com.egen.mini_WEBPACS.dbobj.table;

/*** MetaData from table mini_pacs.img_studylevel
     Version: 2.6 -  Date: 20/10/2004 18:07:48 */
public class Img_studylevelMetaData {

  public static final String SCHEMA = "mini_pacs";

  /*** Table Primary Key fields. */
  public static final String[] PK = {"stuinsuid"};


  /*** Method for getting table metadada. 
     * { type, name, lenght, not_null, html_type, label, format, validate_type, case_restriction } */
  public static String[][] getMetadata() {
    String[][] metadata = {
                            {"java.lang.String","paci_id","64","true","text","paci_id","","false","","",},
                            {"java.lang.String","patage","4","false","text","patage","","false","","",},
                            {"java.lang.String","patwei","16","false","text","patwei","","false","","",},
                            {"double","numsturelima","65530","false","text","numsturelima","","false","","",},
                            {"java.lang.String","tp_mod","5","false","text","tp_mod","","false","","",},
                            {"java.lang.String","accnum","16","false","text","accnum","","false","","",},
                            {"java.lang.String","stuid","16","true","text","stuid","","false","","",},
                            {"java.lang.String","stuinsuid","64","true","text","stuinsuid","","false","","",},
                            {"java.lang.String","refphynam","64","false","text","refphynam","","false","","",},
                            {"java.lang.String","studes","64","false","text","studes","","false","","",},
                            {"java.lang.String","patsiz","16","false","text","patsiz","","false","","",},
                            {"double","numsturelser","65530","false","text","numsturelser","","false","","",},
                            {"java.lang.String","owner","16","false","text","owner","","false","","",},
                            {"double","size_bytes","65530","false","text","size_bytes","","false","","",},
                            {"java.lang.String","path_estudo","100","false","text","path_estudo","","false","","",},
                            {"int","flag_online","-1","false","text","flag_online","","false","","",},
                            {"java.sql.Date","studat","12","false","text","studat","","false","","",},
                            {"java.lang.String","stutim","12","false","text","stutim","","false","","",},
                            {"java.sql.Timestamp","insertdatetime","26","false","text","insertdatetime","dd/MM/yyyy HH:mm:ss","false","","",}
                          };
    return metadata;
  }

}
