package com.egen.mini_WEBPACS.dbobj.table;

import java.lang.reflect.*;

/*** DataBase Object from table mini_pacs.img_studylevel
     Version: 2.6 -  Date: 20/10/2004 18:07:47 */
public class Img_studylevel implements java.io.Serializable, com.egen.util.jdbc.Table {
  private java.lang.String Paci_id;
  private java.lang.String Patage;
  private java.lang.String Patwei;
  private double Numsturelima;
  private java.lang.String Tp_mod;
  private java.lang.String Accnum;
  private java.lang.String Stuid;
  private java.lang.String Stuinsuid;
  private java.lang.String Refphynam;
  private java.lang.String Studes;
  private java.lang.String Patsiz;
  private double Numsturelser;
  private java.lang.String Owner;
  private double Size_bytes;
  private java.lang.String Path_estudo;
  private int Flag_online;
  private java.sql.Date Studat;
  private java.lang.String Stutim;
  private java.sql.Timestamp Insertdatetime;

  public Img_studylevel() {
  }

  public java.lang.String getPaci_id() {
    return Paci_id;
  }

  public void setPaci_id(java.lang.String PACI_ID) {
    Paci_id = PACI_ID;
  }

  public java.lang.String getPatage() {
    return Patage;
  }

  public void setPatage(java.lang.String PATAGE) {
    Patage = PATAGE;
  }

  public java.lang.String getPatwei() {
    return Patwei;
  }

  public void setPatwei(java.lang.String PATWEI) {
    Patwei = PATWEI;
  }

  public double getNumsturelima() {
    return Numsturelima;
  }

  public void setNumsturelima(double NUMSTURELIMA) {
    Numsturelima = NUMSTURELIMA;
  }

  public java.lang.String getTp_mod() {
    return Tp_mod;
  }

  public void setTp_mod(java.lang.String TP_MOD) {
    Tp_mod = TP_MOD;
  }

  public java.lang.String getAccnum() {
    return Accnum;
  }

  public void setAccnum(java.lang.String ACCNUM) {
    Accnum = ACCNUM;
  }

  public java.lang.String getStuid() {
    return Stuid;
  }

  public void setStuid(java.lang.String STUID) {
    Stuid = STUID;
  }

  public java.lang.String getStuinsuid() {
    return Stuinsuid;
  }

  public void setStuinsuid(java.lang.String STUINSUID) {
    Stuinsuid = STUINSUID;
  }

  public java.lang.String getRefphynam() {
    return Refphynam;
  }

  public void setRefphynam(java.lang.String REFPHYNAM) {
    Refphynam = REFPHYNAM;
  }

  public java.lang.String getStudes() {
    return Studes;
  }

  public void setStudes(java.lang.String STUDES) {
    Studes = STUDES;
  }

  public java.lang.String getPatsiz() {
    return Patsiz;
  }

  public void setPatsiz(java.lang.String PATSIZ) {
    Patsiz = PATSIZ;
  }

  public double getNumsturelser() {
    return Numsturelser;
  }

  public void setNumsturelser(double NUMSTURELSER) {
    Numsturelser = NUMSTURELSER;
  }

  public java.lang.String getOwner() {
    return Owner;
  }

  public void setOwner(java.lang.String OWNER) {
    Owner = OWNER;
  }

  public double getSize_bytes() {
    return Size_bytes;
  }

  public void setSize_bytes(double SIZE_BYTES) {
    Size_bytes = SIZE_BYTES;
  }

  public java.lang.String getPath_estudo() {
    return Path_estudo;
  }

  public void setPath_estudo(java.lang.String PATH_ESTUDO) {
    Path_estudo = PATH_ESTUDO;
  }

  public int getFlag_online() {
    return Flag_online;
  }

  public void setFlag_online(int FLAG_ONLINE) {
    Flag_online = FLAG_ONLINE;
  }

  public java.sql.Date getStudat() {
    return Studat;
  }

  public void setStudat(java.sql.Date STUDAT) {
    Studat = STUDAT;
  }

  public java.lang.String getStutim() {
    return Stutim;
  }

  public void setStutim(java.lang.String STUTIM) {
    Stutim = STUTIM;
  }

  public java.sql.Timestamp getInsertdatetime() {
    return Insertdatetime;
  }

  public void setInsertdatetime(java.sql.Timestamp INSERTDATETIME) {
    Insertdatetime = INSERTDATETIME;
  }

}
