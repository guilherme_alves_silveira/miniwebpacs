package com.egen.mini_WEBPACS.dbobj.table;

import java.lang.reflect.*;

/*** DataBase Object from table mini_pacs.img_imagelevel
     Version: 2.6 -  Date: 20/10/2004 18:08:52 */
public class Img_imagelevel implements java.io.Serializable, com.egen.util.jdbc.Table {
  private java.lang.String Sopinsuid;
  private java.lang.String Serinsuid;
  private java.lang.String Sopclauid;
  private long Samperpix;
  private java.lang.String Phoint;
  private long Row_num;
  private long Col_num;
  private long Bitall;
  private long Bitsto;
  private long Pixrep;
  private java.lang.String Patori;
  private java.lang.String Imanum;
  private java.sql.Timestamp Insertdatetime;
  private java.lang.String Owner;
  private double Size_bytes;
  private java.lang.String Transfer;
  private java.lang.String Img_path;

  public Img_imagelevel() {
  }

  public java.lang.String getSopinsuid() {
    return Sopinsuid;
  }

  public void setSopinsuid(java.lang.String SOPINSUID) {
    Sopinsuid = SOPINSUID;
  }

  public java.lang.String getSerinsuid() {
    return Serinsuid;
  }

  public void setSerinsuid(java.lang.String SERINSUID) {
    Serinsuid = SERINSUID;
  }

  public java.lang.String getSopclauid() {
    return Sopclauid;
  }

  public void setSopclauid(java.lang.String SOPCLAUID) {
    Sopclauid = SOPCLAUID;
  }

  public long getSamperpix() {
    return Samperpix;
  }

  public void setSamperpix(long SAMPERPIX) {
    Samperpix = SAMPERPIX;
  }

  public java.lang.String getPhoint() {
    return Phoint;
  }

  public void setPhoint(java.lang.String PHOINT) {
    Phoint = PHOINT;
  }

  public long getRow_num() {
    return Row_num;
  }

  public void setRow_num(long ROW_NUM) {
    Row_num = ROW_NUM;
  }

  public long getCol_num() {
    return Col_num;
  }

  public void setCol_num(long COL_NUM) {
    Col_num = COL_NUM;
  }

  public long getBitall() {
    return Bitall;
  }

  public void setBitall(long BITALL) {
    Bitall = BITALL;
  }

  public long getBitsto() {
    return Bitsto;
  }

  public void setBitsto(long BITSTO) {
    Bitsto = BITSTO;
  }

  public long getPixrep() {
    return Pixrep;
  }

  public void setPixrep(long PIXREP) {
    Pixrep = PIXREP;
  }

  public java.lang.String getPatori() {
    return Patori;
  }

  public void setPatori(java.lang.String PATORI) {
    Patori = PATORI;
  }

  public java.lang.String getImanum() {
    return Imanum;
  }

  public void setImanum(java.lang.String IMANUM) {
    Imanum = IMANUM;
  }

  public java.sql.Timestamp getInsertdatetime() {
    return Insertdatetime;
  }

  public void setInsertdatetime(java.sql.Timestamp INSERTDATETIME) {
    Insertdatetime = INSERTDATETIME;
  }

  public java.lang.String getOwner() {
    return Owner;
  }

  public void setOwner(java.lang.String OWNER) {
    Owner = OWNER;
  }

  public double getSize_bytes() {
    return Size_bytes;
  }

  public void setSize_bytes(double SIZE_BYTES) {
    Size_bytes = SIZE_BYTES;
  }

  public java.lang.String getTransfer() {
    return Transfer;
  }

  public void setTransfer(java.lang.String TRANSFER) {
    Transfer = TRANSFER;
  }

  public java.lang.String getImg_path() {
    return Img_path;
  }

  public void setImg_path(java.lang.String IMG_PATH) {
    Img_path = IMG_PATH;
  }

}
